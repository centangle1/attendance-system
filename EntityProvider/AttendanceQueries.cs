﻿using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.SqlServer;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public DataTableAttendanceViewModel GetAllAttendances(UserParameters search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = new DataTableAttendanceViewModel();
                    result.TotalRecordCount = db.Attendances.Count();
                    bool isSearchColumnNull = !(search.AjaxSearchParameters.columns != null && search.AjaxSearchParameters.columns.Count() != 0);


                    var excludeMachineCheck = search.UserRole == "Administrator" || search.UserRole == "Company Manager";

                    int userMachineId = 0;
                    if (search.AjaxSearchParameters.search != null)
                        int.TryParse(search.AjaxSearchParameters.search.value, out userMachineId);

                    string searchParameter = search.AjaxSearchParameters.search != null && !string.IsNullOrEmpty(search.AjaxSearchParameters.search.value) ? search.AjaxSearchParameters.search.value.ToLower() : "";
                    string userName = isSearchColumnNull ? "" : search.AjaxSearchParameters.columns.Where(m => m.name == "UserName").FirstOrDefault().search.value;
                    string branchName = isSearchColumnNull ? "" : search.AjaxSearchParameters.columns.Where(m => m.name == "BranchName").FirstOrDefault().search.value;
                    string departmentName = isSearchColumnNull ? "" : search.AjaxSearchParameters.columns.Where(m => m.name == "DepartmentName").FirstOrDefault().search.value;
                    string entry = isSearchColumnNull ? "" : search.AjaxSearchParameters.columns.Where(m => m.name == "EntryTime").FirstOrDefault().search.value;

                    bool isOrderNull = search.AjaxSearchParameters.order == null;
                    string sortColumn = isOrderNull && isSearchColumnNull ? "UserName" : search.AjaxSearchParameters.columns.ElementAt(search.AjaxSearchParameters.order.First().column).name;
                    string sortDirection = isOrderNull ? "" : search.AjaxSearchParameters.order.First().dir;

                    int searchMachineId = 0;
                    if (search.AjaxSearchParameters.columns != null)
                        int.TryParse(search.AjaxSearchParameters.columns.Where(m => m.name == "MachineId").FirstOrDefault().search.value, out searchMachineId);

                    int generalSearchMachineId = 0;
                    int.TryParse(search.AjaxSearchParameters.search.value, out generalSearchMachineId);

                    var queryableResult = db.Attendances
                        .Join(db.Branches, a => a.BranchId, b => b.Id, (a, b) => new { a, b })
                        .Join(db.Departments, b => b.b.Id, d => d.BranchCode, (b, d) => new { a = b.a, b = b.b, d })
                        .Join(db.Users, d => d.d.Id, u => u.DepartmentId, (d, u) => new { a = d.a, b = d.b, d = d.d, u })
                        .AsQueryable();

                    if (excludeMachineCheck)
                    {
                        if (!string.IsNullOrEmpty(searchParameter))
                        {
                            queryableResult = queryableResult.Where(m => m.a.Active
                                    && (m.a.MachineId == m.u.MachineId)
                                    && (
                                                    m.b.Name.ToLower().Contains(searchParameter)
                                                    || m.d.Name.ToLower().Contains(searchParameter)
                                                    || m.u.UserName.ToLower().Contains(searchParameter)
                                                    || m.u.MachineId == generalSearchMachineId
                                                    || (SqlFunctions.DateName("Day", m.a.EntryTime) + "-" + SqlFunctions.DateName("Month", m.a.EntryTime).Substring(0, 3) + "-" + SqlFunctions.DateName("Year", m.a.EntryTime) + " " + SqlFunctions.DateName("hh", m.a.EntryTime) + ":" + SqlFunctions.DateName("n", m.a.EntryTime)).Contains(searchParameter)
                                        )
                              ).AsQueryable();
                        }

                        if (string.IsNullOrEmpty(searchParameter) || !string.IsNullOrEmpty(userName) || !string.IsNullOrEmpty(branchName) || !string.IsNullOrEmpty(departmentName) || !string.IsNullOrEmpty(entry) || searchMachineId != 0)
                        {
                            queryableResult = queryableResult.Where(m => m.a.Active && 
                                    (m.a.MachineId == m.u.MachineId) &&
                                    (string.IsNullOrEmpty(userName) || m.u.UserName.ToLower().Contains(userName.ToLower())) &&
                                    (string.IsNullOrEmpty(branchName) || m.b.Name.ToLower().Contains(branchName.ToLower())) &&
                                    (string.IsNullOrEmpty(departmentName) || departmentName.ToLower().Contains(m.b.Name.ToLower())) &&
                                    (string.IsNullOrEmpty(entry) || (SqlFunctions.DateName("Day", m.a.EntryTime) + "-" + SqlFunctions.DateName("Month", m.a.EntryTime).Substring(0, 3) + "-" + SqlFunctions.DateName("Year", m.a.EntryTime) + " " + SqlFunctions.DateName("hh", m.a.EntryTime) + ":" + SqlFunctions.DateName("n", m.a.EntryTime)).Contains(entry.ToLower())) &&
                                    (searchMachineId == 0 || m.u.MachineId == searchMachineId)
                             ).AsQueryable();
                        }


                    }
                    else
                    {
                        queryableResult = queryableResult.Where(m => m.a.Active
                                   && (m.a.MachineId == m.u.MachineId)
                                   && (m.u.MachineId == search.MachineId)
                                   && (m.u.Id == search.UserId)
                                   && (string.IsNullOrEmpty(entry) || (SqlFunctions.DateName("Day", m.a.EntryTime) + "-" + SqlFunctions.DateName("Month", m.a.EntryTime).Substring(0, 3) + "-" + SqlFunctions.DateName("Year", m.a.EntryTime) + " " + SqlFunctions.DateName("hh", m.a.EntryTime) + ":" + SqlFunctions.DateName("n", m.a.EntryTime)).Contains(entry.ToLower()))
                             ).AsQueryable();
                    }

                    //var check = queryableResult.Skip(search.DataTableParameters.Start).Take(search.DataTableParameters.Length).OrderBy(search.DataTableParameters.SortColumnName).ToList();
                    result.FilteredRecordCount = queryableResult.Count();

                    result.Attendances = queryableResult
                        .Select(m => new AttendanceViewModel
                        {
                            Active = m.a.Active,
                            BranchId = m.a.BranchId,
                            Id = m.a.Id,
                            MacAddress = m.a.MacAddress,
                            EntryTime = m.a.EntryTime,
                            Error = m.a.Error,
                            MachineId = m.a.MachineId,
                            ManualEntry = m.a.ManualEntry,
                            UserId = search.UserId,
                            BranchName = m.b.Name,
                            DepartmentId = m.d.Id,
                            DepartmentName = m.d.Name,
                            UserName = m.u.UserName
                        })
                        .OrderBy(sortColumn + " " + sortDirection)
                        .Skip(search.AjaxSearchParameters.start).Take(search.AjaxSearchParameters.length)
                        .ToList();

                    return result;
                }
            }
            catch (Exception ex)
            {
                return new DataTableAttendanceViewModel();
            }
        }



        Expression<Func<DateTime, string, bool>> ConvertDateTimeToString = (entryDate, searchDate) =>
                                        entryDate != new DateTime() ? ((SqlFunctions.DateName("Day", entryDate) + "-" + SqlFunctions.DateName("Month", entryDate) + "-" + SqlFunctions.DateName("Year", entryDate)).Contains(searchDate.ToLower())) : false;

        public bool AddAttendance(Attendance model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    model.Id = Guid.NewGuid();
                    model.Active = true;
                    var result = db.Attendances.Add(model);
                    return db.SaveChanges() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public AttendanceViewModel GetUser(string id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Users
           .Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
           .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
           .Join(db.Machines, b => b.b.Id, m => m.BranchId, (b, m) => new { b, m })
           .Where(m => m.b.d.u.Id == id && m.b.d.u.Active)
           .Select(m => new AttendanceViewModel
           {
               BranchId = m.b.b.Id,
               MacAddress = m.m.MacAddress,
               MachineId = m.b.d.u.MachineId,
               UserId = m.b.d.u.Id
           }).FirstOrDefault();
                    return result;
                    //return db.Attendances.Where(m => m.Id == id && m.Active).FirstOrDefault();  
                }
            }
            catch (Exception ex)
            {
                return new AttendanceViewModel();
            }
        }

        public string GetUserId(Attendance model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Users
             .Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
             .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
             .Join(db.Machines, b => b.b.Id, m => m.BranchId, (b, m) => new { b, m })
             .Join(db.Attendances, m => m.m.MacAddress, a => a.MacAddress, (m, a) => new { m, a })
             .Where(m => m.a.Id == model.Id)
             .Select(m => m.m.b.d.u.Id).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string AddManualAttendance(AttendanceViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var attendance = new Attendance
                    {
                        Active = model.Active,
                        Id = model.Id,
                        MacAddress = model.MacAddress,
                        BranchId = model.BranchId,
                        EntryTime = model.EntryTime,
                        Error = model.Error,
                        MachineId = model.MachineId,
                        ManualEntry = model.ManualEntry,
                        ImageUrl = model.ImageUrl
                    };
                    var result = db.Attendances.Add(attendance);
                    db.SaveChanges();
                    return model.UserId;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public AttendanceViewModel GetAttendance(Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Users
        .Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
        .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
        .Join(db.Machines, b => b.b.Id, m => m.BranchId, (b, m) => new { b, m })
        .Join(db.Attendances, m => m.m.MacAddress, a => a.MacAddress, (m, a) => new { m, a })
        .Where(m => m.a.Id == id && m.a.Active)
        .Select(m => new AttendanceViewModel
        {
            Active = m.a.Active,
            MacAddress = m.a.MacAddress,
            BranchId = m.a.BranchId,
            EntryTime = m.a.EntryTime,
            Error = m.a.Error,
            Id = m.a.Id,
            MachineId = m.a.MachineId,
            ManualEntry = m.a.ManualEntry,
            UserId = m.m.b.d.u.Id
        }).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new AttendanceViewModel();
            }
        }

        public string EditAttendance(AttendanceViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Users
         .Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
         .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
         .Join(db.Machines, b => b.b.Id, m => m.BranchId, (b, m) => new { b, m })
         .Join(db.Attendances, m => m.m.MacAddress, a => a.MacAddress, (m, a) => new { m, a })
         .Where(m => m.a.Id == model.Id && m.m.b.d.u.Active)
         .Select(m => new AttendanceViewModel
         {
             Id = m.a.Id,
             Error = m.a.Error,
             EntryTime = m.a.EntryTime,
             ManualEntry = m.a.ManualEntry,
             BranchId = m.a.Id,
             MacAddress = m.a.MacAddress,
             MachineId = m.a.MachineId,
             UserId = m.m.b.d.u.Id,
             Active = m.a.Active
         }).FirstOrDefault();
                    var savedAttendance = new Attendance { Id = result.Id, BranchId = result.BranchId, Active = result.Active, EntryTime = model.EntryTime, MacAddress = result.MacAddress, Error = model.Error, MachineId = result.MachineId, ManualEntry = true };
                    db.Entry(savedAttendance).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.UserId;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        //    public List<Attendance1> GetUsers(string macAddress)
        //    {
        //        try
        //        {
        //            using (ApplicationDbContext db = new ApplicationDbContext())
        //            {
        //                var result = db.Users
        //                    .Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
        //                    .Join(db.Branches, dd => dd.d.BranchCode, b => b.Id, (dd, b) => new { dd, b })
        //                    .Join(db.Applications, bb => bb.b.Id, a => a.BranchId, (bb, a) => new { bb, a })
        //                    .Where(m => m.a.MacAddress == macAddress)
        //                    .Select(m => new
        //                    {
        //                        AppId = m.a.Id,
        //                        BranchId = m.bb.b.Id,
        //                        MachineId = m.bb.dd.u.MachineId,
        //                        UserId = m.bb.dd.u.Id
        //                    }).ToList()
        //                    .Select(m => new Attendance1
        //                    {
        //                        AppId = m.AppId,
        //                        BranchId = m.BranchId,
        //                        MachineId = m.MachineId,
        //                        UserId = m.UserId
        //                    }).ToList();
        //                return result;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return new List<Attendance1>();
        //        }
        //    }

        //    public Guid AddUsers(Attendance model)
        //    {
        //        try
        //        {
        //            using (ApplicationDbContext db = new ApplicationDbContext())
        //            {
        //                var check = db.Attendances1.Where(m => m.BranchId == model.BranchId && m.MachineId == model.MachineId && m.EntryTime < model.EntryTime && m.ExitTime == new DateTime(0001, 01, 01)).FirstOrDefault();
        //                if (check != null)
        //                {
        //                    check.ExitTime = model.EntryTime;
        //                    db.Entry(check).State = EntityState.Modified;
        //                    db.SaveChanges();
        //                    return check.Id;
        //                }
        //                else
        //                {
        //                    var attendance = new Attendance1
        //                    {
        //                        Active = true,
        //                        AppId = model.AppId,
        //                        BranchId = model.BranchId,
        //                        Error = model.Error,
        //                        EntryTime = model.EntryTime,
        //                        Id = Guid.NewGuid(),
        //                        ManualEntry = model.ManualEntry,
        //                        MachineId = model.MachineId,
        //                        UserId = model.UserId
        //                    };
        //                    var result = db.Attendances1.Add(attendance);
        //                    return result.Id;
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return new Guid();
        //        }
        //    }

        //    public Guid FixSingleAttendances(string userId)
        //    {
        //        try
        //        {
        //            using (ApplicationDbContext db = new ApplicationDbContext())
        //            {
        //                var attendance = db.Attendances1.Where(m => m.UserId == userId && m.ExitTime == new DateTime(0001, 01, 01)).FirstOrDefault();
        //                var schedules = db.ScheduleDetails.Join(db.Schedules, sd => sd.ScheduleId, s => s.Id, (sd, s) => new { sd, s })
        //                    .Join(db.UserSchedules, ss => ss.s.Id, us => us.Id, (ss, us) => new { ss, us })
        //                    .Where(m => m.us.UserId == userId && m.ss.sd.Day == (Int16)attendance.EntryTime.DayOfWeek)
        //                    .Select(m => m.ss.sd.EndingTime).ToList();

        //                for (int i = 1; i < schedules.Count(); i++)
        //                {
        //                    var thisResult =
        //                    if ()
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return new Guid();
        //        }
        //    }
        //}
    }
}
