﻿using CustomModels.Collection;
using CustomModels.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;

namespace EntityProvider
{
    public partial class DataAccess
    {
        /// <summary>
        /// Filter out the companies as per search filters.
        /// It allows Administrator to view all records and for the rest the parameters are
        /// CompanyId,
        /// BranchId,
        /// UserId,
        /// or if in case none is given then first it will check that a list of all records is asked by the method i.e. for times when we require just a single company the user is register in
        /// and for dropdowns we use GetAllCompanies
        /// </summary>
        /// <param name="search"></param>
        /// <returns>filtered list of companies</returns>
        public List<Company> GetCompanies(CompanySearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    bool IsAdministrator = HttpContext.Current.User.IsInRole("Administrator");
                    var userId = IsAdministrator && search.GetAllCompanies ? "" : HttpContext.Current.User.Identity.GetUserId();

                    var result = db.Companies.GroupJoin(db.Branches, c => c.Id, b => b.CompanyId, (c, b) => new { c, b = b.FirstOrDefault() })
                                             .GroupJoin(db.Departments, b => b.b.Id, d => d.BranchCode, (b, d) => new { b = b.b, c = b.c, d = d.FirstOrDefault() })
                                             .GroupJoin(db.Users, d => d.d.Id, u => u.DepartmentId, (d, u) => new { d = d.d, b = d.b, c = d.c, u = u.FirstOrDefault() })
                                                .Where(m => m.c.Active &&
                                                (search.Id == new Guid() || m.c.Id == search.Id) &&
                                                (string.IsNullOrEmpty(search.UserId) || m.u.Id == search.UserId) &&
                                                (search.GetAllCompanies || string.IsNullOrEmpty(userId) || m.u.Id == userId)
                                                )
                                                .Select(m => new
                                                {
                                                    Active = m.c.Active,
                                                    Description = m.c.Description,
                                                    Id = m.c.Id,
                                                    Name = m.c.Name
                                                }).ToList()
                                                .Select(m => new Company
                                                {
                                                    Active = m.Active,
                                                    Description = m.Description,
                                                    Id = m.Id,
                                                    Name = m.Name
                                                }).ToList();

                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<Company>();
            }
        }

        public Guid CreateCompany(CompanyViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    model.Active = true;
                    model.Id = Guid.NewGuid();

                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<CompanyViewModel, Company>(); });
                    IMapper iMapper = config.CreateMapper();
                    var companyObject = iMapper.Map<CompanyViewModel, Company>(model);
                    var result = db.Companies.Add(companyObject);
                    db.SaveChanges();
                    return result.Id;
                }
            }

            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid EditCompany(CompanyViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    //var config = new MapperConfiguration(cfg => { cfg.CreateMap<CompanyViewModel, Company>(); });
                    //IMapper iMapper = config.CreateMapper();
                    //var companyObject = iMapper.Map<CompanyViewModel, Company>(model);

                    var result = db.Companies.Where(m => m.Id == model.Id).FirstOrDefault();
                    result.Name = model.Name;
                    result.Description = model.Description;
                    result.LogoUrl = model.LogoUrl != null ? model.LogoUrl : result.LogoUrl;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception)
            {
                return new Guid();
            }
        }

        public Guid DeleteCompany(Company model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Companies.Where(m => m.Id == model.Id).FirstOrDefault();
                    result.Active = false;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return Guid.NewGuid();
                }
            }
            catch (Exception)
            {
                return new Guid();
            }
        }

        public List<CompanyPolicy> GetCompanyPolicies(CompanyPolicySearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.CompanyPolicies.Where(m => m.Active && 
                                                (search.CompanyId == new Guid() || m.CompanyId == search.CompanyId) &&
                                                (search.Id == new Guid() || m.Id == search.Id)
                                                ).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<CompanyPolicy>();
            }
        }

        public Guid AddPolicy(CompanyPolicy model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.CompanyPolicies.Add(model);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid EditPolicy(CompanyPolicy model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.CompanyPolicies.Find(model.Id);
                    result.Name = model.Name;
                    result.Value = model.Value;
                    result.Type = model.Type;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid DeletePolicy(CompanyPolicy model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.CompanyPolicies.Find(model.Id);
                    result.Active = false;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }
    }
}
