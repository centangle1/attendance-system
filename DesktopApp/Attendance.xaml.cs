﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AttendanceSystemWpf
{
    /// <summary>
    /// Interaction logic for Attendance.xaml
    /// </summary>
    public partial class Attendance : Window
    {
        public Attendance()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string cs = @"Data Source=.\SQLEXPRESS;Initial Catalog=AttendanceDb;Integrated Security=True";
            SqlConnection conn = null;


            try
            {

                string attstatus = tb1.Text;

                DateTime datenow = Convert.ToDateTime(tb2.Text);
                DateTime timeinn = Convert.ToDateTime(tb3.Text);
                timeinn.ToString("HH:mm:ss");
                DateTime timeoutt = Convert.ToDateTime(tb4.Text);
                timeoutt.ToString("HH:mm:ss");
                int attId = Convert.ToInt32(tb5.Text);
                int userid = Convert.ToInt32(tb6.Text);


                conn = new SqlConnection(cs);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;

                cmd.CommandText = "INSERT INTO Attendance(att_status,date,time_in,time_out,att_id,user_id) VALUES('" + attstatus + "',' " + datenow + "', '" + timeinn + "', '" + timeoutt + "','" + attId + "', '" + userid + "')";

                cmd.Prepare();


                cmd.ExecuteNonQuery();
                //}


            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
