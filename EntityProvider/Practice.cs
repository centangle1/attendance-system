﻿using CustomModels.Collection;
using CustomModels.Identity;
using EnumLibrary;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace EntityProvider
{
    class Practice
    {
        public List<EarnedLeaveViewModel> GetEarnedLeavesForUpdation(EarnedLeaveViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var leaveTypes = Enum.GetNames(typeof(LeaveDurationType));
                    List<LeaveTypeList> durations = new List<LeaveTypeList>();
                    foreach (var x in leaveTypes)
                    {
                        var val = 0;
                        switch ((LeaveDurationType)Enum.Parse(typeof(LeaveDurationType), x))
                        {
                            case LeaveDurationType.Weekly:
                                val = 7; break;

                            case LeaveDurationType.BiWeekly:
                                val = 14; break;

                            case LeaveDurationType.Monthly:
                                val = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); break;

                            case LeaveDurationType.BiMonthly:
                                val = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month); break;

                            case LeaveDurationType.Quarterly:
                                val = 365 / 4; break;

                            case LeaveDurationType.SemiAnnually:
                                val = 365 / 2; break;

                            case LeaveDurationType.Annually:
                                val = 365; break;

                            default: val = 0; break;

                        }
                        durations.Add(new LeaveTypeList { Name = x, Value = val });
                    }

                    var result = db.Users.GroupJoin(db.LeaveRules, e => e.Type, lr => lr.EmployeeType, (e, lr) => new { e = e, lrs = lr })
                                                                    .SelectMany(m => m.lrs.DefaultIfEmpty(), (x, y) => new { e = x.e, lr = y })
                                         .Join(db.LeaveRulesDetails, lr => lr.lr.RuleId, lrd => lrd.Id, (lr, lrd) => new { lr = lr.lr, e = lr.e, lrd = lrd })
                                         .Where(m => m.lr.IsDeleted == false && m.lrd.IsDeleted == false && m.e.Active == true &&
                                               (m.lrd.RuleType == RuleType.EarnedLeaves) &&
                                               (string.IsNullOrEmpty(search.Employee.Id) || m.e.Id == search.Employee.Id) &&
                                               (string.IsNullOrEmpty(search.Employee.UserName) || m.e.UserName.Contains(search.Employee.UserName)) &&
                                               (search.Rule.Id == new Guid() || m.lrd.Id == search.Rule.Id) &&
                                               (string.IsNullOrEmpty(search.Rule.Name) || m.lrd.Name.Contains(search.Rule.Name)) &&
                                               (DbFunctions.DiffDays(m.e.AddedOn, DateTime.Now) == ReturnDuration.Compile()(m.lrd.Duration) || (DbFunctions.DiffDays(GetLogDate(m.e.Id, db), DateTime.Now) > durations.Where(x => x.Name == m.lrd.LeaveType.ToString()).First().Value)))
                                         .Select(m => new EarnedLeaveViewModel

                                         {
                                             Employee = m.e,
                                             //Rule = m.lrd,
                                             ExistingLeaves = db.LeaveDetails.Where(x => x.EmployeeId == m.e.Id && x.Type == m.lrd.LeaveType).FirstOrDefault().Count
                                         });

                    return result.ToList() as List<EarnedLeaveViewModel>;


                    //return new List<EarnedLeaveViewModel>() as IQueryable<EarnedLeaveViewModel>;
                }
            }
            catch (Exception ex)
            {
                return new List<EarnedLeaveViewModel>();
            }
        }

        //private static Expression<Func<T, ApplicationDbContext, string>> BuildIt<T>(ApplicationDbContext db, string employeeId)
        //{
        //    var employeeIdFormat = string.Format(@"""EmployeeId"":""{0}""", employeeId);
        //    var lastManualUpdateLog = db.GeneralLogs.Where(m => m.Action == "Leave synchronized manually" && m.Table == "LeaveDetails" && m.Model.Contains(employeeIdFormat)).OrderByDescending(m => m.DateTime).First().DateTime;
        //    var lastAutomaticUpdateLog = db.GeneralLogs.Where(m => m.Action == "Leave synchronized automatically" && m.Table == "LeaveDetails" && m.Model.Contains(employeeIdFormat)).OrderByDescending(m => m.DateTime).First().DateTime;
        //    var paramExp1 = Expression.Parameter(typeof(ApplicationDbContext), "a");
        //    var paramExp2 = Expression.Parameter(typeof(string), "b");

        //    var body = BinaryExpression.GreaterThan(paramExp1, paramExp2);

        //    var lambda = Expression.Lambda<Func<T, T, T>>(body, paramExp1, paramExp2);

        //    lambda.Compile()

        //    return lambda;
        //}

        Expression<Func<ApplicationDbContext, string, DateTime>> GetGreaterCount = (db, employeeId) =>
                            db.GeneralLogs.Where(m => m.Action == "Leave synchronized automatically" && m.Table == "LeaveDetails" && m.Model.Contains(string.Format(@"""EmployeeId"":""{0}""", employeeId))).OrderByDescending(m => m.DateTime).First().DateTime > db.GeneralLogs.Where(m => m.Action == "Leave synchronized manually" && m.Table == "LeaveDetails" && m.Model.Contains(string.Format(@"""EmployeeId"":""{0}""", employeeId))).OrderByDescending(m => m.DateTime).First().DateTime ? db.GeneralLogs.Where(m => m.Action == "Leave synchronized automatically" && m.Table == "LeaveDetails" && m.Model.Contains(string.Format(@"""EmployeeId"":""{0}""", employeeId))).OrderByDescending(m => m.DateTime).First().DateTime : db.GeneralLogs.Where(m => m.Action == "Leave synchronized manually" && m.Table == "LeaveDetails" && m.Model.Contains(string.Format(@"""EmployeeId"":""{0}""", employeeId))).OrderByDescending(m => m.DateTime).First().DateTime;


        //public static IEnumerable<SelectListItem> GetTable <T> (this IQueryable<T> source)
        //{
        //    KeyValuePair<PropertyInfo, PropertyInfo> sourceDestMap1 = new KeyValuePair<PropertyInfo, PropertyInfo>(typeof(SelectListItem).GetProperty("Text"), typeof(T).GetProperty("Name"));
        //    KeyValuePair<PropertyInfo, PropertyInfo> sourceDestMap2 = new KeyValuePair<PropertyInfo, PropertyInfo>(typeof(SelectListItem).GetProperty("Value"), typeof(T).GetProperty("Id"));

        //    var paramExpr = Expression.Parameter(typeof(T), "i");

        //    var propertyA = Expression.Property(paramExpr, sourceDestMap1.Value);
        //    var propertyB = Expression.Property(paramExpr, sourceDestMap2.Value);

        //    var propertyBToString = Expression.Call(propertyB, typeof(object).GetMethod("ToString"));

        //    var createObject = Expression.New(typeof(SelectListItem));
        //    var InitializePropertiesOnObject = Expression.MemberInit(createObject, new[] {
        //        Expression.Bind(sourceDestMap1.Key, propertyA),
        //        Expression.Bind(sourceDestMap2.Key, propertyBToString)});

        //    var selectExpression = Expression.Lambda<Func<T, SelectListItem>>(InitializePropertiesOnObject, paramExpr);

        //    return source.Select(selectExpression).ToList();

        //}


        public DateTime GetLogDate(string employeeId, ApplicationDbContext db)
        {
            var employeeIdFormat = string.Format(@"""EmployeeId"":""{0}""", employeeId);
            var lastManualUpdateLog = db.GeneralLogs.Where(m => m.Action == "Leave synchronized manually" && m.Table == "LeaveDetails" && m.Model.Contains(employeeIdFormat)).OrderByDescending(m => m.DateTime).First();
            var lastAutomaticUpdateLog = db.GeneralLogs.Where(m => m.Action == "Leave synchronized automatically" && m.Table == "LeaveDetails" && m.Model.Contains(employeeIdFormat)).OrderByDescending(m => m.DateTime).First();
            return lastAutomaticUpdateLog.DateTime > lastManualUpdateLog.DateTime ? lastAutomaticUpdateLog.DateTime : lastManualUpdateLog.DateTime;
        }

        public Expression<Func<LeaveDurationType, int>> ReturnDuration = duration =>
        duration == LeaveDurationType.Weekly ? 7 :
        duration == LeaveDurationType.BiWeekly ? 14 :
        duration == LeaveDurationType.Monthly ? DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) :
        duration == LeaveDurationType.BiMonthly ? DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month) :
        duration == LeaveDurationType.Quarterly ? 365 / 4 :
        duration == LeaveDurationType.SemiAnnually ? 365 / 2 :
        duration == LeaveDurationType.Annually ? 365 :
        0;

        public Expression<Func<int, int, bool>> CompareLogDates(LeaveDurationType type)
        {
            var paramExprA = Expression.Parameter(typeof(int), "a");
            var paramExprB = Expression.Parameter(typeof(LeaveDurationType), "b");
            var paramExprC = Expression.Parameter(typeof(int), "c");

            var body = BinaryExpression.Equal(paramExprA, paramExprB);

            var lambda = Expression.Lambda<Func<int,
                int, bool>>(body, paramExprA, paramExprB);

            return lambda;
        }

        public static int SetDuration(LeaveDurationType duration)
        {
            switch (duration)
            {
                case LeaveDurationType.Weekly:
                    return 7;

                case LeaveDurationType.BiWeekly:
                    return 14;

                case LeaveDurationType.Monthly:
                    return DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

                case LeaveDurationType.BiMonthly:
                    return DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month);

                case LeaveDurationType.Quarterly:
                    return 365 / 4;

                case LeaveDurationType.SemiAnnually:
                    return 365 / 2;

                case LeaveDurationType.Annually:
                    return 365;

            }
            return 0;
        }
    }
}
