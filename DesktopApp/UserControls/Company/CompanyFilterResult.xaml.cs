﻿using BusinessLogic.CompanyLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AttendanceSystemWpf.UserControls.Company
{
    /// <summary>
    /// Interaction logic for CompanyFilterResult.xaml
    /// </summary>
    public partial class CompanyFilterResult : UserControl
    {
        public CompanyFilterResult()
        {
            InitializeComponent();
        }
        #region[Events]
        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            // ... Create a List of objects.
            Logic cl = new Logic();
            var result = cl.GetAllCompaniesLogic();
            // ... Assign ItemsSource of DataGrid.
            var grid = sender as DataGrid;
            grid.ItemsSource = result;

        }
        #endregion

       
    }
}
