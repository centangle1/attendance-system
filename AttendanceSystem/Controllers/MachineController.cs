﻿using AttendanceSystem.Models;
using BusinessLogic;
using CustomModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttendanceSystem.Controllers
{
    public class MachineController : Controller
    {
        // GET: Application
        public ActionResult Index()
        {
            return View(new Logic().ViewAllMachines());
        }
        
        public ActionResult Create()
        {
            var companyId = Singleton.Instance.CompanyId;
            Session["branchList"] = new Logic().GetBranches(new BranchSearchViewModel { CompanyId = companyId });
            return View();
        } 

        [HttpPost]
        public ActionResult Create(Machine model)
        {
            model.CompanyId = Singleton.Instance.CompanyId;
            var result = new Logic().AddMachine(model);
            if(result != new Guid())
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Edit(string macAddress)
        {
            var companyId = Singleton.Instance.CompanyId;
            var branchList = new Logic().GetBranches(new BranchSearchViewModel { CompanyId = companyId });
            Session["branchList"] = branchList;
            var result = new Logic().GetMachine(HttpUtility.UrlDecode(macAddress));
            return View("Create", result);
        }
        [HttpPost]
        public ActionResult Edit(Machine model)
        {
            var result = new Logic().Edit(model);
            if(result!=new Guid())
            {
                return RedirectToAction("Index");
            }
            return View("Create");
        }

        public ActionResult Delete(string macAddress)
        {
            var result = new Logic().GetMachine(HttpUtility.UrlDecode(macAddress));
            return View(result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = new Logic().Delete(id);
            if(result!=new Guid())
            {
                return RedirectToAction("Index");
            }
            return View(); 
        }

       
    }
}