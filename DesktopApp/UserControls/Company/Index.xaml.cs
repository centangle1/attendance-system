﻿using BusinessLogic.CompanyLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AttendanceSystemWpf.UserControls.Company
{
    /// <summary>
    /// Interaction logic for CompanyIndex.xaml
    /// </summary>
    public partial class Index : UserControl
    {
        public Index()
        {
            
            InitializeComponent();
            FilterResult.createcompany.Click += new RoutedEventHandler(createcompany_Click);
        }
        private void createcompany_Click(object sender, RoutedEventArgs e)
        {
            ContentArea.Children.Clear();
            var cc = new UserControls.Company.Create();
            ContentArea.Children.Add(cc);
        }

        private void FilterResult_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
