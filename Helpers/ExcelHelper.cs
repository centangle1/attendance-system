﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public static class ExcelHelper
    {
        public static void WriteToExcel(System.Data.DataTable dataTable, string worksheetName, string saveAsLocation, string ReporType, List<string> columnsToShow, bool isDaily, bool isUserSpecific, Dictionary<string, string> AdditionalProperties, List<string> HeaderColumns = null)
        {
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add(worksheetName);
                var excelSheet = excel.Workbook.Worksheets[worksheetName];

                excelSheet.Cells[1, 1].Value = ReporType;

                excelSheet.Cells[1, columnsToShow.Count].Value = "Date : " + DateTime.Now.ToShortDateString();
                string dateRange = "A" + 1 + ":" + Char.ConvertFromUtf32(columnsToShow.Count + 64) + 1;
                //Format Headers
                excelSheet.Cells[dateRange].Style.Font.Bold = true;
                excelSheet.Cells[dateRange].Style.Font.Size = 12;

                // loop through each row and add values to our sheet
                int rowcount = 2;
                var oldDate = new DateTime();
                if (isDaily && isUserSpecific == false)
                {
                    var date = dataTable.Rows[3][3].ToString();
                    oldDate = DateTime.Parse(date).Date;
                }
                // Show Headers
                int colIndex = 0;
                foreach (string col in columnsToShow)
                {
                    string HeaderName = null;
                    if (HeaderColumns != null)
                    {
                        HeaderName = HeaderColumns[colIndex];
                    }
                    colIndex++;


                    int index = dataTable.Columns.IndexOf(col);

                    if (!string.IsNullOrEmpty(HeaderName))
                        excelSheet.Cells[rowcount, colIndex].Value = HeaderName;
                    else
                        excelSheet.Cells[rowcount, colIndex].Value = dataTable.Columns[index].ColumnName;
                }
                string headerRange = "A" + rowcount + ":" + Char.ConvertFromUtf32(colIndex + 64) + rowcount;
                //Format Headers
                excelSheet.Cells[headerRange].Style.Font.Bold = true;
                excelSheet.Cells[headerRange].Style.Font.Size = 12;

                // Show Rows
                foreach (DataRow datarow in dataTable.Rows)
                {
                    rowcount += 1;
                    colIndex = 0;
                    foreach (string col in columnsToShow)
                    {
                        colIndex++;
                        int index = dataTable.Columns.IndexOf(col);
                        excelSheet.Cells[rowcount, colIndex].Value = datarow[index].ToString();
                        if (isDaily)
                        {
                            var newDate = DateTime.Parse(datarow[3].ToString()).Date;
                            if (oldDate.Date != newDate.Date && isUserSpecific == false)
                            {
                                ModifyRowForNewDate(ref rowcount, columnsToShow.Count(), newDate, excelSheet);

                                oldDate = DateTime.Parse(datarow[3].ToString()).Date;
                                break;
                            }

                            var statusIndex = dataTable.Columns.IndexOf("StatusString");
                            var status = datarow[statusIndex].ToString();
                            if (status != "Present")
                            {
                                var branchIndex = dataTable.Columns.IndexOf("BranchName");
                                var dateIndex = dataTable.Columns.IndexOf("StartTime");
                                if (dateIndex == -1)
                                {
                                    dateIndex = dataTable.Columns.IndexOf("Entry");
                                }

                                ModifyAbsentRecord(datarow, ref rowcount, columnsToShow.Count(), status, branchIndex, dateIndex, excelSheet);

                                break;
                            }
                        }

                    }
                }
                rowcount += 2;
                int additionalRowsIndex = rowcount;
                int additionalColIndex = 1;
                if (AdditionalProperties != null)
                {
                    excelSheet.Cells[rowcount, additionalColIndex].Value = "Summary";
                    string Range = "A" + rowcount + ":" + Char.ConvertFromUtf32(AdditionalProperties.Count + 64) + rowcount;
                    FormattingExcelCells(Range, "#cccccc", System.Drawing.Color.Black, true, excelSheet);

                    excelSheet.Cells[Range].Style.Font.Size = 14;

                    rowcount++;

                    foreach (var addProp in AdditionalProperties)
                    {
                        excelSheet.Cells[rowcount, additionalColIndex].Value = addProp.Key;

                        excelSheet.Cells[(rowcount + 1), additionalColIndex].Value = addProp.Value;
                        additionalColIndex++;
                    }

                    if (AdditionalProperties.Count > 0)
                    {
                        string SummaryRange = "A" + rowcount + ":" + Char.ConvertFromUtf32(AdditionalProperties.Count + 64) + rowcount;
                        //excelCellrange = excelSheet.Range[1];
                        excelSheet.Cells[SummaryRange].Style.Font.Bold = true;
                        excelSheet.Cells[SummaryRange].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    }
                }
                excelSheet.Cells[excelSheet.Dimension.Address].AutoFitColumns();
                FileInfo excelFile = new FileInfo(saveAsLocation);
                excel.SaveAs(excelFile);
            }
        }

        public static void FormattingExcelCells(string Range, string HTMLcolorCode, System.Drawing.Color fontColor, bool IsFontbool, ExcelWorksheet excelWorksheet)
        {
            excelWorksheet.Cells[Range].Style.Font.Color.SetColor(fontColor);
            excelWorksheet.Cells[Range].Style.Fill.PatternType = ExcelFillStyle.Solid;
            excelWorksheet.Cells[Range].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(HTMLcolorCode));
            if (IsFontbool == true)
            {
                excelWorksheet.Cells[Range].Style.Font.Bold = true;
            }
        }
        //
        //Function to modify rows for new dates
        public static void ModifyRowForNewDate(ref int rowcount, int columnsToShow, DateTime date, ExcelWorksheet excelSheet)
        {
            excelSheet.Cells[rowcount + 1, columnsToShow / 2].Value = date.Date.ToShortDateString();
            excelSheet.Cells[rowcount + 1, 1].Value = "Date";
            string Range = "A" + (rowcount + 1) + ":" + Char.ConvertFromUtf32(columnsToShow + 64) + (rowcount + 1);
            FormattingExcelCells(Range, "#cec8c8", System.Drawing.Color.Black, true, excelSheet);
            rowcount += 2;

        }

        //
        //Function to modify rows according to status
        public static void ModifyAbsentRecord(DataRow dataRow, ref int rowcount, int columnsToShow, string status, int branchIndex, int dateIndex, ExcelWorksheet excelSheet)
        {
            excelSheet.Cells[rowcount, 2].Value = dataRow[branchIndex].ToString();
            excelSheet.Cells[rowcount, 3].Value = dataRow[dateIndex].ToString();
            excelSheet.Cells[rowcount, 5].Value = status;
            string Range = "A" + rowcount + ":" + Char.ConvertFromUtf32(columnsToShow + 64) + rowcount;
            if (status == "Absent")
            {
                FormattingExcelCells(Range, "#c90808", System.Drawing.Color.White, true, excelSheet);
            }
            else if (status == "Holiday")
            {
                FormattingExcelCells(Range, "#ff9c3a", System.Drawing.Color.White, true, excelSheet);
            }
            else if (status == "OnLeave")
            {
                FormattingExcelCells(Range, "#deff84", System.Drawing.Color.White, true, excelSheet);
            }
        }
    }
}
