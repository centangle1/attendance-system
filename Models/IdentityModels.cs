﻿ using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CustomModels.Collection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using EnumLibrary;

namespace CustomModels.Identity
{
   public class ApplicationUser : IdentityUser
    {
        [ForeignKey("Department")]
        public Guid DepartmentId { get; set; }
        public Department Department { get; set; }

        [ForeignKey("Address")]
        public Guid AddressId { get; set; }
        public Address Address { get; set; }
        public bool Active { get; set; }
        public int MachineId { get; set; }
        public bool Permanant { get; set; }
        public EmployeeType Type { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("DefaultConnection", throwIfV1Schema:false) { }

        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<Leave> Leaves { get; set; }
        //public DbSet<LeaveAllowed> LeavesAllowed { get; set; }
        public DbSet<LeaveRule> LeaveRules { get; set; }
        public DbSet<LeaveRulesDetail> LeaveRulesDetails { get; set; }
        public DbSet<LeaveSynchroniztionLog> LeaveSynchroniztionLogs { get; set; }
        public DbSet<LeaveDetail> LeaveDetails { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<GeneralLog> GeneralLogs { get; set; }
        public DbSet<ExceptionLog> ExceptionLogs { get; set; }
        public DbSet<NationalHoliday> NationalHolidays { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<UserSchedule> UserSchedules { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<ScheduleDetail> ScheduleDetails { get; set; }
        public DbSet<Machine> Machines { get; set; }
        public DbSet<AttendancePolicy> AttendancePolicies { get; set; }
        public DbSet<EarnedLeaveDetail> EarnedLeaveDetails { get; set; }
        public DbSet<SchedulePolicy> SchedulePolicies { get; set; }
        public DbSet<CompanyPolicy> CompanyPolicies { get; set; }
        public DbSet<PartTimeUserDetail> PartTimeUserDetails { get; set; }
        public DbSet<Notification> Notifications { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
