﻿using CustomModels.Collection;
using EntityProvider;
using EnumLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<T> GetLeaveRuleDetail<T>(LeaveRulesDetailViewModel search)
        {
            return new DataAccess().GetLeaveRuleDetail<T>(search);
        }

        public Guid Create(LeaveRulesDetailViewModel model)
        {

            model.Id = Guid.NewGuid();
            return new DataAccess().Create(model);
        }

        public Guid Edit(LeaveRulesDetailViewModel model)
        {
            return new DataAccess().Edit(model);
        }
    }
}
