﻿using System;
using AttendanceSystem.Models;
using BusinessLogic;
using CustomModels.Collection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AttendanceUnitTest
{
    [TestClass]
    public class AttendanceTests
    {
        /// <summary>
        /// gets attendances when branchId is sent as a parameter and user is admin or company manager
        /// </summary>
        [TestMethod]
        public void GetAttendancesForBrancheAndAdmin()
        {
            var search = new SelectUserProperties { BranchId = Guid.Parse("7C81AF16-D073-42F0-B8BA-3F2878FE4D2F") };
            var details = new Helpers.Helpers().GetUserProperties("3e31bf6f-cd6e-4cb1-8fdb-3c895828b0e4");
            bool isUserAdminOrCompanyManager = true;
            search.BranchId = isUserAdminOrCompanyManager ? search.BranchId : details.BranchId;
            search.DepartmentId = isUserAdminOrCompanyManager ? search.DepartmentId : details.DepartmentId;
            search.UserId = isUserAdminOrCompanyManager ? search.UserId : details.UserId;
            search.UserRole = details.UserRole;
            //var result = new Logic().ViewAllAttendance(search);
        }
        /// <summary>
        /// gets attendances when department Id is sent as a parameter and user is admin or company manager
        /// </summary>
        [TestMethod]
        public void GetAttendancesForDepartmentAndAdmin()
        {
            var search = new SelectUserProperties { DepartmentId = Guid.Parse("B9A66169-CCCA-4F43-B988-70D1154ACACB") };
            var details = new Helpers.Helpers().GetUserProperties("3e31bf6f-cd6e-4cb1-8fdb-3c895828b0e4");
            bool isUserAdminOrCompanyManager = true;
            search.BranchId = isUserAdminOrCompanyManager ? search.BranchId : details.BranchId;
            search.DepartmentId = isUserAdminOrCompanyManager ? search.DepartmentId : details.DepartmentId;
            search.UserId = isUserAdminOrCompanyManager ? search.UserId : details.UserId;
            search.UserRole = details.UserRole;
            //var result = new Logic().ViewAllAttendance(search);
        }
        /// <summary>
        /// gets attendances when user Id is sent as a parameter and user is admin or company manager
        /// </summary>
        [TestMethod]
        public void GetAttendancesForUserAndAdmin()
        {
            var search = new SelectUserProperties { UserId = "3e31bf6f-cd6e-4cb1-8fdb-3c895828b0e4" };
            var details = new Helpers.Helpers().GetUserProperties("3e31bf6f-cd6e-4cb1-8fdb-3c895828b0e4");
            bool isUserAdminOrCompanyManager = true;
            search.BranchId = isUserAdminOrCompanyManager ? search.BranchId : details.BranchId;
            search.DepartmentId = isUserAdminOrCompanyManager ? search.DepartmentId : details.DepartmentId;
            search.UserId = isUserAdminOrCompanyManager ? search.UserId : details.UserId;
            search.UserRole = details.UserRole;
            //var result = new Logic().ViewAllAttendance(search);
        }
        /// <summary>
        /// gets attendances when branchId is sent as a parameter and user is not admin or company manager
        /// </summary>
        [TestMethod]
        public void GetAttendancesForBranch()
        {
            var search = new SelectUserProperties { BranchId = Guid.Parse("7C81AF16-D073-42F0-B8BA-3F2878FE4D2F") };
            var details = new Helpers.Helpers().GetUserProperties("3e31bf6f-cd6e-4cb1-8fdb-3c895828b0e4");
            bool isUserAdminOrCompanyManager = false;
            search.BranchId = isUserAdminOrCompanyManager ? search.BranchId : details.BranchId;
            search.DepartmentId = isUserAdminOrCompanyManager ? search.DepartmentId : details.DepartmentId;
            search.UserId = isUserAdminOrCompanyManager ? search.UserId : details.UserId;
            search.UserRole = details.UserRole;
            //var result = new Logic().ViewAllAttendance(search);
        }
        /// <summary>
        /// gets attendances when department Id is sent as a parameter and user is not admin or company manager
        /// </summary>
        [TestMethod]
        public void GetAttendancesForDepartment()
        {
            var search = new SelectUserProperties { DepartmentId = Guid.Parse("B9A66169-CCCA-4F43-B988-70D1154ACACB") };
            var details = new Helpers.Helpers().GetUserProperties("3e31bf6f-cd6e-4cb1-8fdb-3c895828b0e4");
            bool isUserAdminOrCompanyManager = false;
            search.BranchId = isUserAdminOrCompanyManager ? search.BranchId : details.BranchId;
            search.DepartmentId = isUserAdminOrCompanyManager ? search.DepartmentId : details.DepartmentId;
            search.UserId = isUserAdminOrCompanyManager ? search.UserId : details.UserId;
            search.UserRole = details.UserRole;
            //var result = new Logic().ViewAllAttendance(search);
        }
        /// <summary>
        /// gets attendances when user Id is sent as a parameter and user is not admin or company manager
        /// </summary>
        [TestMethod]
        public void GetAttendancesForUser()
        {
            var search = new SelectUserProperties { UserId = "3e31bf6f-cd6e-4cb1-8fdb-3c895828b0e4" };
            var details = new Helpers.Helpers().GetUserProperties("3e31bf6f-cd6e-4cb1-8fdb-3c895828b0e4");
            bool isUserAdminOrCompanyManager = false;
            search.BranchId = isUserAdminOrCompanyManager ? search.BranchId : details.BranchId;
            search.DepartmentId = isUserAdminOrCompanyManager ? search.DepartmentId : details.DepartmentId;
            search.UserId = isUserAdminOrCompanyManager ? search.UserId : details.UserId;
            search.UserRole = details.UserRole;
            //var result = new Logic().ViewAllAttendance(search);
        }
    }
}
