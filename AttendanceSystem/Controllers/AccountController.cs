﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using AttendanceSystem.Models;
using CustomModels.Identity;
using System.Collections.Generic;
using BusinessLogic;
using Microsoft.AspNet.Identity.EntityFramework;
using Helpers;
using CustomModels.Collection;
using EnumLibrary;

namespace AttendanceSystem.Controllers
{
    //[Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        //ApplicationUserManager UserManager = new ApplicationUserManager();
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            ApplicationUser signedUser = UserManager.FindByEmail(model.Email);
            var result = await SignInManager.PasswordSignInAsync(signedUser.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    var instance = Singleton.GetSingleton(model.Email);
                    //return RedirectToLocal(returnUrl);
                    return RedirectToAction("Index", "Home", null);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // Users Index
        [Authorize]
        public ActionResult Index()
        {
            var userProperties = new SelectUserProperties { UserId = Singleton.Instance.UserId, CompanyId = Singleton.Instance.CompanyId, BranchId = Singleton.Instance.BranchId, DepartmentId = Singleton.Instance.DepartmentId };
            var result = new List<CustomModels.Collection.RegisterViewModel>();
            if (User.IsInRole("Member"))
            {
                result = new UserLogic().GetAllUsers(UserType.Member, userProperties);
            }
            else if (User.IsInRole("Administrator") || User.IsInRole("Company Manager"))
            {
                result = new UserLogic().GetAllUsers(UserType.CompanyManager, userProperties);
            }
            else if (User.IsInRole("Branch Manager"))
            {
                result = new UserLogic().GetAllUsers(UserType.BranchManager, userProperties);
            }
            else if (User.IsInRole("Department Manager"))
            {
                result = new UserLogic().GetAllUsers(UserType.DepartmentManager, userProperties);
            }
            else if (User.IsInRole("Member"))
            {
                result = new UserLogic().GetAllUsers(UserType.Member, userProperties);
            }
            if (result != new List<CustomModels.Collection.RegisterViewModel>())
            {
                return View(result);
            }
            return View();
        }

        public ActionResult Index1(Guid id)
        {
            var result = new UserLogic().GetAllUsers(id);
            return View("Index", result);
        }


        //
        // GET: /Account/AddAdminUser
        [AllowAnonymous]
        public ActionResult AddAdminUser()
        {
            var branchList = new Logic().GetBranches(new BranchSearchViewModel());
            TempData["branchList"] = branchList;
            TempData["roles"] = new Helpers.Helpers().GetAllRoles();
            TempData["schedules"] = new Logic().GetAllSchedule();
            return View();
        }


        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            TempData["branchList"] = new Logic().GetBranches(new BranchSearchViewModel());
            TempData["roles"] = new Helpers.Helpers().GetAllRoles();
            TempData["schedules"] = new Logic().GetAllSchedule();
            TempData["action"] = "Register";
            return View("Edit");
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(CustomModels.Collection.RegisterViewModel model)
        {
            TempData["action"] = "Register";
            TempData["roles"] = new Helpers.Helpers().GetAllRoles();
            if (((List<IdentityRole>)TempData["roles"]).Count() == 0)
            {
                var companyId = new Logic().CreateCompany(new CompanyViewModel { Id = Guid.NewGuid(), Active = true, Name = "Admin", LogoUrl = "/Images/Company_Logo/Centangle.png" });
                var branchId = new Logic().AddBranch(new Branch { Active = true, Address = model.Address, CompanyId = companyId, Id = Guid.NewGuid(), Name = "Admin" });
                var departmentId = new Logic().CreateDepartment(new Department { Active = true, BranchCode = branchId, Id = Guid.NewGuid(), Name = "Admin" });
                model.DepartmentId = departmentId;
            }
            ModelState.Remove("ScheduleId");
            if (ModelState.IsValid || ((List<IdentityRole>)TempData["roles"]).Count() == 0)
            {
                var checkExistingRecord = new UserLogic().GetUserByEmail(model.Email);
                if (checkExistingRecord != null)
                {
                    model.Id = checkExistingRecord.Id;
                    var result = new UserLogic().EditUser(model, HttpContext.GetOwinContext());
                    if (!string.IsNullOrEmpty(result.ToString()))
                    {
                        AddLeaveDetails(model.LeaveDetail.LeaveTypeDetailList, result.ToString());
                        return RedirectToAction("Index");
                    }
                    return View("Error");
                }
                else
                {
                    model.Address.Id = Guid.NewGuid();
                    var department = model.DepartmentId != new Guid() ? model.DepartmentId : Guid.Parse(TempData["departmentId"].ToString());
                    var user = new ApplicationUser
                    {
                        Id = Guid.NewGuid().ToString(),
                        UserName = model.Name,
                        PhoneNumber = model.PhoneNumber,
                        Email = model.Email,
                        Active = true,
                        Address = model.Address,
                        AddressId = model.Address.Id,
                        DepartmentId = model.DepartmentId,
                        MachineId = model.MachineId,
                        Permanant = model.Permanant,
                        Type = model.Type,
                        AddedOn = DateTime.Now,
                        UpdatedOn = DateTime.Now
                    };

                    var result = UserManager.Create(user, model.Password);


                    if (result.Succeeded)
                    {
                        AddLeaveDetails(model.LeaveDetail.LeaveTypeDetailList, user.Id);

                        if (model.Permanant)
                        {
                            var scheduleId = new Helpers.Helpers().GetScheduleIdByName(model.Schedule);
                            new Logic().AddUserSchedule(scheduleId, user.Id);
                        }
                        else
                        {
                            model.Hours = model.Hours != 0 ? model.Hours : 40;

                            var details = new PartTimeUserDetail
                            {
                                UserId = user.Id,
                                Id = Guid.NewGuid(),
                                Active = true,
                                WorkingHours = model.Hours + ":" + model.Minutes
                                //WorkingHours = "40" + ":" + "00"
                            };
                            var check = new Logic().AddPartTimeDetails(details);
                        }
                        //await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                        try
                        {
                            using (ApplicationDbContext db = new ApplicationDbContext())
                            {
                                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

                                if (!(roleManager.RoleExists("Company Manager") && roleManager.RoleExists("Branch Manager") && roleManager.RoleExists("Department Manager")
                                    && roleManager.RoleExists("Member")))
                                {
                                    roleManager.Create(new IdentityRole("Administrator"));
                                    roleManager.Create(new IdentityRole("Member"));
                                    roleManager.Create(new IdentityRole("Company Manager"));
                                    roleManager.Create(new IdentityRole("Branch Manager"));
                                    roleManager.Create(new IdentityRole("Department Manager"));
                                    userManager.AddToRole(user.Id, "Administrator");
                                }
                                else
                                {
                                    userManager.AddToRole(user.Id, model.Roles);
                                }
                            }

                        }
                        catch (Exception ex)
                        {

                        }

                        return RedirectToAction("Index", "Account");
                    }
                    AddErrors(result);
                }
            }
            // If we got this far, something failed, redisplay form
            TempData["branchList"] = new Logic().GetBranches(new BranchSearchViewModel());
            TempData["roles"] = new Helpers.Helpers().GetAllRoles();
            TempData["schedules"] = new Logic().GetAllSchedule();
            return View("Edit", model);
        }

        public void AddLeaveDetails(List<LeaveTypeDetailViewModel> leaveDetails, string userId)
        {
            foreach (var item in leaveDetails)
            {
                new Logic().Update(new LeaveDetailViewModel
                {
                    EmployeeId = userId,
                    Count = item.Count,
                    Description = "Leave details added on employee creation",
                    Type = item.Type
                }, false);
            }
        }

        //
        // GET: /Account/Edit
        public ActionResult Edit(string id)
        {
            var branchList = new Logic().GetBranches(new BranchSearchViewModel { IncludeRoleCondition = true});
            TempData["branchList"] = branchList;
            TempData["roles"] = new Helpers.Helpers().GetAllRoles();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var role = userManager.GetRoles(id).FirstOrDefault();
            var result = new UserLogic().GetUser(id);
            TempData["schedules"] = new Logic().GetAllSchedule();
            TempData["departmentId"] = id;
            var time = result.TimeString.Split(':');
            var user = new CustomModels.Collection.RegisterViewModel
            {
                Id = result.Id,
                Active = result.Active,
                Address = result.Address,
                AddressId = result.AddressId,
                Department = result.Department,
                BranchCode = result.BranchCode,
                Permanant = result.Permanant,
                MachineId = result.MachineId,
                DepartmentId = result.Department.Id,
                Email = result.Email,
                Name = result.Name,
                Hours = int.Parse(time[0]),
                Minutes = int.Parse(time[1]),
                PhoneNumber = result.PhoneNumber,
                ScheduleId = result.ScheduleId,
                Type = result.Type,
                Roles = role
            };
            return View(user);
        }

        //
        // POST: /Account/Edit
        [HttpPost]
        public ActionResult Edit(CustomModels.Collection.RegisterViewModel model)
        {
            var result = new UserLogic().EditUser(model, HttpContext.GetOwinContext());
            if (result != new Guid())
                return RedirectToAction("Index1", new { id = model.DepartmentId });
            else
                return View(model);
        }

        //
        //Delete User
        public ActionResult Delete(string id)
        {
            var result = new UserLogic().Delete(id);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            return View("Error");

        }

        //
        //Reset Password
        public ActionResult ResetUserPassword(string id, Guid departmentId)
        {
            TempData["token"] = UserManager.GeneratePasswordResetToken(id);
            TempData["userId"] = id;
            Session["departmentId"] = departmentId;
            return View();
        }

        [HttpPost]
        public ActionResult ResetUserPassword(ResetUserPassword model)
        {

            var result = UserManager.ResetPassword(TempData["userId"].ToString(), TempData["token"].ToString(), model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }


        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", new { });
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}