﻿using CustomModels.Collection;
using EntityProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetNotifications<T>(NotificationViewModel search)
        {
            return new DataAccess().GetNotifications<T>(search);
        }

        public Guid Create(NotificationViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.DateTime = DateTime.Now;
            return new DataAccess().Create(model);
        }

        public Guid Edit(NotificationViewModel model)
        {
            return new DataAccess().Edit(model);
        }
    }
}
