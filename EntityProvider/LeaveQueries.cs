﻿using EnumLibrary;
using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public List<T> GetLeaves<T>(LeaveViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    IQueryable<LeaveViewModel> filteredIQueryableResult = new List<LeaveViewModel>() as IQueryable<LeaveViewModel>;
                    IQueryable<LeaveViewModel> queryableResult = db.Leaves.Join(db.Users, l => l.UserId, u => u.Id, (l, u) => new { l, u })
                            .GroupJoin(db.Users, l => l.l.AddedBy, lab => lab.Id, (l, lab) => new { l = l.l, u = l.u, lab = lab.FirstOrDefault() })
                            .Where(m => m.l.Active && m.u.Active &&
                                              (search.Id == new Guid() || m.l.Id == search.Id) &&
                                              (string.IsNullOrEmpty(search.UserId) || m.l.UserId == search.UserId))
                                              .Select(m => new LeaveViewModel
                                              {
                                                  Active = m.l.Active,
                                                  Id = m.l.Id,
                                                  EndingDate = m.l.EndingDate,
                                                  StartingDate = m.l.StartingDate,
                                                  UserId = m.l.UserId,
                                                  User = m.u,
                                                  Status = m.l.Status,
                                                  Type = m.l.Type,
                                                  AddedBy = m.lab,
                                                  Note = m.l.Note,
                                                  DepartmentId = m.u.DepartmentId
                                              }) as IQueryable<LeaveViewModel>;
                    if (search.GetOverLappedLeave)
                    {
                        filteredIQueryableResult = queryableResult
                            .Where(m =>       (search.SkipLeave == new Guid() || m.Id != search.SkipLeave) &&
                                              (
                                              (search.StartingDate == new DateTime() && search.EndingDate == new DateTime() && string.IsNullOrEmpty(search.UserId)) ||
                                              (search.StartingDate <= m.EndingDate && m.StartingDate <= search.EndingDate)
                                              )
                                              );
                    }
                    else
                    {
                        filteredIQueryableResult = queryableResult.Where(m =>
                                                    (search.SkipLeave == new Guid() || m.Id != search.SkipLeave) &&
                                                    (search.StartingDate == new DateTime() || (search.StartingDate >= m.StartingDate && search.StartingDate <= m.EndingDate)) &&
                                                    (search.EndingDate == new DateTime() || (m.StartingDate < search.EndingDate && search.EndingDate <= m.EndingDate)));
                    }
                    if (typeof(T) == typeof(LeaveViewModel))
                    {
                        return filteredIQueryableResult.ToList() as List<T>;
                    }
                    if (typeof(T) == typeof(Leave))
                    {
                        return queryableResult.Select(m => new
                        {
                            Status = m.Status,
                            Active = m.Active,
                            EndingDate = m.EndingDate,
                            DEndingDate = m.EndingDate,
                            StartingDate = m.StartingDate,
                            DStartingDate = m.StartingDate,
                            Id = m.Id,
                            Type = m.Type,
                            UserId = m.UserId,
                            User = m.User,
                            AddedBy = m.AddedBy,
                            Note = m.Note,
                        }).ToList()
                    .Select(m => new Leave
                    {
                        Status = m.Status,
                        Active = m.Active,
                        EndingDate = m.EndingDate,
                        DEndingDate = m.EndingDate,
                        StartingDate = m.StartingDate,
                        DStartingDate = m.StartingDate,
                        Id = m.Id,
                        Type = m.Type,
                        UserId = m.UserId,
                        User = m.User,
                        Note = m.Note,
                    }).ToList() as List<T>;
                    }
                    return new List<T>();
                }
            }
            catch (Exception ex)
            {
                return new List<T>();
            }
        }

        public Guid CreateLeave(LeaveViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                { 
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<LeaveViewModel, Leave>().ForMember(d => d.AddedBy, x => x.MapFrom(s => s.AddedBy.Id)); });
                    IMapper iMapper = config.CreateMapper();
                    var leaveObject = iMapper.Map<LeaveViewModel, Leave>(model);
                    leaveObject.Active = true;
                    leaveObject.Id = Guid.NewGuid();
                    var result = db.Leaves.Add(leaveObject);
                    db.SaveChanges();
                    return result.Id;
                }
            }

            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public LeaveViewModel EditLeave(LeaveViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Leaves.Find(model.Id);
                    //var config = new MapperConfiguration(cfg => {
                    //    cfg.CreateMap<Leave, LeaveViewModel>().ForMember(dest => dest, x => x.ResolveUsing(src => new ApplicationUser() { Id = src.AddedBy })).ForAllMembers(m => m.MapFrom(dest=>dest));
                    //});
                    //var iMapper = config.CreateMapper();
                    //var record = iMapper.Map<Leave, LeaveViewModel>(result);
                    if (result.Active != false)
                    {
                        result.StartingDate = model.StartingDate != new DateTime() ? model.StartingDate : result.StartingDate;
                        result.EndingDate = model.EndingDate != new DateTime() ? model.EndingDate : result.EndingDate;
                        result.Note = string.IsNullOrEmpty(model.Note)? result.Note : model.Note;
                        result.UpdatedBy = string.IsNullOrEmpty(model.UpdatedBy) ? result.UpdatedBy : model.UpdatedBy;
                        result.UpdatedOn = model.UpdatedOn == new DateTime() ? model.UpdatedOn : result.UpdatedOn;
                        db.Entry(result).State = EntityState.Modified;
                        db.SaveChanges();
                        return model;
                    }
                    else
                    {
                        return new LeaveViewModel();
                    }
                }
            }
            catch (Exception ex)
            {
                return new LeaveViewModel();
            }
        }

        public Guid DeleteLeave(LeaveViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Leaves.Find(model.Id);
                    if (result.Active != false)
                    {
                        result.Active = false;
                        db.Entry(result).State = EntityState.Modified;
                        db.SaveChanges();
                        return Guid.NewGuid();
                    }
                    else
                        return new Guid();
                }
            }
            catch (Exception)
            {
                return new Guid();
            }
        }


    }
}
