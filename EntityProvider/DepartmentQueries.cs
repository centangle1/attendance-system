﻿using CustomModels.Collection;
using CustomModels.Identity;
using EnumLibrary;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EntityProvider
{
    public partial class DataAccess
    {
        /// <summary>
        /// Gets all the department that fulfil the conditions in the parameter, 
        /// for Administrator all the records, 
        /// for Company Manager all branches of the company, 
        /// for Branch Manager all the departments of his branch 
        /// and for Members or Department Managers their own respective departments
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="departmentId"></param>
        /// <param name="userType"></param>
        /// <param name="details"></param>
        /// <returns>list of filtered departments</returns>
        public List<Department> GetDepartments(Guid? branchId, Guid? departmentId, UserType userType, SelectUserProperties details)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Departments
                       .Join(db.Branches, d => d.BranchCode, b => b.Id, (d, b) => new { d, b })
                       .Join(db.Companies, b => b.b.CompanyId, c => c.Id, (b, c) => new { b = b.b, c, d = b.d })
                                       .Where(d => d.d.Active == true && d.b.Active == true && d.c.Active == true &&
                                           (
                                               (departmentId == null || d.d.Id == departmentId)
                                               &&
                                               (branchId == null || d.b.Id == branchId)
                                           )
                                               ||
                                           (

                                               (userType == UserType.Administrator && departmentId != null && branchId != null)
                                               ||
                                               (userType == UserType.CompanyManager && d.c.Id == details.CompanyId)
                                               ||
                                               (userType == UserType.BranchManager && d.b.Id == details.BranchId)
                                               ||
                                               ((userType == UserType.DepartmentManager || userType == UserType.Member) && d.d.Id == details.DepartmentId)
                                           )
                                       )
                                       .Select(m => new
                                       {
                                           Name = m.d.Name,
                                           BranchCode = m.d.BranchCode,
                                           BranchName = m.b.Name,
                                           Id = m.d.Id
                                       }).ToList()
                                       .Select(n => new Department
                                       {
                                           Name = n.Name,
                                           BranchCode = n.BranchCode,
                                           BranchName = n.BranchName,
                                           Id = n.Id
                                       }).ToList();
                    return result;

                }
            }

            catch (Exception)
            {
                return new List<Department>();
            }
        }

        public Guid AddDepartment(Department model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    model.Active = true;
                    model.Id = Guid.NewGuid();
                    var result = db.Departments.Add(model);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception)
            {
                return new Guid();

            }

        }

        public Guid EditDepartment(Department model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var data = db.Departments.Find(model.Id);
                    data.Name = model.Name;
                    data.Active = true;
                    data.BranchCode = model.BranchCode;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    return data.BranchCode;
                }
            }
            catch (Exception)
            {
                return new Guid();
            }
        }

        public Guid DeleteDepartment(Department model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var data = db.Departments.Find(model.Id);
                    if (data != null)
                    {
                        data.Active = false;
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        return data.Id;
                    }
                    return new Guid();
                }
            }

            catch (Exception ex)
            {
                return new Guid();
            }
        }

    }
}
