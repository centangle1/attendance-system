﻿using EnumLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foolproof;
using CustomModels.Collection;

namespace CustomModels.ReportModels

{
    public class DailyReportModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public WeekDays Day { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime Entry { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime Exit { get; set; }
        public bool Present { get; set; }
        public TimeSpan ArrivalTime { get; set; }
        public LateEarlyPolicy Arrival { get; set; }
        [Display(Name = "Arrival")]
        public string ArrivalString
        {
            get
            {
                return Arrival.ToString();
            }
        }
        public TimeSpan DepartureTime { get; set; }
        public LateEarlyPolicy Departure { get; set; }
        [Display(Name = "Departure")]
        public string DepartureString
        {
            get
            {
                return Departure.ToString();
            }
        }
        public TimeSpan Hours { get; set; }
        public bool Holiday { get; set; }
        [Display(Name = "Arrival Delay")]
        public TimeSpan ArrivalDelay { get; set; }
        [Display(Name = "Early Arrival")]
        public TimeSpan EarlyArrival { get; set; }
        [Display(Name = "Departure Delay")]
        public TimeSpan DepartureDelay { get; set; }
        [Display(Name = "Early Departure")]
        public TimeSpan EarlyDeparture { get; set; }
        [Display(Name = "Scheduled Hours")]
        public TimeSpan ScheduledHours { get; set; }
        [Display(Name = "Total Hours")]
        public TimeSpan TotalHours { get; set; }
        [Display(Name = "Working Hours")]
        public TimeSpan WorkingHours { get; set; }
        public TimeSpan Overtime { get; set; }
        public AttendanceStatus Status { get; set; }
        public TimeSpan ActualOverTime { get; set; }
        public string ActualOverTimeString {
            get
            {
                var hours = ActualOverTime.TotalHours > 0 ? Math.Floor(ActualOverTime.TotalHours) : Math.Ceiling(ActualOverTime.TotalHours);

                return string.Format("{0:00}:{1:00}", hours, ActualOverTime.Minutes);
            }
        }
        [Display(Name = "Status")]
        public string StatusString
        {
            get
            {
                return Status.ToString();
            }
        }
        [Display(Name = "Working Hours")]
        public string WorkingHoursString
        {
            get
            {
                return string.Format("{0:00}:{1:00}", WorkingHours.TotalHours, WorkingHours.Minutes);
            }
        }
        [Display(Name = "OverTime")]
        public string OvertimeString
        {
            get
            {
                return string.Format("{0:00}:{1:00}", Overtime.TotalHours, Overtime.Minutes);
            }
        }
        [Display(Name = "Presents")]
        public int PresentCount { get; set; }
        [Display(Name = "Absents")]
        public int AbsentCount { get; set; }
        [Display(Name = "Leaves")]
        public int OnLeaveCount { get; set; }
        [Display(Name = "Late Arrivals")]
        public int LateArrivalCount { get; set; }
        [Display(Name = "Early Arrivals")]
        public int EarlyArrivalCount { get; set; }
        [Display(Name = "On Time Arrivals")]
        public int OnTimeArrivalCount { get; set; }
        [Display(Name = "Late Departures")]
        public int LateDepartureCount { get; set; }
        [Display(Name = "Early Departures")]
        public int EarlyDepartureCount { get; set; }
        [Display(Name = "On Time Departures")]
        public int OnTimeDepartureCount { get; set; }
        public bool IsUserSpecific { get; set; }
        public List<ScheduleDetailReport> ScheduleDetails { get; set; }
        [Display(Name = "Branch")]
        public string BranchName { get; set; }
    }

    public class ScheduleDetailReport
    {
        public TimeSpan StartingTime { get; set; }

        public TimeSpan EndingTime { get; set; }

        public LateEarlyPolicy ArrivalStatus { get; set; }

        public LateEarlyPolicy DepartureStatus { get; set; }

        public TimeSpan ArrivalDifference { get; set; }

        public TimeSpan DepartureDiffernce { get; set; }
    }

    public class ConsolidatedReportModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Presents { get; set; }
        public int Absents { get; set; }
        [Display(Name ="National Holidays")]
        public int NationalHolidays { get; set; }
        public int Holidays { get; set; }
        public int Leaves { get; set; }
        public TimeSpan WorkedHours { get; set; }
        public TimeSpan ScheduledHours { get; set; }
        public TimeSpan TotalHours { get; set; }
        public TimeSpan Late { get; set; }
        public TimeSpan OverTime { get; set; }
        public TimeSpan ActualOverTime { get; set; }
        public string ActualOverTimeString
        {
            get
            {
                return string.Format("{0:00}:{1:00}", Math.Floor(ActualOverTime.TotalHours), ActualOverTime.Minutes);
            }
        }
        [Display(Name ="Worked Hours")]
        public string FormattedWorkedHours
        {
            get
            {
                return string.Format("{0:00}:{1:00}", WorkedHours.TotalHours, WorkedHours.Minutes);
            }
        }
        [Display(Name = "Scheduled Hours")]
        public string FormattedScheduledHours
        {
            get
            {
                return string.Format("{0:00}:{1:00}", ScheduledHours.TotalHours, ScheduledHours.Minutes);
            }
        }
        [Display(Name = "Total Hours")]
        public string FormattedTotalHours
        {
            get
            {
                return string.Format("{0:00}:{1:00}", TotalHours.TotalHours, TotalHours.Minutes);
            }
        }
        [Display(Name = "Late")]
        public string FormattedLate
        {
            get
            {
                return string.Format("{0:00}:{1:00}", Late.TotalHours, Late.Minutes);
            }
        }
        [Display(Name = "OverTime")]
        public string FormattedOvertime
        {
            get
            {
                return string.Format("{0:00}:{1:00}", OverTime.TotalHours, OverTime.Minutes);
            }
        }
        [Display(Name = "Branch")]
        public string BranchName { get; set; }
    }

    public class ConsolidatedReportPartTimeModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Presents { get; set; }
        public int Absents { get; set; }
        public int Leaves { get; set; }
        public TimeSpan WorkedHours { get; set; }
        public TimeSpan TotalHours { get; set; }
        public TimeSpan OverTime { get; set; }
        [Display(Name = "Worked Hours")]
        public string FormattedWorkedHours
        {
            get
            {
                return string.Format("{0:00}:{1:00}", Math.Floor(WorkedHours.TotalHours), WorkedHours.Minutes);
            }
        }
        [Display(Name = "Total Hours")]
        public string FormattedTotalHours
        {
            get
            {
                return string.Format("{0:00}:{1:00}", Math.Floor(TotalHours.TotalHours), TotalHours.Minutes);
            }
        }
        [Display(Name = "OverTime")]
        public string FormattedOvertime
        {
            get
            {
                return string.Format("{0:00}:{1:00}", Math.Floor(OverTime.TotalHours), OverTime.Minutes);
            }
        }
    }

    public class ReportDates
    {
        public ReportSelection Selection { get; set; }

        public DateTime Month { get; set; }

        public string Week { get; set; }

        public string LoggedInUser { get; set; }

        public string PartTime { get; set; }

        public ReportStatusSelection Status { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartTime { get; set; }

        [GreaterThanOrEqualTo("StartTime")]
        [DataType(DataType.Date)]
        public DateTime? EndTime { get; set; }

        public string Company { get; set; }

        private Guid _companyId = new Guid();
        public Guid CompanyId
        {
            get
            {
                if ((Company != null && Company != "") && _companyId == new Guid())
                    return Guid.Parse(Company);

                else
                    return _companyId;
            }
            set
            {
                _companyId = value;
            }

        }

        public string Branch { get; set; }
        private Guid _branchId;
        public Guid BranchId
        {
            get
            {
                if ((Branch != null && Branch != "") && _branchId == new Guid())
                {
                    return Guid.Parse(Branch);
                }
                else
                    return _branchId;
            }
            set
            {
                _branchId = value;
            }
        }

        public string Department { get; set; }
        private Guid _departmentId;
        public Guid DepartmentId
        {
            get
            {
                if ((Department != null && Department != "") && _departmentId == new Guid())
                    return Guid.Parse(Department);
                else
                    return _departmentId;
            }
            set
            {
                _departmentId = value;
            }
        }

        public string User { get; set; }

        public string BranchName { get; set; }

        public bool GetPresentEmployees { get; set; }

    }

    public class Years
    {
        public int Id { get; set; }
        public int Value { get; set; }
    }

    public class SearchValue
    {
        private Guid _id;
        public Guid Id
        {
            get
            {
                if (this._id == new Guid())
                {
                    try
                    {
                        return Guid.Parse(this.strId);
                    }
                    catch
                    {
                        return new Guid();
                    }
                }
                return this._id;
            }
            set
            {
                _id = value;
            }
        }
        public string strId { get; set; }
        public string Name { get; set; }
    }

    public class AttendanceFormat
    {
        public string MacAddress { get; set; }
        public string Error { get; set; }
        public IList<string> UserId { get; set; }
        public IList<DateTime> DateTime { get; set; }
    }

    public class ExcelSheet
    {
        public string Company { get; set; }
        public string Description { get; set; }
        public string Branch { get; set; }
        public string BCity { get; set; }
        public string BArea { get; set; }
        public string BStreet { get; set; }
        public string BHouse { get; set; }
        public string Department { get; set; }
        public string Employee { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string ECity { get; set; }
        public string EArea { get; set; }
        public string EStreet { get; set; }
        public string EHouse { get; set; }
        public int MachineId { get; set; }
        public string Role { get; set; }
        public string Schedule { get; set; }
        public string AddedOn { get; set; }

    }

    public class DailyReportPartTimeModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
        [Display(Name = "Start Time")]
        public DateTime StartTime { get; set; }
        [Display(Name = "End Time")]
        public DateTime EndTime { get; set; }

        public AttendanceStatus Status { get; set; }

        public int Presents { get; set; }

        public int Absents { get; set; }

        public int Leaves { get; set; }

        public TimeSpan TotalHours { get; set; }
        [Display(Name = "Branch")]
        public string BranchName { get; set; }

        public bool IsUserSpecific { get; set; }

        public TimeSpan WorkedHours { get; set; }

        public TimeSpan ActualHoursWorked { get; set; }

        public TimeSpan Overtime { get; set; }
        [Display(Name = "Status")]
        public string StatusString
        {
            get
            {
                return Status.ToString();
            }
        }
        [Display(Name = "Total Hours")]
        public string TotalHoursString
        {
            get
            {
                return string.Format("{0:00}:{1:00}", Math.Floor(TotalHours.TotalHours), TotalHours.Minutes);
            }
        }
        [Display(Name = "Worked Hours")]
        public string WorkedHoursString
        {
            get
            {
                return string.Format("{0:00}:{1:00}", Math.Floor(WorkedHours.TotalHours), WorkedHours.Minutes);
            }
        }
        [Display(Name = "OverTime")]
        public string OvertimeString
        {
            get
            {
                return string.Format("{0:00}:{1:00}", Math.Floor(Overtime.TotalHours), Overtime.Minutes);
            }
        }


    }

    public class ExcelReportLists
    {
        public List<DailyReportModel> DailyList { get; set; }

        public List<ConsolidatedReportModel> MonthlyList { get; set; }

        public List<DailyReportPartTimeModel> DailyPartTimeList { get; set; }

        public List<ConsolidatedReportPartTimeModel> MonthlyPartTimeList { get; set; }
    }

    public class IndexViewModel
    {
        public string OptionId { get; set; }
    }

    public class Select2OptionModel
    {
        public string id { get; set; }

        public string text { get; set; }
    }

    public class Select2PagedResult
    {
        public int Total { get; set; }

        public List<Select2OptionModel> Results { get; set; }
    }

    public class PieChartResult
    {
        public string Title { get; set; }
        public float Presents { get; set; }
        public float Absents { get; set; }
        public float Leaves { get; set; }
        public Guid BranchId { get; set; }
    }

}
