﻿using AutoMapper;
using BusinessLogic;
using CustomModels.Collection;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttendanceSystem.Controllers.CompanyController
{
    public class CompanyController : Controller
    {
        // GET: Company
        [Authorize]
        public ActionResult Index()
        {
            var result = new Logic().GetCompanies(new CompanySearchViewModel { GetAllCompanies=true});

            if (result != new List<Company>())
            {
                return View(result);
            }
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult Create(CompanyViewModel model, HttpPostedFileBase logo)
        {
            if (logo != null)
            {
                var pic = System.IO.Path.GetFileName(model.Logo.FileName);
                var path = System.IO.Path.Combine(
                                   Server.MapPath("~/Images/Company_Logo"), pic);

                model.Logo.SaveAs(path);
                model.LogoUrl = "/Images/Company_Logo/" + pic;
            }

            var result = new Logic().CreateCompany(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(Guid id)
        {
            var result = new Logic().GetCompanies(new CompanySearchViewModel { Id = id }).FirstOrDefault();
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Company, CompanyViewModel>(); });
            var iMapper = config.CreateMapper();
            var model = result != null ? iMapper.Map<Company, CompanyViewModel>(result) : new CompanyViewModel();
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult Edit(CompanyViewModel model, HttpPostedFileBase logo)
        {
            if (logo != null)
            {
                var pic = System.IO.Path.GetFileName(model.Logo.FileName);
                var path = System.IO.Path.Combine(
                                   Server.MapPath("~/Images/Company_Logo"), pic);

                model.Logo.SaveAs(path);
                model.LogoUrl = "/Images/Company_Logo/" + pic;
            }
            var result = new Logic().EditCompany(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(Guid id)
        {
            return View(new Logic().GetCompanies(new CompanySearchViewModel { Id = id }).FirstOrDefault());
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult Delete(Company model)
        {
            Logic lg = new Logic();
            var result = lg.DeleteCompany(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Details(Guid id)
        {
            return View(new Logic().GetCompanies(new CompanySearchViewModel { Id = id }).FirstOrDefault());
        }

        public ActionResult PolicyIndex(Guid id)
        {
            TempData["companyId"] = id;
            var result = new Logic().GetCompanyPolicies(new CompanyPolicySearchViewModel { CompanyId = id });
            return View(result);
        }

        [Authorize(Roles = "Administrator, Company Manager")]
        public ActionResult AddPolicy(Guid id)
        {
            Session["id"] = id;
            return View();
        }

        [Authorize(Roles = "Administrator, Company Manager")]
        [HttpPost]
        public ActionResult AddPolicy(CompanyPolicy model)
        {
            model.CompanyId = Guid.Parse(Session["id"].ToString());
            model.Active = true;
            var result = new Logic().AddPolicy(model);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrator, Company Manager")]
        public ActionResult EditPolicy(Guid id)
        {
            return View(new Logic().GetCompanyPolicies(new CompanyPolicySearchViewModel { Id = id }).First());
        }

        [Authorize(Roles = "Administrator, Company Manager")]
        [HttpPost]
        public ActionResult EditPolicy(CompanyPolicy model)
        {
            model.CompanyId = Guid.Parse(Session["id"].ToString());
            var result = new Logic().EditPolicy(model);
            return RedirectToAction("PolicyIndex", new { id = model.CompanyId });
        }

        [Authorize(Roles = "Administrator, Company Manager")]
        public ActionResult DeletePolicy(Guid id)
        {
            return View(new Logic().GetCompanyPolicies(new CompanyPolicySearchViewModel { Id = id }));
        }

        [Authorize(Roles = "Administrator, Company Manager")]
        [HttpPost]
        public ActionResult DeletePolicy(CompanyPolicy model)
        {
            new Logic().DeletePolicy(model);
            return RedirectToAction("Index");
        }

    }
}