﻿using System;
using BusinessLogic;
using CustomModels.Collection;
using CustomModels.Identity;
using EnumLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AttendanceUnitTest
{
    [TestClass]
    public class NotificationTesting
    {
        //[TestMethod]
        //public void Create()
        //{
        //    var model = new NotificationViewModel
        //    {
        //        Message = "This notification is for testing purposes",
        //        Status = false,
        //        CommunicateBy = ModesOfCommunication.Email,
        //        EmployeeId = "2c437576-44a7-4618-b36a-0e4a308419c3"
        //    };

        //    var result = new Logic().Create(model);
            
        //}

        [TestMethod]
        public void Edit()
        {
            var model = new NotificationViewModel
            {
                Id = Guid.Parse("5B22D6B1-35CC-4FBA-891E-F4CD1EF6C691"),
                Message = "This notification is for testing purposes only",
                CommunicateBy = ModesOfCommunication.Call,
                Status = true
            };

            var result = new Logic().Edit(model);
            
        }

        [TestMethod]
        public void Delete()
        {
            var model = new NotificationViewModel
            {
                Id = Guid.Parse("5B22D6B1-35CC-4FBA-891E-F4CD1EF6C691"),
                IsDeleted = true,
            };

            var result = new Logic().Edit(model);
            
        }

        [TestMethod]
        public void UnDelete()
        {
            var model = new NotificationViewModel
            {
                Id = Guid.Parse("5B22D6B1-35CC-4FBA-891E-F4CD1EF6C691"),
                IsDeleted = false,
            };

            var result = new Logic().Edit(model);
            
        }


        [TestMethod]
        public void SearchByEmployeeName()
        {
            var model = new NotificationViewModel
            {
                EmployeeName = "Thor",
                AllStatus = true
            };

            var result = new Logic().GetNotifications<NotificationViewModel>(model);
        }

        [TestMethod]
        public void SearchById()
        {
            var model = new NotificationViewModel
            {
                Id = Guid.Parse("5B22D6B1-35CC-4FBA-891E-F4CD1EF6C691"),
                AllStatus = true
            };

            var result = new Logic().GetNotifications<NotificationViewModel>(model);
        }

        [TestMethod]
        public void SearchByStatus()
        {
            var model = new NotificationViewModel
            {
                IsDeleted = true
            };

            var result = new Logic().GetNotifications<NotificationViewModel>(model);
        }

        [TestMethod]
        public void SearchAll()
        {
            var model = new NotificationViewModel
            {
                AllStatus = true
            };

            var result = new Logic().GetNotifications<NotificationViewModel>(model);
        }
    }
}
