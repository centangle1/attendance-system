﻿using EntityProvider;
using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnumLibrary;
using Microsoft.Owin;

namespace BusinessLogic
{
    public class UserLogic
    {
        public List<RegisterViewModel> GetAllUsers(UserType userType, SelectUserProperties userProperties)
        {
            UserQueries da = new UserQueries();
            var result = da.GetAllUsers(userType, userProperties);
            return result;
        }

        public List<RegisterViewModel> GetAllUsers(Guid departmentId)
        {
            return new UserQueries().GetAllUsers(departmentId);
        }

        public RegisterViewModel GetUser(string id)
        {
            var result = new UserQueries().GetUser(id);
            return result;
        }

        public ApplicationUser GetUserByEmail(string email)
        {
            return new UserQueries().GetUserByEmail(email);
        }

        public string UndoDelete(string id)
        {
            return new UserQueries().UndoDelete(id);
        }

        public Guid EditUser(RegisterViewModel model, IOwinContext context)
        {
            model.UpdatedOn = DateTime.Now;
            var result = new UserQueries(context).EditUser(model, context);
            return result;
        }

        public Guid Delete(string id)
        {
            return new UserQueries().Delete(id);
        }
    }
}
