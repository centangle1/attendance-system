﻿using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IdentityModel.Metadata;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public Guid AddSchedule(List<ScheduleDetail> model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    
                    int i = 0; var id = new Guid();
                    foreach (var scheduleDetail in model)
                    {
                        if (i == 0)
                        {
                           
                            Schedule sch = new Schedule();
                            sch.Id = Guid.NewGuid();
                            sch.Name = scheduleDetail.ScheduleName;
                            sch.Active = true;
                            db.Schedules.Add(sch);
                            if (db.SaveChanges()>0)
                            {
                                id = sch.Id;
                                scheduleDetail.ScheduleId = id;
                            }
                        }
                        if (i > 0)
                        {
                            scheduleDetail.ScheduleId= id;
                        }
                        scheduleDetail.Day += 1;
                        scheduleDetail.Active = true;
                        var result = db.ScheduleDetails.Add(scheduleDetail);
                        if (db.SaveChanges() > 0)
                        {
                            i++;
                        }
                    }
                    return id;
                }
            }
            catch(Exception ex)
            {
                return new Guid();
            }
        }

        public Guid EditSchedule(List<ScheduleDetail> model)
        {
            try
            {
                using(ApplicationDbContext db= new ApplicationDbContext())
                {
                    var i = 0; Guid id = new Guid();
                    foreach(var schedule in model)
                    {
                        if (i == 0)
                        {
                            var previousSchedule = db.ScheduleDetails.Where(m => m.ScheduleId == schedule.ScheduleId).ToList();
                            foreach (var record in previousSchedule)
                            {
                                db.ScheduleDetails.Remove(record);
                                db.SaveChanges();
                            }
                            id = schedule.ScheduleId;
                            var scheduleRecord = db.Schedules.Find(id);
                            scheduleRecord.Name = schedule.ScheduleName;
                            db.Entry(scheduleRecord).State = EntityState.Modified;
                            if (db.SaveChanges() > 0)
                            {
                                i++;
                            }
                        }
                        schedule.Active = true;
                        schedule.Day += 1;
                        var result = db.ScheduleDetails.Add(schedule);
                        db.SaveChanges();
                    }
                     return id;
                    
                }
            }
            catch(Exception ex)
            {
                return new Guid();
            }
        }

        public List<Schedule> GetAllSchedule()
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Schedules.Where(m => m.Active).ToList();
                    return result;
                }
            }
            catch(Exception ex)
            {
                return new List<Schedule>();
            }
        }
        
        public List<ScheduleDetail> GetScheduleDetail(Guid id)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.ScheduleDetails.Where(m => m.ScheduleId == id && m.Active)
                        .Select(m => new
                        {
                            Id = m.Id,
                            Active = m.Active,
                            Day = m.Day,
                            EndingTime = m.EndingTime,
                            Schedule = m.Schedule,
                            ScheduleId = m.ScheduleId,
                            StartingTime = m.StartingTime,
                            Title = m.Title
                        }).ToList().Select(m => new ScheduleDetail
                        {
                            Id = m.Id,
                            Active = m.Active,
                            Day = m.Day-1,
                            EndingTime = m.EndingTime,
                            Schedule = m.Schedule,
                            ScheduleId = m.ScheduleId,
                            StartingTime = m.StartingTime,
                            Title = m.Title
                        }).ToList();
                    return result;
                }
            }
            catch(Exception ex)
            {
                return new List<ScheduleDetail>();
            }
        }

        public Guid DeleteSchedule(Guid id)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Schedules.Find(id);
                    db.Schedules.Remove(record);
                    db.SaveChanges();
                    return Guid.NewGuid();
                }
            }
            catch(Exception ex)
            {
                return new Guid();
            }
        }

        public Guid AddSchedulePolicy(AttendancePolicy model, Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var schedulePolicy = db.SchedulePolicies.Join(db.AttendancePolicies, sp => sp.PolicyId, ap => ap.Id, (sp, ap) => new { sp, ap })
                        .Where(m => m.sp.ScheduleId == id).FirstOrDefault();
                    if (schedulePolicy != null)
                    {
                        var policy = db.AttendancePolicies.Find(schedulePolicy.ap.Id);
                        var deletePolicy = db.AttendancePolicies.Remove(policy);
                        db.SaveChanges();
                    }
                    model.Id = Guid.NewGuid();
                    model.Active = true;
                    var attendancePolicy = db.AttendancePolicies.Add(model);
                    db.SaveChanges();
                    var newSchedulePolicy = new SchedulePolicy {                        
                        Active=true,
                        PolicyId=attendancePolicy.Id,
                        ScheduleId=id
                    };
                    var result = db.SchedulePolicies.Add(newSchedulePolicy);
                    db.SaveChanges();
                    return result.PolicyId;
                }
            }
            catch(Exception ex)
            {
                return new Guid();
            }
        }

        public AttendancePolicy GetAttendancePolicy(Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.SchedulePolicies.Join(db.AttendancePolicies, s => s.PolicyId, a => a.Id, (s, a) => new { s, a })
                        .Where(m => m.s.Active && m.s.ScheduleId == id && m.s.Active)
                        .Select(m => new AttendancePolicy
                        {
                            Id = m.a.Id,
                            Early = m.a.Early,
                            Late = m.a.Late
                        }).FirstOrDefault();

                    return result;
                }
            }
            catch(Exception ex)
            {
                return new AttendancePolicy();
            }
        }

        public Guid EditSchedulePolicy(AttendancePolicy model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var schedule = db.AttendancePolicies.Find(model.Id);
                    schedule.Early = model.Early;
                    schedule.Late = model.Late;
                    db.Entry(schedule).State = EntityState.Modified;
                    db.SaveChanges();
                    return schedule.Id;
                }
            }
            catch(Exception ex)
            {
                return new Guid();
            }
        }

        public Guid DeleteSchedulePolicy(AttendancePolicy model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    model.Active = false;
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    return model.Id;
                }
            }
            catch(Exception ex)
            {
                return new Guid();
            }
        }

        public UserScheduleViewModel GetSchedule(string id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Users.Join(db.UserSchedules, u => u.Id, us => us.UserId, (u, us) => new { u, us })
                        .Join(db.Schedules, us => us.us.Id, s => s.Id, (us, s) => new { us, s })
                        .Where(m => m.us.us.UserId == id)
                        .Select(m => new UserScheduleViewModel
                        {
                            ScheduleId = m.s.Id,
                            ScheduleName = m.s.Name,
                            UserId = m.us.u.Id,
                            UserName = m.us.u.UserName,
                            DepartmentId = m.us.u.DepartmentId
                        })
                        .FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new UserScheduleViewModel();
            }
        }

        public Guid AddUserSchedule(Guid scheduleId, string userId)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.UserSchedules.Add(new UserSchedule { Id = scheduleId, UserId = userId });
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch(Exception ex)
            {
                return new Guid();
            }
        }

        public string EditUserSchedule(UserScheduleViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.UserSchedules.Where(m => m.UserId == model.UserId).FirstOrDefault();
                    record.Id = model.ScheduleId;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.UserId;
                }
            }
            catch(Exception ex)
            {
                return "";
            }
        }
    }
}
