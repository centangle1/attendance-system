﻿using CustomModels.Collection;
using CustomModels.Identity;
using EntityProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {

        public Company CheckCompany(string name)
        {
            return new DataAccess().CheckCompany(name);
        }

        public Branch CheckBranch(string name, Guid id)
        {
            return new DataAccess().CheckBranch(name, id);
        }

        public Department CheckDepartment(string name, Guid id)
        {
            return new DataAccess().CheckDepartment(name, id);
        }

        public ApplicationUser CheckUser(string email, Guid id)
        {
            return new DataAccess().CheckUser(email, id);
        }
    }
}
