﻿using CustomModels.Collection;
using EnumLibrary;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using EntityProvider;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<EarnedLeaveViewModel> GetEarnedLeavesForUpdation(EarnedLeaveViewModel search)
        {
            return new DataAccess().GetEarnedLeavesForUpdation(search);
        }
        public bool CreateEarnedLeaveDetail(EarnedLeaveAutomaticUpdationViewModel pendingEarnedLeaves)
        {
            foreach (var item in pendingEarnedLeaves.EarnedLeavesList.Where(m => m.Checked))
            {
                DateTime LastStartingDate = item.StartingDate;
                DateTime LastEndingDate = item.StartingDate;
                float NoOfLeavesToAdd = 0;
                float.TryParse(item.Rule.Value, out NoOfLeavesToAdd);
                if (NoOfLeavesToAdd > 0)
                {
                    var addInToLeaveDetails = new Logic().Update(new LeaveDetailViewModel { AutomaticUpdate = true, EmployeeId = item.Employee.Id, IsCarriedOn = item.Rule.IsCarriedOn, Type = item.Rule.LeaveType, Count = item.EarnedLeaveCount });
                    for (int i = 0; i < item.MissedCycles; i++)
                    {
                        LastStartingDate = LastEndingDate;
                        LastEndingDate = this.GetNextEarnedLeaveDate(LastStartingDate, item.Rule.Duration);
                        var addInToEarnedLeaveDetails = new Logic().Create(new EarnedLeaveDetail { EmployeeId = item.Employee.Id, StartingDate = LastStartingDate, EndingDate = LastEndingDate, LeaveType = item.Rule.LeaveType, NumberOfLeaves = NoOfLeavesToAdd, Source = EnumLibrary.EarnedLeaveDetailSource.Automatic, RuleId = item.Rule.Id });
                    }
                }
            }
            return true;

        }
        private DateTime GetNextEarnedLeaveDate(DateTime currentDate, LeaveDurationType duration)
        {
            switch (duration)
            {
                case LeaveDurationType.Weekly:
                    return currentDate.AddDays(7);

                case LeaveDurationType.BiWeekly:
                    return currentDate.AddDays(14);

                case LeaveDurationType.Monthly:
                    return currentDate.AddMonths(1);

                case LeaveDurationType.BiMonthly:
                    return currentDate.AddMonths(2);

                case LeaveDurationType.Quarterly:
                    return currentDate.AddMonths(3);

                case LeaveDurationType.SemiAnnually:
                    return currentDate.AddMonths(6);

                case LeaveDurationType.Annually:
                    return currentDate.AddYears(1);

            }
            return currentDate;
        }
        public Guid Create(EarnedLeaveDetail model)
        {
            model.Id = Guid.NewGuid();
            model.AddedBy = HttpContext.Current.User.Identity.GetUserId();
            model.AddedOn = DateTime.Now;
            model.UpdatedOn = DateTime.Now;
            return new DataAccess().Create(model);
        }

        public Guid Edit(EarnedLeaveDetail model)
        {
            model.UpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }

    }
}
