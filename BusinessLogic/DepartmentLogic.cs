﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityProvider;
using CustomModels.Collection;
using System.Web;
using Microsoft.AspNet.Identity;
using EnumLibrary;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<Department> ViewDepartments(Guid? branchId, UserType userType)
        {

            var userId = HttpContext.Current.User.Identity.GetUserId();
            var details = new Helpers.Helpers().GetUserProperties(userId);
            return new DataAccess().GetDepartments(branchId, null, userType, details);
        }

        public Guid CreateDepartment(Department model)
        {
            model.Branches = new List<Branch>();
            model.Active = true;
            return new DataAccess().AddDepartment(model);
        }

        public List<Branch> GetBranches()
        {
            return new DataAccess().GetBranches(new BranchSearchViewModel { IncludeRoleCondition = true });
        }

        public Department GetDepartment(Guid id)
        {
            return new DataAccess().GetDepartments(null, id, 0, new SelectUserProperties()).FirstOrDefault();
        }

        public Guid DepartmentEdit(Department model)
        {
            return new DataAccess().EditDepartment(model);
        }

        public Guid DepartmentDelete(Department model)
        {
            return new DataAccess().DeleteDepartment(model);
        }

        public List<Department> GetDepartmentList(Guid id)
        {
            return new DataAccess().GetDepartments(id, null, 0, new SelectUserProperties());
        }
    }
}
