﻿function generateDonutChart(present, absent, onLeave, divId) {
    var div =  divId;
    var chart = c3.generate({
        bindto: document.getElementById(div), // id of chart wrapper
                                        data: {
                                            
                                            columns: [
                                                // each columns data
                                                ['data1', present],
                                                ['data2', absent],
                                                ['data3', onLeave]
                                            ],
                                            type: 'donut', // default type of chart
                                            colors: {
                                                'data1': '#00ee09',
                                                'data2': '#a10600',
                                                'data3': '#e4ee00'
                                            },
                                            names: {
                                                // name of each serie
                                                'data1': 'Present',
                                                'data2': 'Absent',
                                                'data3': 'On Leave'
                                            }
                                        },
                                        axis: {
                                        },
                                        legend: {
                                            show: false, //hide legend
                                        },
                                        padding: {
                                            bottom: 0,
                                            top: 0
                                        },
                                    });
    }