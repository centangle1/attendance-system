﻿using CustomModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityProvider;
using EnumLibrary;
using System.Web;
using Microsoft.AspNet.Identity;
using CustomModels.Identity;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<T> GetLeaves<T>(LeaveViewModel search)
        {
            var result = new DataAccess().GetLeaves<T>(search);
            return result;
        }

        public Guid CreateLeave(LeaveViewModel model)
        {
            model.EndingDate = model.EndingDate.AddMilliseconds(-1);
            model.AddedBy = new ApplicationUser { Id = HttpContext.Current.User.Identity.GetUserId() };
            model.AppliedOn = DateTime.Now;
            model.UpdatedOn = DateTime.Now;
            var check = new DataAccess().CreateLeave(model);
            if (check != new Guid())
            {
                if (Update(new LeaveDetailViewModel { Type = model.Type, EmployeeId = model.UserId, Count = model.AvailableLeaves - model.DaysCount }, false) != new Guid())
                {
                    return check;
                }
            }
            return new Guid();
        }

        public Guid EditLeave(LeaveViewModel model)
        {
            model.DaysCount = Rules.SortLeaves(model, new LeaveRulesDetailViewModel());
            model.UpdatedOn = DateTime.Now;
            model.UpdatedBy = HttpContext.Current.User.Identity.GetUserId();
            var result = new DataAccess().EditLeave(model);


            if (result.Id != new Guid())
            {
                model.AvailableLeaves = new DataAccess().GetLeaveDetail(new LeaveDetailSearchViewModel { Type = model.Type, EmployeeId = model.UserId }).FirstOrDefault().Count;
                if (Update(new LeaveDetailViewModel { Type = model.Type, EmployeeId = model.UserId, Count = model.AvailableLeaves + (model.EditedLeaveDayCount - model.DaysCount) }, false) != new Guid())
                    return result.Id;
            }
            return new Guid();
        }

        public Guid DeleteLeave(LeaveViewModel model)
        {
            var result = new DataAccess().DeleteLeave(model);
            model.DaysCount = Rules.SortLeaves(model, new LeaveRulesDetailViewModel());
            model.AvailableLeaves = Rules.GetAvailableLeaves(model.UserId, model.Type).Count;
            if (result != new Guid())
            {
                if (Update(new LeaveDetailViewModel { Type = model.Type, EmployeeId = model.UserId, Count = model.AvailableLeaves + model.DaysCount, Description = "Leaves added after leave delete" }, false) != new Guid())
                    return result;
            }
            return new Guid();
        }
    }
}
