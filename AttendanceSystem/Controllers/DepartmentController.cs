﻿using BusinessLogic;
using CustomModels;
using CustomModels.Collection;
using EnumLibrary;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttendanceSystem.Controllers
{
    public class DepartmentController : Controller
    {
        // GET: Department
        [Authorize]
        public ActionResult Index(Guid? id, bool individual=false)
        {
            var result = new List<Department>();

            if (individual)
            {
                result = new Logic().ViewDepartments(id, 0);
            }
            else if (User.IsInRole("Administrator"))
            {
                result = new Logic().ViewDepartments(id, UserType.Administrator);
            }
            else if (User.IsInRole("Company Manager"))
            {
                result = new Logic().ViewDepartments(id, UserType.CompanyManager);
            }
            else if (User.IsInRole("Branch Manager"))
            {
                result = new Logic().ViewDepartments(id, UserType.BranchManager);
            }
            else if (User.IsInRole("Member")|| User.IsInRole("Department Manager"))
            {
                result = new Logic().ViewDepartments(id, UserType.Member);
            }
            
         
            return View(result);
        }

        public ActionResult Create()
        {
            var branchList = new Logic().GetBranches();
            TempData["branchList"] = branchList;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Department model)
        {
            if (ModelState.IsValid)
            {
                if(new Logic().CreateDepartment(model) != new Guid())
                {
                    return RedirectToAction("Index", new {});
                }
            }
            
            return View();
        }

        public ActionResult Edit(Guid id)
        {
            var branchList = new Logic().GetBranches();
            TempData["branchList"] = branchList;
            var data = new Logic().GetDepartment(id);
            if (data != null)
            {
                return View(data);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Edit(Department model)
        {
            Guid result = new Logic().DepartmentEdit(model);
            if (result != new Guid())
                return RedirectToAction("Index");
            return View();
        }

        public ActionResult Delete(Guid id)
        {
            var result = new Logic().GetDepartment(id);
            if (result != null)
            {
                return View(result);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Delete(Department model)
        {
            Logic lg = new Logic();
            var result = lg.DepartmentDelete(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            else
                return View();
        }

        public ActionResult Details(Guid id)
        {
            Logic lg = new Logic();
            var result = lg.GetDepartment(id);
            if (result != null && result.Active == true)
            {
                return View(result);
            }
            else
                return View();
        }
        /// <summary>
        /// Gets list of Department on the base of branch Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of Departments as a JSON</returns>
       public JsonResult DepartmentList(string id)
        {
            var branchId = Guid.Parse(id);
            var result = new Logic().GetDepartmentList(branchId);
            return Json(result, JsonRequestBehavior.AllowGet);

        }
    }
}
