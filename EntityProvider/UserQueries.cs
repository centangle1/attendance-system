﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using CustomModels.Collection;
using CustomModels.Identity;
using System.Data.Entity;
using EnumLibrary;
using EntityProvider;
using AutoMapper;
using CustomModels.ReportModels;

namespace EntityProvider
{
    public class UserQueries
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public UserQueries()
        {
        }

        public UserQueries(IOwinContext context)
        {
            UserManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager; //?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager;// ?? Controller.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
        public class ApplicationUserManager : UserManager<ApplicationUser>
        {
            public ApplicationUserManager(IUserStore<ApplicationUser> store)
                : base(store)
            {
            }

            public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
            {
                var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
                // Configure validation logic for usernames
                manager.UserValidator = new UserValidator<ApplicationUser>(manager)
                {
                    AllowOnlyAlphanumericUserNames = false,
                    RequireUniqueEmail = true
                };

                // Configure validation logic for passwords
                manager.PasswordValidator = new PasswordValidator
                {
                    RequiredLength = 6,
                    RequireNonLetterOrDigit = true,
                    RequireDigit = true,
                    RequireLowercase = true,
                    RequireUppercase = true,
                };

                // Configure user lockout defaults
                manager.UserLockoutEnabledByDefault = true;
                manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
                manager.MaxFailedAccessAttemptsBeforeLockout = 5;

                // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
                // You can write your own provider and plug it in here.
                manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
                {
                    MessageFormat = "Your security code is {0}"
                });
                manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
                {
                    Subject = "Security Code",
                    BodyFormat = "Your security code is {0}"
                });
                //manager.EmailService = new EmailService();
                //manager.SmsService = new SmsService();
                var dataProtectionProvider = options.DataProtectionProvider;
                if (dataProtectionProvider != null)
                {
                    manager.UserTokenProvider =
                        new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
                }
                return manager;
            }
        }

        // Configure the application sign-in manager which is used in this application.
        public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
        {
            public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
                : base(userManager, authenticationManager)
            {
            }

            public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
            {
                return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
            }

            public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
            {
                return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
            }
        }

        //
        //Delete User
        public Guid Delete(string id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Users.Find(id);
                    if (record != null)
                    {
                        record.Active = false;
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();
                        return Guid.NewGuid();
                    }
                    return new Guid();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<Guid> Register(RegisterViewModel model)
        {

            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return Guid.NewGuid();
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return new Guid();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                //ModelState.AddModelError("", error);
            }
        }


        public List<RegisterViewModel> GetAllUsers(Guid departmentId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var userList = FilterUsers<RegisterViewModel>(null, null, departmentId, "", false);
                return userList;
            }
        }

        public List<RegisterViewModel> GetAllUsers(UserType userType, SelectUserProperties userProperties)
        {
            var result = new List<RegisterViewModel>();
            if (userType == UserType.Administrator || userType == UserType.CompanyManager)
            {
                result = FilterUsers<RegisterViewModel>(userProperties.CompanyId, null, null, "", false);
            }
            if (userType == UserType.BranchManager)
            {
                result = FilterUsers<RegisterViewModel>(null, userProperties.BranchId, null, "", false);
            }
            if (userType == UserType.DepartmentManager)
            {
                result = FilterUsers<RegisterViewModel>(null, null, userProperties.DepartmentId, "", false);
            }
            if (userType == UserType.Member)
            {
                result = FilterUsers<RegisterViewModel>(null, null, null, userProperties.UserId, false);
            }
            return result;
        }

        public ApplicationUser GetUserByEmail(string email)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.Users.Where(m => m.Email == email).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public RegisterViewModel GetUser(string id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var scheduleId = db.UserSchedules.Where(m => m.UserId == id).Select(m => m.Id).FirstOrDefault();

                    var result = db.Users.Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { user = u, department = d })
                        .GroupJoin(db.Branches, d => d.department.BranchCode, b => b.Id, (d, b) => new { department = d, branch = b.FirstOrDefault() })
                        .GroupJoin(db.UserSchedules, u => u.department.user.Id, us => us.UserId, (u, us) => new { user = u, uSchedule = us.FirstOrDefault() })
                        .GroupJoin(db.PartTimeUserDetails, u => u.user.department.user.Id, pt => pt.UserId, (u, pt) => new { user = u, partTime = pt.FirstOrDefault() })
                        .Join(db.Addresses, u => u.user.user.department.user.AddressId, a => a.Id, (u, a) => new { u, a })
                        .Where(m => m.u.user.user.department.user.Id == id)
                        .Select(m => new
                        {
                            Id = m.u.user.user.department.user.Id,
                            Name = m.u.user.user.department.user.UserName,
                            Permanant = m.u.user.user.department.user.Permanant,
                            Address = m.a,
                            BranchCode = m.u.user.user.branch.Id,
                            BranchName = m.u.user.user.branch.Name,
                            Department = m.u.user.user.department.department,
                            Email = m.u.user.user.department.user.Email,
                            ScheduleId = m.u.user.uSchedule.ScheduleId.Id == null ? new Guid() : m.u.user.uSchedule.ScheduleId.Id,
                            TimeString = string.IsNullOrEmpty(m.u.partTime.WorkingHours) ? "00:00" : m.u.partTime.WorkingHours,
                            MachineId = m.u.user.user.department.user.MachineId,
                            PhoneNumber = m.u.user.user.department.user.PhoneNumber,
                            EmployeeType = m.u.user.user.department.user.Type
                        })
                        .Select(m => new RegisterViewModel
                        {
                            Id = m.Id,
                            Address = m.Address,
                            Name = m.Name,
                            Permanant = m.Permanant,
                            Department = m.Department,
                            BranchCode = m.BranchCode,
                            BranchName = m.BranchName,
                            Email = m.Email,
                            ScheduleId = m.ScheduleId,
                            MachineId = m.MachineId,
                            PhoneNumber = m.PhoneNumber,
                            TimeString = m.TimeString,
                            Type = m.EmployeeType
                        }).DefaultIfEmpty().FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {
                return new RegisterViewModel();
            }
        }

        public Guid EditUser(RegisterViewModel model, IOwinContext context)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
                    // Configure validation logic for usernames
                    userManager.UserValidator = new UserValidator<ApplicationUser>(userManager)
                    {
                        AllowOnlyAlphanumericUserNames = false,
                        RequireUniqueEmail = true
                    };

                    // Configure validation logic for passwords
                    userManager.PasswordValidator = new PasswordValidator
                    {
                        RequiredLength = 6,
                        RequireNonLetterOrDigit = true,
                        RequireDigit = true,
                        RequireLowercase = true,
                        RequireUppercase = true,
                    };

                    var user = db.Users.Find(model.Id);
                    var address = db.Addresses.Find(user.AddressId);
                    address.Active = true;
                    address.Area = string.IsNullOrEmpty(model.Address.Area) ? address.Area : model.Address.Area;
                    address.City = string.IsNullOrEmpty(model.Address.City) ? address.City : model.Address.City;
                    address.House = model.Address.House;
                    address.Street = model.Address.Street;
                    db.Entry(address).State = EntityState.Modified;
                    db.SaveChanges();

                    if (model.Permanant)
                    {
                        var scheduleEdit = db.UserSchedules.Where(m => m.UserId == model.Id).FirstOrDefault();
                        if (scheduleEdit != null)
                        {
                            scheduleEdit.Id = model.ScheduleId;
                            db.Entry(scheduleEdit).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            var check = new DataAccess().AddUserSchedule(model.ScheduleId, model.Id);
                            if (check == new Guid())
                            {
                                throw new Exception();
                            }
                        }
                    }
                    else
                    {
                        var userPartTimeDetails = db.PartTimeUserDetails.Where(m => m.UserId == model.Id).FirstOrDefault();
                        if (userPartTimeDetails != null)
                        {
                            userPartTimeDetails.WorkingHours = model.Hours + ":" + model.Minutes;
                            db.Entry(userPartTimeDetails).State = EntityState.Modified;
                        }
                        else
                        {
                            var data = new PartTimeUserDetail { Active = true, Id = Guid.NewGuid(), UserId = model.Id, WorkingHours = model.Hours + ":" + model.Minutes };
                            var check = new DataAccess().AddPartTimeDetails(data);
                            if (check == new Guid())
                            {
                                throw new Exception();
                            }
                        }
                    }

                    model.Address.Id = user.AddressId;
                    user.Permanant = model.Permanant;
                    user.DepartmentId = model.DepartmentId != new Guid() ? model.DepartmentId : user.DepartmentId;
                    user.UserName = string.IsNullOrEmpty(model.Name) ? user.UserName : model.Name;
                    user.Email = string.IsNullOrEmpty(model.Email) ? user.Email : model.Email;
                    user.Type = model.Type == 0 ? user.Type : model.Type;
                    user.Active = true;
                    var previousRoles = userManager.GetRoles(user.Id);
                    if (previousRoles.Count() > 0)
                    {
                        var result2 = userManager.RemoveFromRole(user.Id, previousRoles.First());
                    }
                    var result1 = userManager.AddToRole(user.Id, model.Roles);
                    db.Entry(user).State = EntityState.Modified;
                    return db.SaveChanges() > 0 ? Guid.Parse(user.Id) : new Guid();
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public string UndoDelete(string id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Users.Find(id);
                    record.Active = true;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public List<T> FilterUsers<T>(Guid? companyId, Guid? branchId, Guid? departmentId, string userId, bool isSelect2)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (isSelect2)
                    {
                        return db.Users.Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
                        .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
                        .Join(db.Companies, b => b.b.CompanyId, c => c.Id, (b, c) => new { b, c })
                        .Where(m => m.b.d.u.Active &&
                        (m.c.Id == companyId || !companyId.HasValue) &&
                        (m.b.b.Id == branchId || !branchId.HasValue) &&
                        (m.b.d.d.Id == departmentId || !departmentId.HasValue) &&
                        (m.b.d.u.Id == userId || userId == "")
                        && m.c.Active && m.b.b.Active && m.b.d.d.Active)
                    .Select(m => new
                    {
                        Id = m.b.d.u.Id,
                        Name = m.b.d.u.UserName

                    }).ToList() 
                    .Select(m => new Select2OptionModel
                    {
                        id = m.Id,
                        text = m.Name
                    }).ToList() as List<T>;
                    }
                    var result = db.Users.Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
                        .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
                        .Join(db.Companies, b => b.b.CompanyId, c => c.Id, (b, c) => new { b, c })
                        .Where(m => m.b.d.u.Active &&
                        (m.c.Id == companyId || !companyId.HasValue) &&
                        (m.b.b.Id == branchId || !branchId.HasValue) &&
                        (m.b.d.d.Id == departmentId || !departmentId.HasValue) &&
                        (m.b.d.u.Id == userId || userId == "")
                        && m.c.Active && m.b.b.Active && m.b.d.d.Active)
                    .Select(m => new
                    {
                        Active = m.b.d.u.Active,
                        Address = m.b.d.u.Address,
                        Department = m.b.d.u.Department,
                        Email = m.b.d.u.Email,
                        PhoneNumber = m.b.d.u.PhoneNumber,
                        UserName = m.b.d.u.UserName,
                        AddressId = m.b.d.u.AddressId,
                        DepartmentId = m.b.d.u.DepartmentId,
                        Id = m.b.d.u.Id,
                        MachineId = m.b.d.u.MachineId,
                        CompanyName = m.c.Name,
                        DepartmentName = m.b.d.d.Name,
                        BranchName = m.b.b.Name

                    }).ToList()
                    .Select(m => new RegisterViewModel
                    {
                        Active = m.Active,
                        Address = m.Address,
                        Department = m.Department,
                        Email = m.Email,
                        PhoneNumber = m.PhoneNumber,
                        Name = m.UserName,
                        AddressId = m.AddressId,
                        DepartmentId = m.DepartmentId,
                        Id = m.Id,
                        MachineId = m.MachineId,
                        CompanyName = m.CompanyName,
                        DepartmentName = m.DepartmentName,
                        BranchName = m.BranchName
                    }).ToList();
                    return result as List<T>;

                }
            }
            catch (Exception ex)
            {
                return new List<T>();
            }

        }
    }
}

