﻿using AutoMapper;
using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public List<LeaveRuleViewModel> GetAllAssignedRules(LeaveRuleViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.Type != 0)
                    {
                        var record = db.LeaveRules.Find(search.Type, search.Rule.Id, search.EmployeeType);
                        if (record.IsDeleted == search.IsDeleted)
                            return new List<LeaveRuleViewModel> { new LeaveRuleViewModel { Rule = record.Rule, EmployeeType = record.EmployeeType, Id = record.Id } };
                        throw new Exception();
                    }
                    else
                    {
                        return db.LeaveRules
                            .Join(db.LeaveRulesDetails, r => r.RuleId, rd => rd.Id, (r, rd) => new { r, rd })
                            .Where(m => (m.r.IsDeleted == search.IsDeleted) &&
                                                 (m.rd.IsDeleted == search.IsDeleted) &&
                                                 (search.Rule.Id == new Guid() || m.r.RuleId == search.Rule.Id) &&
                                                 (search.Rule.RuleType == 0 || m.rd.RuleType == search.Rule.RuleType) &&
                                                 (search.EmployeeType == 0 || m.r.EmployeeType == search.EmployeeType))
                                                 .Select(m => new LeaveRuleViewModel
                                                 {
                                                     Id = m.r.Id,
                                                     Rule = m.rd,
                                                     Type = m.rd.LeaveType,
                                                     EmployeeType = m.r.EmployeeType
                                                 }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<LeaveRuleViewModel>();
            }
        }

        public Guid Create(LeaveRuleViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<LeaveRuleViewModel, LeaveRule>().ForMember(dest => dest.RuleId, x => x.MapFrom(src => src.Rule.Id))
                                                                                                                .ForMember(dest => dest.Rule, x => x.Ignore()); });
                    var iMapper = config.CreateMapper();
                    var leaveRule = iMapper.Map<LeaveRuleViewModel, LeaveRule>(model);
                    var result = db.LeaveRules.Add(leaveRule);
                    db.SaveChanges();
                    return result.RuleId;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid Edit(LeaveRuleViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.LeaveRules.Where(m=>m.Id == model.Id).First();
                        record.IsDeleted = model.IsDeleted;
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();
                        return record.RuleId;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }
    }
}
