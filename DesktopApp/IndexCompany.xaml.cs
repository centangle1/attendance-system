﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLogic.CompanyLogic;
using Models.Collection;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AttendanceSystemWpf
{
    /// <summary>
    /// Interaction logic for IndexCompany.xaml
    /// </summary>
    public partial class IndexCompany : Window
    {

        public IndexCompany()
        {
            
            InitializeComponent();
            
            //this.indexDataGrid.ItemsSource = result;

        }
        //private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        //{
        //    // ... Create a List of objects.
        //    CompanyLogic cl = new CompanyLogic();
        //    var result = cl.GetAllLogic();
        //    // ... Assign ItemsSource of DataGrid.
        //    var grid = sender as DataGrid;
        //    grid.ItemsSource = result;
        //}
        private void createcompany_Click(object sender, RoutedEventArgs e)
        {
           
            CreateCompany cc = new CreateCompany();
            cc.Owner = App.Current.MainWindow;
            cc.Show();
            this.Close();
            
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var code = button.Tag;
            EditCompany ec = new EditCompany();
            ec.Owner = App.Current.MainWindow;
            ec.Show();
            
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BranchIndex_Click(object sender, RoutedEventArgs e)
        {
            IndexBranch ib = new IndexBranch();
            ib.Owner = App.Current.MainWindow;
            ib.Show();
        }
    }
}
