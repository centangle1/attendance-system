﻿using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public List<NationalHoliday> GetAllHolidays()
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.NationalHolidays.Where(m => m.Active).Select(m => new 
                    {
                        StartDate = m.StartDate,
                        Description = m.Description,
                        Id = m.Id,
                        EndDate = m.EndDate,
                        Title = m.Title,
                        Active = m.Active
                    }).ToList()
                    .Select(m => new NationalHoliday
                    {
                        StartDate = m.StartDate,
                        EndDate = m.EndDate,
                        Description = m.Description,
                        Id = m.Id,
                        Title = m.Title,
                        Active = m.Active
                    }).ToList(); ;
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<NationalHoliday>();
            }
        }

        public Guid CreateNationHoliday(NationalHoliday model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    model.Active = true;
                    model.Id = Guid.NewGuid();
                    db.NationalHolidays.Add(model);
                    if (db.SaveChanges() > 0)
                        return model.Id;
                    else
                        return new Guid();
                }

            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return new Guid();
            }
        }

        public NationalHoliday GetNationalHoliday(Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.NationalHolidays.Where(m => m.Id == id && m.Active).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception)
            {
                return new NationalHoliday();
            }
        }

        public Guid EditNationalHoliday(NationalHoliday model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.NationalHolidays.Where(m => m.Id == model.Id && m.Active).FirstOrDefault();
                    result.StartDate = model.StartDate;
                    result.Description = model.Description;
                    result.Title = model.Title;
                    result.EndDate = model.EndDate;
                    db.Entry(result).State = EntityState.Modified;
                    var check = db.SaveChanges();
                    if (check > 0)
                    {
                        return model.Id;
                    }
                    else
                        return new Guid();
                }
            }
            catch (Exception)
            {
                return new Guid();
            }
        }

        public Guid DeleteNationalHoliday(NationalHoliday model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.NationalHolidays.Where(m => m.Id == model.Id && m.Active).FirstOrDefault();
                    result.Active = false;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return Guid.NewGuid();
                }
            }
            catch (Exception)
            {
                return new Guid();
            }
        }
    }
}
