﻿using CustomModels.Collection;
using EntityProvider;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
      public List<NationalHoliday> GetAllHolidays()
        {
            return new DataAccess().GetAllHolidays(); 
        }

      public Guid CreateNationalHoliday(NationalHoliday model)
        {
            model.EndDate = model.EndDate.AddDays(1);
            return new DataAccess().CreateNationHoliday(model);
        }

      public NationalHoliday GetNationalHoliday(Guid id)
        {
            return new DataAccess().GetNationalHoliday(id);
        }

      public Guid EditNationalHoliday(NationalHoliday model)    
        {
            model.EndDate = model.EndDate.AddDays(1);
            return new DataAccess().EditNationalHoliday(model);
        }

      public Guid DeleteNationalHoliday(NationalHoliday model)
        {
            return new DataAccess().DeleteNationalHoliday(model);
        }
    }

}
