﻿using BusinessLogic;
using CustomModels.Collection;
using EnumLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttendanceSystem.Controllers
{
    public class EarnedLeaveController : Controller
    {
        // GET: EarnedLeave
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexPartial()
        {
            var result = new EarnedLeaveAutomaticUpdationViewModel { EarnedLeavesList = new Logic().GetEarnedLeavesForUpdation(new EarnedLeaveViewModel()) };
            return PartialView(result);
        }

        [HttpPost]
        public ActionResult EarnedLeaveAutomaticUpdation(EarnedLeaveAutomaticUpdationViewModel pendingEarnedLeaves)
        {
            var result = new Logic().CreateEarnedLeaveDetail(pendingEarnedLeaves);
            if (result)
            {
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }
        
    }
}