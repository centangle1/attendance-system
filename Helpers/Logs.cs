﻿using AutoMapper;
using CustomModels.Collection;
using CustomModels.Identity;
using EnumLibrary;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Helpers
{
    public class Logs
    {
        public Guid Create(LogSearchViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    model.Id = Guid.NewGuid();
                    model.DateTime = DateTime.Now;
                    model.AddedBy = string.IsNullOrEmpty(model.AddedBy) ? HttpContext.Current.User.Identity.GetUserId() : model.AddedBy;
                    if (model.SearchType == LogSearchType.Exception)
                    {
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<LogSearchViewModel, ExceptionLog>(); });
                        var iMapper = config.CreateMapper();
                        var exceptionLog = iMapper.Map<LogSearchViewModel, ExceptionLog>(model);
                        var result = db.ExceptionLogs.Add(exceptionLog);
                        db.SaveChanges();
                        return result.Id;
                    }
                    else if (model.SearchType == LogSearchType.General)
                    {
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<LogSearchViewModel, GeneralLog>(); });
                        var iMapper = config.CreateMapper();
                        var generalLog = iMapper.Map<LogSearchViewModel, GeneralLog>(model);
                        var result = db.GeneralLogs.Add(generalLog);
                        db.SaveChanges();
                        return result.Id;
                    }
                    else if (model.SearchType == LogSearchType.LeaveSync)
                    {
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<LogSearchViewModel, LeaveSynchroniztionLog>(); });
                        var iMapper = config.CreateMapper();
                        var leaveSynchroniztionLog = iMapper.Map<LogSearchViewModel, LeaveSynchroniztionLog>(model);
                        var result = db.LeaveSynchroniztionLogs.Add(leaveSynchroniztionLog);
                        db.SaveChanges();
                        return result.Id;
                    }

                    return new Guid();
                }
            }
            catch (Exception ex)
            {

                return new Guid();
            }
        }
    }
}
