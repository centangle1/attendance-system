﻿using CustomModels.Collection;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttendanceSystem.Models
{
    public sealed class Singleton
    {
        private Singleton() { }

        private static readonly Lazy<Singleton> lazy = new Lazy<Singleton>(() => new Singleton());

        public static Singleton Instance { get { return lazy.Value; } }
        private static SelectUserProperties details;
        public static Singleton GetSingleton(string email)
        {
            Singleton.details = new Helpers.Helpers().GetUserProperties(email);
            return Instance;
        }
        public string UserId { get {
                return details.UserId;} }
        public Guid CompanyId { get {
                return details.CompanyId; } }
        public Guid BranchId { get {
                return details.BranchId;
            } }
        public Guid DepartmentId { get {
                return details.DepartmentId; ; } }
        public int MachineId { get {
                return details.MachineId;
            } }
        public string LogoUrl { get {
                var result = details != null ? details.LogoUrl : "/Images/Company_Logo/Centangle.png";
                return result;
            } }
        public string UserRole { get {
                var result = details != null ? new Helpers.Helpers().GetRoles(details.UserId).First() : "";
                return result;
            } }
    }
}