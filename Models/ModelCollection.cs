﻿using EnumLibrary;
using Foolproof;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CustomModels.Collection
{
    public class Company
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Enter a name")]
        [Display(Name = "Company Name")]
        public string Name { get; set; }
        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public bool Active { get; set; }

        public string LogoUrl { get; set; }
    }
    public class CompanyViewModel
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Enter a name")]
        [Display(Name = "Company Name")]
        public string Name { get; set; }
        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public bool Active { get; set; }

        public string LogoUrl { get; set; }
        public HttpPostedFileBase Logo { get; set; }
    }

    public class Department
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter a name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Select a branch")]
        [ForeignKey("Branch")]
        [Display(Name = "Branch")]
        public Guid BranchCode { get; set; }
        public Branch Branch { get; set; }

        [Display(Name = "Delete")]
        public bool Active { get; set; }

        [NotMapped]
        public List<Branch> Branches { get; set; }

        [NotMapped]
        public string BranchName { get; set; }
    }

    public class CompanySearchViewModel
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public bool GetAllCompanies { get; set; }
    }

    //public class DepartmentView
    //{
    //    public int Id { get; set; }

    //    [Required(ErrorMessage = "Enter a name")]
    //    public string Name { get; set; }

    //    [Required(ErrorMessage = "Select a branch")]
    //    [Display(Name = "Branch")]
    //    public int BranchCode { get; set; }

    //    public Branch Branch { get; set; }

    //    [Display(Name = "Delete")]
    //    public bool Active { get; set; }

    //    public List<Branch> Branches { get; set; }

    //    public string BranchName { get; set; }
    //}
    public class Address
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Enter a City")]
        public string City { get; set; }

        public string Area { get; set; }

        [Display(Name = "Street Number")]
        [Range(0, 10000, ErrorMessage = "Enter correct street number")]
        public int Street { get; set; }
        [Display(Name = "House Number")]
        [Range(0, 10000, ErrorMessage = "Enter correct house number")]

        public int House { get; set; }

        public bool Active { get; set; }
    }

    public class Branch
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Enter a name")]
        public string Name { get; set; }

        [Display(Name = "Company")]
        [ForeignKey("Company")]
        public Guid CompanyId { get; set; }
        public Company Company { get; set; }

        [Required]
        [ForeignKey("Address")]
        public Guid AddressId { get; set; }
        public Address Address { get; set; }

        public bool Active { get; set; }
    }

    public class BranchSearchViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public string UserId { get; set; }
        public bool IncludeRoleCondition { get; set; }

    }

    public class Leave
    {
        public Guid Id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required(ErrorMessage = "Select a date")]
        public DateTime StartingDate { get; set; }
        [NotMapped]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DStartingDate { get; set; }
        [DataType(DataType.Date)]
        [GreaterThanOrEqualTo("StartingDate")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required(ErrorMessage = "Select a date")]
        public DateTime EndingDate { get; set; }
        [NotMapped]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DEndingDate { get; set; }
        public bool Status { get; set; }
        [Required(ErrorMessage = "Select leave type")]
        public LeaveType Type { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        [Display(Name = "Delete")]
        public bool Active { get; set; }
        public string AddedBy { get; set; }
        public DateTime AppliedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string ImageUrl { get; set; }
        public string UpdatedBy { get; set; }
        public string Note { get; set; }
    }

    public class LeaveViewModel : GeneralSearchViewModel
    {
        public LeaveViewModel()
        {
            AddedBy = new ApplicationUser();
        }
        public HttpPostedFileBase Image { get; set; }
        public string ImageUrl { get; set; }

        public Guid Id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime StartingDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DStartingDate { get; set; }
        [DataType(DataType.Date)]
        [GreaterThanOrEqualTo("StartingDate",ErrorMessage = "Starting Date is greater than ending date.")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime EndingDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DEndingDate { get; set; }
        public bool Status { get; set; }
        public LeaveType Type { get; set; }
        public string UserId { get; set; }
        [Display(Name = "Delete")]
        public bool Active { get; set; }
        public Guid BranchId { get; set; }
        public Guid DepartmentId { get; set; }
        public ApplicationUser User { get; set; }
        [Display(Name = "Employee Type")]
        public EmployeeType EmployeeType { get; set; }
        [Display(Name = "Consecutive")]
        public bool IsConsecutive { get; set; }
        public bool GetOverLappedLeave { get; set; }
        public int ConsecutiveIndex { get; set; }
        public int DaysCount { get; set; }
        public float AvailableLeaves { get; set; }
        public ApplicationUser AddedBy { get; set; }
        public DateTime AppliedOn { get; set; }
        public string UpdatedBy { get; set; }
        public int EditedLeaveDayCount { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string Note { get; set; }
        public Guid SkipLeave { get; set; }
    }

    public class LeaveDetail
    {
        public Guid Id { get; set; }
        [Key, Column(Order = 1)]
        [ForeignKey("Employee")]
        public string EmployeeId { get; set; }
        public ApplicationUser Employee { get; set; }
        [Key, Column(Order = 2)]
        public LeaveType Type { get; set; }
        public float Count { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class LeaveDetailViewModel
    {
        public LeaveDetailViewModel()
        {
            LeaveTypeDetailList = new List<LeaveTypeDetailViewModel>();
        }
        public Guid Id { get; set; }
        public string EmployeeId { get; set; }
        public List<LeaveTypeDetailViewModel> LeaveTypeDetailList { get; set; }
        public LeaveType Type { get; set; }
        public float Count { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public Guid BranchId { get; set; }
        public Guid DepartmentId { get; set; }
        public ApplicationUser Employee { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public bool AutomaticUpdate { get; set; }
        public bool IsCarriedOn { get; set; }
    }

    public class LeaveDetailReportViewModel
    {
        public LeaveDetailReportViewModel()
        {
            LeaveTypeDetailList = new List<LeaveTypeDetailViewModel>();
        }
        public ApplicationUser Employee { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public List<LeaveTypeDetailViewModel> LeaveTypeDetailList { get; set; }
    }

    public class LeaveDetailSearchViewModel
    {
        public Guid Id { get; set; }
        public string EmployeeId { get; set; }
        public LeaveType Type { get; set; }
        public string Description { get; set; }
        public LeaveDetailSearchType SearchType { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class LeaveTypeDetailViewModel
    {
        public LeaveType Type { get; set; }
        public float Count { get; set; }
        public string Description { get; set; }
    }

    //public class LeaveAllowed
    //{
    //    public Guid Id { get; set; }
    //    public LeaveType Type { get; set; }
    //    public int Number { get; set; }
    //    public bool IsDeleted { get; set; }
    //}

    //public class LeaveAllowedViewModel
    //{
    //    public Guid Id { get; set; }
    //    public LeaveType Type { get; set; }
    //    public int Number { get; set; }
    //    public bool IsDeleted { get; set; }
    //}

    public class LeaveRule
    {
        public Guid Id { get; set; }
        [Key, Column(Order = 1)]
        [ForeignKey("Rule")]
        public Guid RuleId { get; set; }
        public LeaveRulesDetail Rule { get; set; }
        [Key, Column(Order = 2)]
        public EmployeeType EmployeeType { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class LeaveRuleViewModel
    {
        public LeaveRuleViewModel()
        {
            Rule = new LeaveRulesDetail();
        }

        public Guid Id { get; set; }
        public LeaveType Type { get; set; }
        public LeaveRulesDetail Rule { get; set; }
        public EmployeeType EmployeeType { get; set; }
        public bool IsDeleted { get; set; }
        public string Action { get; set; }
    }

    public class LeaveRulesDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public LeaveType LeaveType { get; set; }
        public RulesValueType ValueType { get; set; }
        public Operators Operators { get; set; }
        public RuleType RuleType { get; set; }
        public LeaveDurationType Duration { get; set; }
        public string ErrorMessage { get; set; }
        public bool IncludeHolidays { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsConsecutive { get; set; }
        public bool IsCarriedOn { get; set; }
    }

    public class LeaveRulesDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public LeaveType LeaveType { get; set; }
        public RulesValueType ValueType { get; set; }
        public Operators Operators { get; set; }
        public RuleType RuleType { get; set; }
        public LeaveDurationType Duration { get; set; }
        public bool IncludeHolidays { get; set; }
        public string ErrorMessage { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public string Action { get; set; }
        public bool IsSelect2 { get; set; }
        public EmployeeType EmployeeType { get; set; }
        public bool IsConsecutive { get; set; }
        public bool ExcludePreviousLeavesCount { get; set; }
        public bool IsCarriedOn { get; set; }
        // Method that perform shallow copy  
        public LeaveRulesDetailViewModel ShallowCopy()
        {
            return (LeaveRulesDetailViewModel)MemberwiseClone();
        }
    }

    public class Attendance
    {
        public Guid Id { get; set; }
        public DateTime EntryTime { get; set; }
        public string MacAddress { get; set; }
        public Guid BranchId { get; set; }
        public bool ManualEntry { get; set; }
        public string Error { get; set; }
        [Display(Name = "Delete")]
        public bool Active { get; set; }
        public int MachineId { get; set; }
        public string ImageUrl { get; set; }

    }

    public class AttendanceViewModel
    {
        public Guid Id { get; set; }
        public DateTime EntryTime { get; set; }
        public string MacAddress { get; set; }
        public Guid BranchId { get; set; }
        public bool ManualEntry { get; set; }
        public string Error { get; set; }
        public bool Active { get; set; }
        public int MachineId { get; set; }
        public string UserId { get; set; }
        public Guid DepartmentId { get; set; }      
        public string BranchName { get; set; }
        public string UserName { get; set; }
        public string DepartmentName { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImageUrl { get; set; }
    }

    public class DataTableAttendanceSearchViewModel
    {
        public DataTableParameters DataTableParameters { get; set; }
        public UserSearchViewModel UserParameters { get; set; }
    }

    public class DataTableAttendanceViewModel
    {
        public List<AttendanceViewModel> Attendances { get; set; }
        public int TotalRecordCount { get; set; }
        public int FilteredRecordCount { get; set; }
    }

    public class DataTableParameters
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public string SearchValue { get; set; }
        public string SortColumnName { get; set; }
        public string SortDirection { get; set; }
    }

    public class RegisterViewModel
    {
        public string Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Address Id")]
        public Guid AddressId { get; set; }

        [Required]
        [Display(Name = "Department Id")]
        public Guid DepartmentId { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^((?=.*[A-Z])(?=.*\d)(?=.*[a-z])|(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%&\/=?_.-])|(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%&\/=?_.-])|(?=.*\d)(?=.*[a-z])(?=.*[!@#$%&\/=?_.-])).{6,15}$", ErrorMessage = "Passwords must have at least one digit ('0'-'9'). Passwords must have at least one uppercase ('A'-'Z').")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Select a role")]
        public string Roles { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public Department Department { get; set; }

        public EmployeeType Type { get; set; }

        public Address Address { get; set; }

        public bool Active { get; set; }

        public TimeSpan WorkingHours { get; set; }
        [Range(0, 164, ErrorMessage = "Hour range 0~164")]
        public int Hours { get; set; }

        [Range(0, 60, ErrorMessage = "<inutes range 0~60")]
        public int Minutes { get; set; }

        public int MachineId { get; set; }

        public Guid ScheduleId { get; set; }

        public string Schedule { get; set; }

        public bool Permanant { get; set; }

        public Guid BranchCode { get; set; }

        [Display(Name = "Company")]
        public string CompanyName { get; set; }

        [Display(Name = "Branch")]
        public string BranchName { get; set; }

        [Display(Name = "Department")]
        public string DepartmentName { get; set; }

        public string TimeString { get; set; }

        public LeaveDetailViewModel LeaveDetail { get; set; }

        public DateTime AddedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }

    public class NationalHoliday
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Enter a title")]
        public string Title { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        //[Index(IsUnique =true)]
        [Required(ErrorMessage = "Enter a date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "Enter a date")]
        [DataType(DataType.Date)]
        [GreaterThanOrEqualTo("StartDate")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Permanent")]
        public bool Permanent { get; set; }
        [NotMapped]
        public string FormattedStartDate
        {
            get
            {
                return String.Format("{0:yyyy-MM-dd HH:mm:ss}", this.StartDate);
            }
        }
        [NotMapped]
        public string FormattedEndDate
        {
            get
            {
                return String.Format("{0:yyyy-MM-dd HH:mm:ss}", this.EndDate);
            }
        }
        public bool Active { get; set; }
    }

    public class UserSchedule
    {
        [Key, Column(Order = 0)]
        [ForeignKey("ScheduleId")]
        public Guid Id { get; set; }
        public Schedule ScheduleId { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }

    public class Schedule
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }
    }

    public class UserScheduleViewModel
    {
        public Guid ScheduleId { get; set; }
        public string ScheduleName { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public Guid DepartmentId { get; set; }
    }

    public class ScheduleDetail
    {
        public Guid Id { get; set; }

        public int Day { get; set; }

        public string Title { get; set; }

        public TimeSpan StartingTime { get; set; }

        public TimeSpan EndingTime { get; set; }

        [ForeignKey("Schedule")]
        public Guid ScheduleId { get; set; }
        public Schedule Schedule { get; set; }

        [NotMapped]
        public string ScheduleName { get; set; }

        [NotMapped]
        public WeekDays Days
        {
            get
            {
                return (WeekDays)this.Day;
            }
        }

        public bool Active { get; set; }


    }

    public class AttendancePolicy
    {
        public Guid Id { get; set; }
        public int Early { get; set; }
        public int Late { get; set; }
        public bool Active { get; set; }
    }

    public class SchedulePolicy
    {
        [Key, Column(Order = 1)]
        [ForeignKey("Schedule")]
        public Guid ScheduleId { get; set; }
        public Schedule Schedule { get; set; }
        [Key, Column(Order = 2)]
        [ForeignKey("Policy")]
        public Guid PolicyId { get; set; }
        public AttendancePolicy Policy { get; set; }
        public bool Active { get; set; }
    }

    public class SchedulePolicyDetailViewModel
    {
        public Guid Id { get; set; }

        public int Day { get; set; }

        public string Title { get; set; }

        public Guid ScheduleId { get; set; }

        public TimeSpan StartingTime { get; set; }

        public TimeSpan EndingTime { get; set; }

        public bool Active { get; set; }

        public AttendancePolicy Policy { get; set; }
    }

    public class PartTimeUserDetail
    {
        public Guid Id { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string WorkingHours { get; set; }
        public bool Active { get; set; }
    }

    public class CompanyPolicy
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        [ForeignKey("Company")]
        public Guid CompanyId { get; set; }
        public Company Company { get; set; }
        public string Value { get; set; }
        public bool Active { get; set; }
    }

    public class CompanyPolicySearchViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
    }

    public class Machine
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        [ForeignKey("Branch")]
        public Guid BranchId { get; set; }
        public Branch Branch { get; set; }
        [ForeignKey("Company")]
        public Guid CompanyId { get; set; }
        public Company Company { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        [DataType(DataType.MultilineText)]
        public string AppLink { get; set; }
        public bool Up { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime LastUpdatedOn { get; set; }
        [DataType(DataType.MultilineText)]
        public string StatusUpdateLink { get; set; }
        public bool Active { get; set; }
    }

    public class SelectUserProperties
    {
        public string UserId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid BranchId { get; set; }
        public Guid DepartmentId { get; set; }
        public int MachineId { get; set; }
        public string UserRole { get; set; }
        public string LogoUrl { get; set; }
    }

    public class UserSearchViewModel : SelectUserProperties
    {
        public string UserName { get; set; }
        public string DepartmentName { get; set; }
        public string BranchName { get; set; }
        public string Date { get; set; }
    }

    public class ExcelImportUrl
    {
        public string Url { get; set; }
    }

    public class ResetUserPassword
    {
        public string UserId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class GeneralLog
    {
        public Guid Id { get; set; }
        public string Model { get; set; }
        public DateTime DateTime { get; set; }
        public string Action { get; set; }
        public string AddedBy { get; set; }
        public string AssemblyName { get; set; }
        public string Namespace { get; set; }
        public string DataType { get; set; }
        public string Table { get; set; }
    }

    public class ExceptionLog
    {
        public Guid Id { get; set; }
        public string Model { get; set; }
        public DateTime DateTime { get; set; }
        public string Action { get; set; }
        public string AddedBy { get; set; }
        public string AssemblyName { get; set; }
        public string Namespace { get; set; }
        public string DataType { get; set; }
    }

    public class LeaveSynchroniztionLog
    {
        public Guid Id { get; set; }
        public string Model { get; set; }
        public DateTime DateTime { get; set; }
        public string Action { get; set; }
        public string AddedBy { get; set; }
        public string Table { get; set; }
        public Guid RuleId { get; set; }
    }



    public class LogSearchViewModel
    {
        public Guid Id { get; set; }
        public string Model { get; set; }
        public DateTime DateTime { get; set; }
        public string Action { get; set; }
        public string AddedBy { get; set; }
        public LogSearchType SearchType { get; set; }
        public string AssemblyName { get; set; }
        public string Namespace { get; set; }
        public string DataType { get; set; }
        public Guid RuleId { get; set; }
        public string Table { get; set; }
    }

    public class Notification
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public bool Status { get; set; }
        public DateTime DateTime { get; set; }
        [ForeignKey("Employee")]
        public string UserId { get; set; }
        public ApplicationUser Employee { get; set; }
        public ModesOfCommunication CommunicateBy { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class NotificationViewModel : GeneralSearchViewModel
    {
        public NotificationViewModel() {
            Employee = new ApplicationUser();
        }

        public Guid Id { get; set; }
        public string Message { get; set; }
        public bool Status { get; set; }
        public DateTime DateTime { get; set; }
        public ApplicationUser Employee { get; set; }
        public ModesOfCommunication CommunicateBy { get; set; }
        public bool AllStatus { get; set; }
        public bool IsDeleted { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
    }
    
    public class EarnedLeaveDetail
    {
        public Guid Id { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndingDate { get; set; }
        public float NumberOfLeaves { get; set; }
        public string EmployeeId { get; set; }
        public LeaveType LeaveType { get; set; }
        public DateTime AddedOn { get; set; }
        public string AddedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public Guid RuleId { get; set; }
        public EarnedLeaveDetailSource Source { get; set; }

    }

    public class EarnedLeaveDetailViewModel
    {
        public Guid Id { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndingDate { get; set; }
        public float NumberOfLeaves { get; set; }
        public string EmployeeId { get; set; }
        public LeaveType LeaveType { get; set; }
        public DateTime AddedOn { get; set; }
        public string AddedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public Guid RuleId { get; set; }
        public EarnedLeaveDetailSource Source { get; set; }

    }
    public class EarnedLeaveDetailSearchViewModel
    {
        public EarnedLeaveDetailSearchViewModel()
        {
            EmployeeList = new List<string>();
        }
        public Guid Id { get; set; }
        public DateTime DateTime { get; set; }
        public string EmployeeId { get; set; }
        public LeaveType LeaveType { get; set; }
        public DateTime AddedOn { get; set; }
        public string AddedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public Guid RuleId { get; set; }
        public LeaveDetailSearchType SearchType { get; set; }
        public EarnedLeaveDetailSource Source { get; set; }
        public List<string> EmployeeList { get; set; }

    }

    public class EarnedLeaveAutomaticUpdationViewModel
    {
        public List<EarnedLeaveViewModel> EarnedLeavesList { get; set; }
    }

    public class EarnedLeaveViewModel : GeneralSearchViewModel
    {
        public EarnedLeaveViewModel()
        {
            Employee = new ApplicationUser
            {
                Id = null
            };
            Rule = new LeaveRulesDetailViewModel();
        }

        public bool Checked { get; set; }
        public ApplicationUser Employee { get; set; }
        public LeaveRulesDetailViewModel Rule { get; set; }
        public float ExistingLeaves { get; set; }
        public float EarnedLeaveCount { get; set; }
        public int MissedCycles { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndingDate { get; set; }
    }

    public class LeaveTypeList
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }

    public class ScheduleViewModel
    {
        public string Name { get; set; }
        public List<ScheduleDetailViewModel> schedules { get; set; }
    }

    public class ScheduleDetailViewModel
    {
        public int day { get; set; }
        public List<PeriodsViewModel> periods { get; set; }
    }

    public class PeriodsViewModel
    {
        public TimeSpan start { get; set; }
        public TimeSpan end { get; set; }
    }

    public class GeneralSearchViewModel
    {
        public bool Pagination { get; set; }
        public bool IsSelect2 { get; set; }
        public bool CalculateTotal { get; set; }
        public bool ReturnEmpty { get; set; }
        public int CurrentPage { get; set; }
        public int RecordsPerPage { get; set; }
    }

    public class SearchResultViewModel<T>
    {
        public List<T> ResultList { get; set; }
        public int TotalCount { get; set; }
    } 

}
