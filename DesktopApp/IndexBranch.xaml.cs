﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AttendanceSystemWpf
{
    /// <summary>
    /// Interaction logic for IndexBranch.xaml
    /// </summary>
    public partial class IndexBranch : Window
    {
        public IndexBranch()
        {
            InitializeComponent();
        }

        private void createBranch_Click(object sender, RoutedEventArgs e)
        {
            CreateBranch cb = new CreateBranch();
            cb.Owner = App.Current.MainWindow;
            cb.Show();
        }
    }
}
