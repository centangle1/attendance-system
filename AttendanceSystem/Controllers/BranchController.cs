﻿using CustomModels;
using CustomModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic;
using Microsoft.AspNet.Identity;
using AttendanceSystem.Models;

namespace AttendanceSystem.Controllers
{
    public class BranchController : Controller
    {
        // GET: Branch
        [Authorize]
        public ActionResult Index()
        {
            Logic lg = new Logic();
            var result = lg.GetBranches(new BranchSearchViewModel ());
            if (result != null)
            {
                return View(result);
            }
            return View();
        }

        public ActionResult GetBranches(Guid id)
        {
            if(User.IsInRole("Administrator"))
            {
                var result = new Logic().GetBranches();
                if (result != null)
                {
                    return View(result);
                }
            }
            else
            {
                var userId = User.Identity.GetUserId();
                var details = new Helpers.Helpers().GetUserProperties(userId);
                var result = new Logic().GetBranches(new BranchSearchViewModel{CompanyId = details.CompanyId});
                if (result != null)
                {
                    return View(result);
                }
            }
            return View();
        }

        public ActionResult Create()
        {
            var companyList = new Logic().GetCompanies(new CompanySearchViewModel { GetAllCompanies = true});
            return View();
        }
        [HttpPost]
        public ActionResult Create(Branch model)
        {
            if (ModelState.IsValid)
            {
                model.CompanyId = Singleton.Instance.CompanyId;
                var result = new Logic().AddBranch(model);
                if (result != new Guid())
                    return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Edit(Guid id)
        {
            var result = new Logic().GetBranches(new BranchSearchViewModel { Id = id}).FirstOrDefault();
            if (result != null)
            { 
                TempData["addressId"] = result.AddressId;
            return View(result);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Edit(Branch model)
        {
            Logic lg = new Logic();
            model.AddressId = Guid.Parse(TempData["addressId"].ToString());
            var result = lg.EditBranch(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Delete(Guid id)
        {
            return View(new Logic().GetBranches(new BranchSearchViewModel { Id = id }).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Delete(Branch model)
        {
            var result = new Logic().DeleteBranch(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Details(Guid id)
        {
            return View(new Logic().GetBranches(new BranchSearchViewModel { Id = id }).FirstOrDefault());
        }
    }
}