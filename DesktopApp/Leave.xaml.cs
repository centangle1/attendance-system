﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AttendanceSystemWpf
{
    /// <summary>
    /// Interaction logic for Leave.xaml
    /// </summary>
    public partial class Leave : Window
    {
        public Leave()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string cs = @"Data Source=.\SQLEXPRESS;Initial Catalog=AttendanceDb;Integrated Security=True";
            SqlConnection conn = null;


            try
            {

                int LeaveId = Convert.ToInt16(tb1.Text);
                DateTime LeaveDate = Convert.ToDateTime(tb2.Text);
                string LeaveType = tb3.Text;
                string LeaveStatus = tb4.Text;
                int LeaveDuration = Convert.ToInt32(tb5.Text);
                int userid = Convert.ToInt32(tb6.Text);



                conn = new SqlConnection(cs);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;

                cmd.CommandText = "INSERT INTO Leave(leave_id,leave_date,leave_type,leave_status,leave_duration,user_id) VALUES('" + LeaveId + "',' " + LeaveDate + "', '" + LeaveType + "', '" + LeaveStatus + "','" + LeaveDuration + "','"+userid+"')";

                cmd.Prepare();


                cmd.ExecuteNonQuery();
                //}


            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }
    }
}
