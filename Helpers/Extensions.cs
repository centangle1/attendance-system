﻿using CustomModels.Collection;
using CustomModels.Identity;
using EnumLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public static class Extensions
    {
        public static SearchResultViewModel<T> Paginate<T>(this IQueryable<T> query, GeneralSearchViewModel search, ApplicationDbContext db)
        {
            var result = new SearchResultViewModel<T>();
            if (search.Pagination)
            {
                var paginatedResult = query.Skip((search.CurrentPage - 1) * search.RecordsPerPage).Take(search.RecordsPerPage).ToList() as List<T>;
                paginatedResult = paginatedResult ?? new List<T>();
                if (search.CalculateTotal)
                {
                    result.TotalCount = query == null ? 0 : query.Count();
                }
            }
            else
            {
                try
                {
                    result.ResultList = query.ToList() as List<T>; 
                }
                catch (Exception ex)
                {
                    new Logs().Create(new LogSearchViewModel { Action = "Search", DataType = typeof(T).Name, Model = ex.StackTrace, SearchType = LogSearchType.Exception });
                }
            }
            return result;
        }
    }
}
