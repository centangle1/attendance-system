﻿using CustomModels.Collection;
using EntityProvider;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<T> GetLogs<T>(LogSearchViewModel search)
        {
            return new DataAccess().GetLogs<T>(search);
        }

        public Guid Create(LogSearchViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.DateTime = DateTime.Now;
            model.AddedBy = string.IsNullOrEmpty(model.AddedBy) ? HttpContext.Current.User.Identity.GetUserId() : model.AddedBy ;
            return new DataAccess().Create(model);
        }

    }
}
