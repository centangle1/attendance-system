﻿using BusinessLogic;
using ClosedXML.Excel;
using CustomModels.ReportModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using Microsoft.AspNet.Identity.EntityFramework;
using CustomModels.Identity;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using EnumLibrary;
using System.Data;
using System.ComponentModel;
using Helpers;
using CustomModels.Collection;
using AttendanceSystem.Models;

namespace AttendanceSystem.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        [Authorize]
        public ActionResult Index(int reportType)
        {
            ViewBag.ReportType = reportType;
            return View();
        }

        public PartialViewResult Daily()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            var line = sr.ReadToEnd();
            var model = JsonConvert.DeserializeObject<ReportDates>(line);
            model.LoggedInUser = User.Identity.GetUserId();
            new Logic().CheckFilters(model);
            var result = new Logic().DailyReportLogic(model);
            return PartialView(result);
        }

        public PartialViewResult DailyPartTime()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            var line = sr.ReadToEnd();
            var model = JsonConvert.DeserializeObject<ReportDates>(line);
            model.LoggedInUser = User.Identity.GetUserId();
            new Logic().CheckFilters(model);
            var result = new Logic().DailyReportPartTime(model);
            return PartialView(result);
        }

        public PartialViewResult Monthly()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            var line = sr.ReadToEnd();
            var model = JsonConvert.DeserializeObject<ReportDates>(line);
            model.StartTime = new DateTime(model.Month.Year, model.Month.Month, 1);
            model.EndTime = model.StartTime.AddMonths(1).AddDays(-1);
            model.LoggedInUser = User.Identity.GetUserId();
            new Logic().CheckFilters(model);
            //var result = new Logic().MonthlyReport(model);
            var result = new Logic().DailyReportLogic(model);

            result.RemoveAt(result.Count() - 1);
            return PartialView(result.GroupBy(x => x.Id));
        }

        public PartialViewResult MonthlyPartTime()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            var line = sr.ReadToEnd();
            var model = JsonConvert.DeserializeObject<ReportDates>(line);
            model.StartTime = new DateTime(model.Month.Year, model.Month.Month, 1);
            model.EndTime = model.StartTime.AddMonths(1).AddDays(-1);
            model.LoggedInUser = User.Identity.GetUserId();
            new Logic().CheckFilters(model);
            var result = new Logic().MonthlyReportPartTime(model);
            return PartialView(result);
        }

        public PartialViewResult MonthlyConsolidated()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            var line = sr.ReadToEnd();
            var model = JsonConvert.DeserializeObject<ReportDates>(line);
            model.StartTime = new DateTime(model.Month.Year, model.Month.Month, 1);
            model.EndTime = model.StartTime.AddMonths(1).AddDays(-1);
            model.LoggedInUser = User.Identity.GetUserId();
            new Logic().CheckFilters(model);
            var result = new Logic().MonthlyReport(model);
            return PartialView(result);
        }

        public PartialViewResult Weekly()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            var line = sr.ReadToEnd();
            var model = JsonConvert.DeserializeObject<ReportDates>(line);
            var weekString = model.Week.Split('-');
            var weekNumber = weekString[1].Remove(0, 1);
            var firstDay = new DateTime(int.Parse(weekString[0]), 01, 01);
            var day = new Helpers.Helpers().GetDayNumber(firstDay.DayOfWeek.ToString());
            model.StartTime = firstDay.AddDays((int.Parse(weekNumber) * 7) - (day) - 6);
            model.EndTime = model.StartTime.AddDays(6);
            model.LoggedInUser = User.Identity.GetUserId();
            new Logic().CheckFilters(model);
            var result = new Logic().MonthlyReport(model);
            return PartialView(result);
        }

        public PartialViewResult WeeklyPartTime()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            var line = sr.ReadToEnd();
            var model = JsonConvert.DeserializeObject<ReportDates>(line);
            var weekString = model.Week.Split('-');
            var weekNumber = weekString[1].Remove(0, 1);
            var firstDay = new DateTime(int.Parse(weekString[0]), 01, 01);
            var day = new Helpers.Helpers().GetDayNumber(firstDay.DayOfWeek.ToString());
            model.StartTime = firstDay.AddDays((int.Parse(weekNumber) * 7) - (day) - 6);
            model.EndTime = model.StartTime.AddDays(6);
            model.LoggedInUser = User.Identity.GetUserId();
            new Logic().CheckFilters(model);
            var result = new Logic().MonthlyReportPartTime(model);
            return PartialView(result);
        }

        public PartialViewResult SingleDayReport(string date)
        {
            return PartialView(new Logic().SingleDayReport(date));
        }


        public ActionResult LeaveDetailReport()
        {
            var result = new Logic().GetLeaveDetailsReport(new LeaveDetailSearchViewModel());
            return View(result);
        }

        public PartialViewResult LeaveDetailPartial()
        {
            return PartialView();
        }

        [Authorize]
        public JsonResult Company(string prefix, int pageSize, int pageNumber)
        {
            //var roles = new Helpers.Helpers().GetRoles(User.Identity.GetUserId());
            var companies = new Helpers.Helpers().Companies(prefix);
            var result = new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, companies);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Branch(string prefix, int pageSize, int pageNumber, Guid company)
        {
            var branches = new Helpers.Helpers().Branches(prefix, company);
            var result = new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, branches);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Department(string prefix, int pageSize, int pageNumber, Guid branch)
        {
            var departments = new Helpers.Helpers().Department(prefix, branch);
            var result = new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, departments);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Employee(string prefix, int pageSize, int pageNumber, Guid department)
        {
            var employees = new Helpers.Helpers().Employee(prefix, department);
            var result = new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, employees);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public FileResult ExportExcelSheet(ReportDates model)
        {
            //System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            //var line = sr.ReadToEnd();
            //var model = JsonConvert.DeserializeObject<ReportDates>(line);
            model.LoggedInUser = User.Identity.GetUserId();
            new Logic().CheckFilters(model);
            //var result = new Logic().DailyReportPartTime(model);

            ExcelReportLists reports = new ExcelReportLists();
            DataTable table = new DataTable();
            List<string> columnsToShow = new List<string>();
            Dictionary<string, string> AdditionalProperties = null;
            bool isDaily = false;
            bool isUserSpecific = string.IsNullOrEmpty(model.User)?false:true;
            var reportType = "";
            var headerColumns = new List<string>();
            if (model.Selection == ReportSelection.Daily)
            {
                reportType = "Daily";
                if (model.PartTime == null)
                {
                    reports.DailyList = new Logic().DailyReportLogic(model);
                    table = new Helpers.Helpers().ToDataTable(reports.DailyList);
                    isDaily = true;
                    columnsToShow = new List<string>()
                {
                    "Name","BranchName","Entry","Exit","StatusString","Arrival","Departure","WorkingHoursString","OvertimeString","ActualOverTimeString"

                };
                    headerColumns = new List<string> { "","Name","Day", "Entry", "Exit", "", "", "Arrival", "", "", "Departure", "","","","","","","","","","","","","Actual Overtime"
                        ,"Status", "Working Hours", "Overtime", "", "","","","","","","","","","","","Bracnh"};
                    if (!string.IsNullOrEmpty(model.User))
                    {
                        isUserSpecific = true;
                        var lastRecord = reports.DailyList.Last();
                        AdditionalProperties = new Dictionary<string, string>();
                        AdditionalProperties.Add("Working Hours", lastRecord.WorkingHoursString);
                        AdditionalProperties.Add("Presents", lastRecord.PresentCount.ToString());
                        AdditionalProperties.Add("Absents", lastRecord.AbsentCount.ToString());
                        AdditionalProperties.Add("Leaves", lastRecord.OnLeaveCount.ToString());
                        AdditionalProperties.Add("Early Arrivals", lastRecord.EarlyArrivalCount.ToString());
                        AdditionalProperties.Add("Late Arrivals", lastRecord.LateArrivalCount.ToString());
                        AdditionalProperties.Add("On Time Arrivals", lastRecord.OnTimeArrivalCount.ToString());
                        AdditionalProperties.Add("Early Departures", lastRecord.EarlyDepartureCount.ToString());
                        AdditionalProperties.Add("Late Departures", lastRecord.LateDepartureCount.ToString());
                        AdditionalProperties.Add("On Time Departures", lastRecord.OnTimeDepartureCount.ToString());
                    }
                }
                else
                {
                    reports.DailyPartTimeList = new Logic().DailyReportPartTime(model);
                    table = new Helpers.Helpers().ToDataTable(reports.DailyPartTimeList);
                    isDaily = true;
                    columnsToShow = new List<string>()
                {
                    "Name","BranchName","StartTime","EndTime","StatusString","TotalHoursString","WorkedHoursString","OvertimeString"

                };
                    if (!string.IsNullOrEmpty(model.User))
                    {
                        isUserSpecific = true;
                        var lastRecord = reports.DailyPartTimeList.Last();
                        AdditionalProperties = new Dictionary<string, string>();
                        AdditionalProperties.Add("Total Hours", lastRecord.TotalHoursString);
                        AdditionalProperties.Add("Worked Hours", lastRecord.WorkedHoursString);
                        AdditionalProperties.Add("Overtime", lastRecord.OvertimeString);
                        AdditionalProperties.Add("Presents", lastRecord.Presents.ToString());
                        AdditionalProperties.Add("Absents", lastRecord.Absents.ToString());
                        AdditionalProperties.Add("Leaves", lastRecord.Leaves.ToString());

                    }
                }
            }
            else if (model.Selection == ReportSelection.MonthlyConsolidated)
            {
                reportType = "Monthly";
                if (model.PartTime == null)
                {
                    model.StartTime = model.Month.Date;
                    model.EndTime = model.Month.AddMonths(1).AddMilliseconds(-1);
                    reports.MonthlyList = new Logic().MonthlyReport(model);
                    table = new Helpers.Helpers().ToDataTable(reports.MonthlyList);
                    columnsToShow = new List<string>()
                {
                    "Name","BranchName","Presents","Absents","Holidays","NationalHolidays","Leaves","FormattedOverTime","FormattedLate","FormattedWorkedHours","FormattedTotalHours","FormattedScheduledHours","ActualOverTime"
                };
                    headerColumns = new List<string>
                    {
                        "","Name","Presents","Absents","National Holidays","Holidays","Leaves","","","","","","Overtime","Worked Hours","Scheduled Hours", "Total Hours", "Late", "Overtime","Branch"
                    };
                }
                else
                {
                    model.StartTime = model.Month.Date;
                    model.EndTime = model.Month.AddMonths(1).AddMilliseconds(-1);
                    reports.MonthlyPartTimeList = new Logic().MonthlyReportPartTime(model);
                    table = new Helpers.Helpers().ToDataTable(reports.MonthlyPartTimeList);
                    columnsToShow = new List<string>()
                {
                     "Name","Presents","Absents","FormattedTotalHours","FormattedWorkedHours","FormattedOvertime"
                };
                }
            }
            else if (model.Selection == ReportSelection.Weekly)
            {
                reportType = "Weekly";
                if (model.PartTime == null)
                {
                    var weekString = model.Week.Split('-');
                    var weekNumber = weekString[1].Remove(0, 1);
                    var firstDay = new DateTime(int.Parse(weekString[0]), 01, 01);
                    var day = new Helpers.Helpers().GetDayNumber(firstDay.DayOfWeek.ToString());
                    model.StartTime = firstDay.AddDays((int.Parse(weekNumber) * 7) - (day) - 6);
                    model.EndTime = model.StartTime.AddDays(6);
                    reports.MonthlyList = new Logic().MonthlyReport(model);
                    table = new Helpers.Helpers().ToDataTable(reports.MonthlyList);
                    columnsToShow = new List<string>()
                {
                    "Name","BranchName","Presents","Absents","Holidays","NationalHolidays","Leaves","FormattedOverTime","FormattedLate","FormattedWorkedHours","FormattedTotalHours","FormattedScheduledHours","ActualOverTime"

                };
                    headerColumns = new List<string>
                    {
                        "","Name","Presents","Absents","National Holidays","Holidays","Leaves","","","","","","Overtime","Worked Hours","Scheduled Hours", "Total Hours", "Late", "Overtime","Branch"
                    };
                }
                else
                {
                    var weekString = model.Week.Split('-');
                    var weekNumber = weekString[1].Remove(0, 1);
                    var firstDay = new DateTime(int.Parse(weekString[0]), 01, 01);
                    var day = new Helpers.Helpers().GetDayNumber(firstDay.DayOfWeek.ToString());
                    model.StartTime = firstDay.AddDays((int.Parse(weekNumber) * 7) - (day) - 6);
                    model.EndTime = model.StartTime.AddDays(6);
                    reports.MonthlyPartTimeList = new Logic().MonthlyReportPartTime(model);
                    table = new Helpers.Helpers().ToDataTable(reports.MonthlyPartTimeList);
                    columnsToShow = new List<string>()
                {
                    "Name","Presents","Absents","FormattedTotalHours","FormattedWorkedHours","FormattedOvertime"
                };
                    headerColumns = new List<string>
                    {
                        "","Name","Presents","Absents","National Holidays","Holidays","Leaves","","","","","","Overtime","Worked Hours","Scheduled Hours", "Total Hours", "Late", "Overtime","Branch"
                    };
                }
            }
            if (isUserSpecific)
            {
                if (table.Rows.Count > 0)
                    table.Rows[table.Rows.Count - 1].Delete();

            }
            string FilePath = System.IO.Path.Combine(Server.MapPath("~/Reports/") + DateTime.Now.ToFileTimeUtc() + @".xlsx");
            Helpers.ExcelHelper.WriteToExcel(table, "Report", FilePath, reportType, columnsToShow, isDaily, isUserSpecific, AdditionalProperties, headerColumns);
            byte[] fileBytes = System.IO.File.ReadAllBytes(FilePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "AttendanceReport.xlsx");


        }

        public ActionResult Select2Prac()
        {
            var branches = new Logic().GetBranches();
            var chartResultList = new List<PieChartResult>();
            foreach(var branch in branches)
            {
                var parameters = new ReportDates {
                    BranchId = branch.Id, BranchName = branch.Name, StartTime=DateTime.Now, Selection = ReportSelection.Daily
                };
                var userProperties = new SelectUserProperties { UserId = Singleton.Instance.UserId, CompanyId = Singleton.Instance.CompanyId};
                var result = new Logic().GetNodeAttendanceResult(parameters, userProperties);
                chartResultList.Add(result);
            }
            TempData["chartResultList"] = chartResultList;
            return View();
        }

        public JsonResult GetUpdatedChartResult()
        {
          
            StreamReader sr = new StreamReader(Request.InputStream);
            var line = sr.ReadToEnd();
            var parameters = JsonConvert.DeserializeObject<ReportDates>(line);
            var userProperties = new SelectUserProperties { UserId = Singleton.Instance.UserId, CompanyId = Singleton.Instance.CompanyId};
            var result = new Logic().GetNodeAttendanceResult(parameters, userProperties);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
     public ActionResult Practice1()
        {
            return View();
        }

    }
}