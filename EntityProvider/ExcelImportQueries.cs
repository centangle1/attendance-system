﻿using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {

        public Company CheckCompany(string name)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.Companies.Where(m => m.Name == name).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new Company();
            }
        }

        public Branch CheckBranch(string name, Guid id)
        {
            try
            { 
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.Branches.Where(m => m.Name == name && m.CompanyId == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new Branch();
            }
        }

        public Department CheckDepartment(string name, Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.Departments.Where(m => m.Name == name && m.BranchCode==id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new Department();
            }
        }

        public ApplicationUser CheckUser(string email, Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.Users.Where(m => m.Email == email && m.DepartmentId==id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return new ApplicationUser();
            }
        }
    }
}
