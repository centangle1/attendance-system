﻿using EntityProvider;
using EnumLibrary;
using CustomModels.ReportModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomModels.Collection;
using Helpers;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<DailyReportModel> DailyReportLogic(ReportDates model)
        {
            var result = new DataAccess().DailyReportQuery(model);
            if (model.Status == ReportStatusSelection.Absent)
                result = result.Where(m => m.Status == AttendanceStatus.Absent).ToList();
            if (model.Status == ReportStatusSelection.Late)
                result = result.Where(m => m.Arrival == LateEarlyPolicy.Late).ToList();
            if (model.Status == ReportStatusSelection.OnLeave)
                result = result.Where(m => m.Status == AttendanceStatus.OnLeave).ToList();
            if (model.Status == ReportStatusSelection.Present)
                result = result.Where(m => m.Status == AttendanceStatus.Present).ToList();

            if(model.StartTime.TimeOfDay != new TimeSpan() || model.EndTime.Value.TimeOfDay != new TimeSpan())
            {
                result = model.GetPresentEmployees ? 
                    result.Where(m => m.Entry.TimeOfDay >= model.StartTime.TimeOfDay && m.Exit.TimeOfDay <= model.EndTime.Value.TimeOfDay).ToList()
                    : result.Where(m => (m.Entry.TimeOfDay >= model.StartTime.TimeOfDay && m.Exit.TimeOfDay <= model.EndTime.Value.TimeOfDay) || m.Status != AttendanceStatus.Present).ToList();
            }

            if (!string.IsNullOrEmpty(model.User))
            {
                var consolidatedRow = new DailyReportModel();
                consolidatedRow.IsUserSpecific = true;
                consolidatedRow.WorkingHours = new TimeSpan(result.Sum(m => m.WorkingHours.Ticks));
                consolidatedRow.Overtime = new TimeSpan(result.Sum(m => m.Overtime.Ticks));
                consolidatedRow.ActualOverTime = new TimeSpan(result.Sum(x => x.ActualOverTime.Ticks));
                consolidatedRow.PresentCount = result.Where(m => m.Status == AttendanceStatus.Present).Count();
                consolidatedRow.AbsentCount = result.Where(m => m.Status == AttendanceStatus.Absent).Count();
                consolidatedRow.OnLeaveCount = result.Where(m => m.Status == AttendanceStatus.OnLeave).Count();
                consolidatedRow.EarlyArrivalCount = result.Where(m => m.Arrival == LateEarlyPolicy.Early && m.Status != AttendanceStatus.Absent).Count();
                consolidatedRow.LateArrivalCount = result.Where(m => m.Arrival == LateEarlyPolicy.Late && m.Status != AttendanceStatus.Absent).Count();
                consolidatedRow.OnTimeArrivalCount = result.Where(m => m.Arrival == LateEarlyPolicy.OnTime && m.Status != AttendanceStatus.Absent).Count();
                consolidatedRow.EarlyDepartureCount = result.Where(m => m.Departure == LateEarlyPolicy.Early && m.Status != AttendanceStatus.Absent).Count();
                consolidatedRow.LateDepartureCount = result.Where(m => m.Departure == LateEarlyPolicy.Late && m.Status != AttendanceStatus.Absent).Count();
                consolidatedRow.OnTimeDepartureCount = result.Where(m => m.Departure == LateEarlyPolicy.OnTime && m.Status != AttendanceStatus.Absent).Count();
                result.Add(consolidatedRow);
            }

            return result;
        }

        public List<DailyReportPartTimeModel> DailyReportPartTime(ReportDates model)
        {
            var result = new DataAccess().DailyReportPartTime(model);
            if (!string.IsNullOrEmpty(model.User) && result.Count() > 0)
            {
                var totalHours = new TimeSpan(result.Sum(m => m.TotalHours.Ticks));
                var totalWorkedHours = new TimeSpan(result.Sum(m => m.WorkedHours.Ticks));
                var actualHoursWorked = new TimeSpan(result.Sum(m => m.ActualHoursWorked.Ticks));

                var consolidatedRow = new DailyReportPartTimeModel
                {
                    IsUserSpecific = true,
                    TotalHours = totalHours,
                    WorkedHours = actualHoursWorked > totalHours ? totalHours : actualHoursWorked,
                    Presents = result.Count(x => x.Status == AttendanceStatus.Present),
                    Absents = result.Count(x => x.Status == AttendanceStatus.Absent),
                    Name = result[0].Name != null ? result[0].Name : "",
                    Overtime = (actualHoursWorked - totalHours).TotalHours > 0 ? (actualHoursWorked - totalHours) : new TimeSpan(0, 0, 0)
                };
                result.Add(consolidatedRow);
                result.OrderBy(x => x.BranchName);
                return result;
            }
            return result;
        }

        public List<ConsolidatedReportModel> MonthlyReport(ReportDates model)
        {
            var result = new DataAccess().DailyReportQuery(model);
            //var dummyResult = new DataAccess().GetReport(model);
            List<ConsolidatedReportModel> consolidatedResult = new List<ConsolidatedReportModel>();
            var groupedResult = result.GroupBy(x => x.Id).ToList();
            foreach (var emp in groupedResult)
            {
                ConsolidatedReportModel consolidationModel = new ConsolidatedReportModel();
                consolidationModel.Id = emp.Select(x => x.Id).FirstOrDefault();
                consolidationModel.Name = emp.Select(x => x.Name).FirstOrDefault();
                consolidationModel.Presents = emp.Where(x => x.Status == AttendanceStatus.Present).Count();
                consolidationModel.Absents = emp.Where(x => x.Status == AttendanceStatus.Absent).Count();
                consolidationModel.Holidays = emp.Where(x => x.Status == AttendanceStatus.Holiday).Count();
                consolidationModel.NationalHolidays = emp.Where(x => x.Status == AttendanceStatus.NationalHoliday).Count();
                consolidationModel.Leaves = emp.Where(x => x.Status == AttendanceStatus.OnLeave).Count();
                consolidationModel.OverTime = new TimeSpan(emp.Sum(x => x.Overtime.Ticks));
                consolidationModel.Late = new TimeSpan(emp.Sum(x => x.ArrivalDelay.Ticks));
                consolidationModel.WorkedHours = new TimeSpan(emp.Sum(x => x.WorkingHours.Ticks));
                consolidationModel.TotalHours = new TimeSpan(emp.Sum(x => x.TotalHours.Ticks));
                consolidationModel.ScheduledHours = new TimeSpan(emp.Sum(x => x.ScheduledHours.Ticks));
                consolidationModel.BranchName = emp.Select(m => m.BranchName).FirstOrDefault();
                consolidationModel.ActualOverTime = new TimeSpan(emp.Sum(x => x.ActualOverTime.Ticks));
                consolidatedResult.Add(consolidationModel);
            }
            return consolidatedResult;
        }
        public List<ConsolidatedReportPartTimeModel> MonthlyReportPartTime(ReportDates model)
        {
            var result = new DataAccess().DailyReportPartTime(model);
            var dummyResult = new DataAccess().GetReport(model);
            List<ConsolidatedReportPartTimeModel> consolidatedResult = new List<ConsolidatedReportPartTimeModel>();
            var groupedResult = result.GroupBy(x => x.Id).ToList();
            foreach (var emp in groupedResult)
            {
                var totalHours = new TimeSpan(emp.Sum(m => m.TotalHours.Ticks));
                var totalWorkedHours = new TimeSpan(emp.Sum(m => m.WorkedHours.Ticks));
                var actualHoursWorked = new TimeSpan(emp.Sum(m => m.ActualHoursWorked.Ticks));
                ConsolidatedReportPartTimeModel consolidationModel = new ConsolidatedReportPartTimeModel();
                consolidationModel.Id = emp.Select(x => x.Id).FirstOrDefault();
                consolidationModel.Name = emp.Select(x => x.Name).FirstOrDefault();
                consolidationModel.Presents = emp.Where(x => x.Status == AttendanceStatus.Present).Count();
                consolidationModel.Absents = emp.Where(x => x.Status == AttendanceStatus.Absent).Count();
                consolidationModel.WorkedHours = actualHoursWorked > totalHours ? totalHours : actualHoursWorked;
                consolidationModel.TotalHours = totalHours;
                consolidationModel.OverTime = (actualHoursWorked - totalHours).TotalHours > 0 ? (actualHoursWorked - totalHours) : new TimeSpan(0, 0, 0);
                consolidatedResult.Add(consolidationModel);
            }
            return consolidatedResult;
        }

        public void CheckFilters(ReportDates filters)
        {
            new DataAccess().CheckFilters(filters);
        }

        public List<DailyReportModel> SingleDayReport(string date)
        {
            var selectedDate = DateTime.Parse(date);
            return new DataAccess().SingleDayReport(selectedDate);
        }

        public PieChartResult GetNodeAttendanceResult(ReportDates parameters, SelectUserProperties userProperties)
        {
            parameters.LoggedInUser = userProperties.UserId;
            parameters.CompanyId = userProperties.CompanyId;
            return new DataAccess().GetNodeAttendanceResult(parameters);
        }

        public Guid MakeAllUsersPermanant()
        {
            return new DataAccess().MakeAllUsersPermanant();
        }

        public ScheduleDetailReport CheckArrivalLeaveStatus(SchedulePolicyDetailViewModel sct, TimeSpan startTime, TimeSpan endTime)
        {
            return new DataAccess().CheckArrivalLeaveStatus(sct, startTime, endTime);
        }
    }
}
