﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AttendanceSystemWpf
{
    /// <summary>
    /// Interaction logic for Department.xaml
    /// </summary>
    public partial class Department : Window
    {
        public Department()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string cs = @"Data Source=.\SQLEXPRESS;Initial Catalog=AttendanceDb;Integrated Security=True";
            SqlConnection conn = null;


            try
            {

                int deptid = Convert.ToInt32(tb1.Text);
                string deptname = tb2.Text;
                int branchcode = Convert.ToInt32(tb3.Text);

                //for (DateTime a =st; a <=et ; a = st.AddSeconds(10))
                //{ 
                conn = new SqlConnection(cs);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;

                cmd.CommandText = "INSERT INTO Department(Dept_Id,Dept_Name,branch_code) VALUES('" + deptid + "',' " + deptname + "', '" + branchcode + "')";

                cmd.Prepare();


                cmd.ExecuteNonQuery();
                //}


            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }
    }
}
