﻿using AutoMapper;
using CustomModels.Collection;
using CustomModels.Identity;
using EnumLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public List<T> GetLogs<T>(LogSearchViewModel search)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.SearchType == LogSearchType.Exception)
                    {
                        return db.ExceptionLogs.Where(m =>
                        (search.Id == new Guid() || m.Id == search.Id) &&
                        (string.IsNullOrEmpty(search.Action) || m.Action == search.Action) &&
                        (string.IsNullOrEmpty(search.AddedBy) || m.AddedBy == search.AddedBy) &&
                        (string.IsNullOrEmpty(search.Model) || m.Model == search.Model) &&
                        (search.DateTime == new DateTime() || m.DateTime == search.DateTime)).ToList() as List<T>;
                    }
                    else if (search.SearchType == LogSearchType.General)
                    {
                        return db.GeneralLogs.Where(m =>
                        (search.Id == new Guid() || m.Id == search.Id) &&
                        (string.IsNullOrEmpty(search.Action) || m.Action == search.Action) &&
                        (string.IsNullOrEmpty(search.AddedBy) || m.AddedBy == search.AddedBy) &&
                        (string.IsNullOrEmpty(search.Model) || m.Model == search.Model) &&
                        (search.DateTime == new DateTime() || m.DateTime == search.DateTime)).ToList() as List<T>;
                    }
                    else if (search.SearchType == LogSearchType.LeaveSync)
                    {
                        return db.LeaveSynchroniztionLogs.Where(m =>
                        (search.Id == new Guid() || m.Id == search.Id) &&
                        (search.RuleId == new Guid() || m.RuleId == search.RuleId) &&
                        (string.IsNullOrEmpty(search.Action) || m.Action == search.Action) &&
                        (string.IsNullOrEmpty(search.AddedBy) || m.AddedBy == search.AddedBy) &&
                        (string.IsNullOrEmpty(search.Model) || m.Model == search.Model) &&
                        (search.DateTime == new DateTime() || m.DateTime == search.DateTime)).ToList() as List<T>;
                    }
                    return new List<T>();
                }
            }
            catch (Exception ex)
            {
                    return new List<T>();
            }
        }
        
        public Guid Create(LogSearchViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (model.SearchType == LogSearchType.Exception)
                    {
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<LogSearchViewModel, ExceptionLog>(); });
                        var iMapper = config.CreateMapper();
                        var exceptionLog = iMapper.Map<LogSearchViewModel, ExceptionLog>(model);
                        var result = db.ExceptionLogs.Add(exceptionLog);
                        db.SaveChanges();
                        return result.Id;
                    }
                    else if (model.SearchType == LogSearchType.General)
                    {
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<LogSearchViewModel, GeneralLog>(); });
                        var iMapper = config.CreateMapper();
                        var generalLog = iMapper.Map<LogSearchViewModel, GeneralLog>(model);
                        var result = db.GeneralLogs.Add(generalLog);
                        db.SaveChanges();
                        return result.Id;
                    }
                    else if (model.SearchType == LogSearchType.LeaveSync)
                    {
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<LogSearchViewModel, LeaveSynchroniztionLog>(); });
                        var iMapper = config.CreateMapper();
                        var leaveSynchroniztionLog = iMapper.Map<LogSearchViewModel, LeaveSynchroniztionLog>(model);
                        var result = db.LeaveSynchroniztionLogs.Add(leaveSynchroniztionLog);
                        db.SaveChanges();
                        return result.Id;
                    }

                    return new Guid();
}
            }
            catch (Exception ex)
            {

                return new Guid();
            }
        }

    }
}
