﻿function AddPagination() {
    var paginationhtml = "";
    var startDate = new Date($("#StartTime").val());
    var endDate = new Date($("#EndTime").val());
    for (var start = startDate; start <= endDate; start.setDate(start.getDate() + 1)) {
        paginationhtml += '<li><a class="dateSelected" href="#">' + $.datepicker.formatDate('MM dd, yy', new Date(start)); + '</a></li>';
    }
    $("#pagination").html(paginationhtml);
}
function InitializeControls() {
    $("#reportDisplay").hide();
    $(".weeklyInput").hide();
    $(".monthlyInput").hide();
    $("#pagination").show();
    //$("#pagination").toggle();
    $("#reportDisplay").toggle();
    $("#backToFilters").hide();
    $("#export").hide();
}
function GetAttendanceResults(date, month, week) {
    var url = "/Report/";

    if ($("#reportType").val() == "0")//Daily
    {
        url += "Daily";
    } else if ($("#reportType").val() == "1")//Weekly
    {
        url += "Weekly";
    } else if ($("#reportType").val() == "2")//Monthly
    {
        url += "Monthly";
    } else if ($("#reportType").val() == "3")//Monthly Consolidated
    {
        url += "MonthlyConsolidated";
    }

var formattedDate = new Date(date);

formattedDate.setHours(0 - formattedDate.getTimezoneOffset() / 60); //stringify changes timezone
var postData;
var end = null;

if ($("#reportType").val() == "0" && $("#employee").val() != "" && $("#employee").val() != null) {
    end = new Date();
    var result = $("#EndTime").val();
    end = new Date(result);
    end.setHours(0 - end.getTimezoneOffset() / 60);
}
postData = {
    "Month": month,
    "Week": week,
    "Selection": 0,
    "StartTime": formattedDate,
    "EndTime": end,
    "PartTime": 0,
    "Company": ($("#company").val() == '') ? '' : $("#company").val(),
    "Branch": ($("#branch").val() == '') ? '' : $("#branch").val(),
    "Department": ($("#department").val() == '') ? '' : $("#department").val(),
    "User": ($("#employee").val() == '') ? '' : $("#employee").val()
}

$.ajax({
    url: url,
    dataType: "text",
    type: "POST",
    data: JSON.stringify(postData),

})
    .done(function (data) {
        $("#reportDisplay").html(data);
    });

}
function AutoCompleteAjaxRequest(url, postData, response) {
    $.ajax({
        url: url,
        type: "Post",
        dataType: "json",
        data: postData,
        success: function (data) {
            response($.map(data, function (item) {
                return { label: item.Name, value: item.Name, Id: item.Id }
            }))
        }
    });
}

function InitializeSelect2() {
    var pageSize = 20;
    var optionListUrl = '@Url.Action("Company", "Report")';
    //
    //
    //Method which is to be called for populating options in dropdown //dynamically

    var id = "#company";
    var url = "/Company/Report";
    var dataArray = function (params) {
        params.page = params.page || 1;
        return {
            prefix: params.term,
            pageSize: pageSize,
            pageNumber: params.page
        }
    };
    Select2AutoCompleteAjax(id, url, dataArray, pageSize)

   //$("#company").select2(
   //     {
   //         ajax: {
   //             delay: 150,
   //             url: '@Url.Action("Company", "Report")',
   //             dataType: 'json',

   //             data: function (params) {
   //                 params.page = params.page || 1;
   //                 return {
   //                     prefix: params.term,
   //                     pageSize: pageSize,
   //                     pageNumber: params.page

   //                 };
   //             },
   //             processResults: function (data, params) {

   //                 params.page = params.page || 1;
   //                 return {
   //                     results: data.Results,
   //                     pagination: {
   //                         more: (params.page * pageSize) < data.Total
   //                     }
   //                 };
   //             }
   //         },
   //         placeholder: "-- Select --",
   //         minimumInputLength: 0,
   //         allowClear: true,
   //     });


        //$("#company").autocomplete({
        //    select: function (event, ui) {
        //        $("#company").attr("company-value", ui.item.Id);
        //    },
        //    source: function (request, response) {
        //        var data = { prefix: request.term };
        //        AutoCompleteAjaxRequest("/Report/Company", data, response);
        //    }
        //});

     
    var id = "#branch";
    var url = "/Branch/Report";
    var dataArray = function (params) {
        params.page = params.page || 1;
        return {
            prefix: params.term,
            pageSize: pageSize,
            pageNumber: params.page,
            company: $("#company").val()
        }
    };
    Select2AutoCompleteAjax(id, url, dataArray, pageSize)


    var id = "#department";
    var url = "/Department/Report";
    var dataArray = function (params) {
        params.page = params.page || 1;
        return {
            prefix: params.term,
            pageSize: pageSize,
            pageNumber: params.page,
            branch: $("#branch").val()
        }
    };
    Select2AutoCompleteAjax(id, url, dataArray, pageSize)


    var id = "#employee";
    var url = "/Employee/Report";
    var dataArray = function (params) {
        params.page = params.page || 1;
        return {
            prefix: params.term,
            pageSize: pageSize,
            pageNumber: params.page,
            department: $("#department").val(),
            partTime: $("#partTime").val()

        }

    };
    Select2AutoCompleteAjax(id, url, dataArray, pageSize)

}