﻿using EntityProvider;
using CustomModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<Schedule> GetAllSchedule()
        {
            return new DataAccess().GetAllSchedule();
        }

        public Guid AddSchedule(List<ScheduleDetail> model)
        {
            DataAccess da = new DataAccess();
            var result = da.AddSchedule(model);
            if (result != new Guid())
            {
                return result;
            }
            return new Guid();
        }

        public Guid EditSchedule(List<ScheduleDetail> model)
        {
            DataAccess da = new DataAccess();
            var result = da.EditSchedule(model);
            if (result != new Guid())
            {
                return result;
            }
            return new Guid();
        }

        public List<ScheduleDetail> GetScheduleDetail(Guid id)
        {
            return new DataAccess().GetScheduleDetail(id);
        }

        /// <summary>
        /// Gets a schedule from the database and map it to ScheduleViewModel and ScheduleDetailViewModel
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ScheduleViewModel</returns>
        public ScheduleViewModel GetScheduleForDisplay(Guid id)
        {
            var result = new DataAccess().GetScheduleDetail(id);
            var uniqueDays = result.Select(m => m.Day).Distinct().ToList();


            List<ScheduleDetailViewModel> scheduleDetailCollection = new List<ScheduleDetailViewModel>();
            var scheduleName = "";
            foreach (var day in uniqueDays)
            {
                ScheduleDetailViewModel sdvm = new ScheduleDetailViewModel();
                sdvm.day = day;

                List<PeriodsViewModel> periodCollection = new List<PeriodsViewModel>();
                var schedules = result.Where(m => m.Day == day).ToList();
                foreach (var period in schedules)
                {
                    PeriodsViewModel pvm = new PeriodsViewModel();
                    pvm.start = period.StartingTime;
                    pvm.end = period.EndingTime;
                    periodCollection.Add(pvm);
                }

                if (scheduleName == "")
                {
                    scheduleName = result.Select(m => m.Schedule.Name).FirstOrDefault();
                }
                sdvm.periods = periodCollection;
                scheduleDetailCollection.Add(sdvm);
            }
            scheduleDetailCollection = scheduleDetailCollection.OrderBy(m => m.day).ToList();
            var scheduleCollection = new ScheduleViewModel();
            scheduleCollection.Name = scheduleName;
            scheduleCollection.schedules = scheduleDetailCollection;
            return scheduleCollection;
        }

        public Guid DeleteSchedule(Guid id)
        {
            var result = new DataAccess().DeleteSchedule(id);
            return result;
        }

        public Guid AddSchedulePolicy(AttendancePolicy model, Guid id)
        {
            return new DataAccess().AddSchedulePolicy(model, id);
        }

        public AttendancePolicy GetAttendancePolicy(Guid id)
        {
            return new DataAccess().GetAttendancePolicy(id);
        }

        public Guid EditSchedulePolicy(AttendancePolicy model)
        {
            return new DataAccess().EditSchedulePolicy(model);
        }

        public Guid DeleteSchedulePolicy(AttendancePolicy model)
        {
            return new DataAccess().DeleteSchedulePolicy(model);
        }

        public Guid AddUserSchedule(Guid scheduleId, string userId)
        {
            return new DataAccess().AddUserSchedule(scheduleId, userId);
        }

        public string EditUserSchedule(UserScheduleViewModel model)
        {
            return new DataAccess().EditUserSchedule(model);
        }

        public UserScheduleViewModel GetSchedule(string id)
        {
            return new DataAccess().GetSchedule(id);
        }
    }
}
