﻿using CustomModels.Collection;
using EntityProvider;
using EnumLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<LeaveRuleViewModel> GetAllAssignedRules(LeaveRuleViewModel search)
        {
            return new DataAccess().GetAllAssignedRules(search);
        }

        public Guid Create(LeaveRuleViewModel model)
        {
            var check = new DataAccess().GetAllAssignedRules(new LeaveRuleViewModel { EmployeeType = model.EmployeeType, Type = model.Type, Rule = new LeaveRulesDetail { Id = model.Rule.Id }, IsDeleted = true });
            if (check.Count() > 0)
            {
                model.Id = check.First().Id;
                return new DataAccess().Edit(model);
            }
            else
            {
                model.Id = Guid.NewGuid();
                return new DataAccess().Create(model);
            }
        }

        public Guid Edit(LeaveRuleViewModel model)
        {
            return new DataAccess().Edit(model);
        }

        public string ProcessRulesForSync(string userId, LeaveType leaveType, EmployeeType employeeType, float value)
        {
            return Rules.ProcessRulesForSync(userId, leaveType, employeeType, value);
        }

        public string ProcessRulesForLeaveCreation(LeaveViewModel model)
        {
            model.EndingDate = model.EndingDate.AddDays(1).AddSeconds(-1);
            var overlapingLeaves = new DataAccess().GetLeaves<LeaveViewModel>(new LeaveViewModel { StartingDate = model.StartingDate, EndingDate = model.EndingDate, UserId = model.UserId, SkipLeave = model.SkipLeave, GetOverLappedLeave = true });

            if (overlapingLeaves.Count() == 0)
            {
                return Rules.ProcessRulesForLeaveCreation(model);
            }
            else
            {
                return "A leave for this date already exists.";
            }

        }
    }
}
