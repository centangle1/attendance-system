﻿using BusinessLogic;
using CustomModels.Collection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttendanceSystem.Controllers
{
    public class ScheduleController : Controller
    {
        // GET: Schedule
        [Authorize(Roles = "Administrator, Company Manager, Branch Manager, Department Manager")]
        public ActionResult Index()
        {
            return View(new Logic().GetAllSchedule());
        }

        public ActionResult Schedule(Guid? id)
        {
            if (id != null)
            {
                var scheduleCollection = new Logic().GetScheduleForDisplay(id.GetValueOrDefault());
                var Sched = JsonConvert.SerializeObject(scheduleCollection);
                ViewBag.result = new JArray(Sched);
                ViewBag.id = id;
            }
            else
            {
                ViewBag.result = JsonConvert.ToString(JsonConvert.SerializeObject(new JArray()));
            }
            return View();
        }


        public JsonResult AddSchedule(List<ScheduleDetail> detail)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            string line = "";
            line = sr.ReadToEnd();
            dynamic dynObj = JsonConvert.DeserializeObject(line);
            var jo = dynObj["result"].ToString();
            var obj = JArray.Parse(jo);
            var list = new List<ScheduleDetail>();
            var scheduleId = dynObj["scheduleId"].ToString();

            for (int i = 0; i < 7; i++)
            {
                foreach (var period in obj[i]["periods"])
                {
                    var schedule = new ScheduleDetail();
                    schedule.Day = i;
                    schedule.ScheduleName = dynObj["name"].ToString();
                    if (scheduleId != "")
                    { schedule.ScheduleId = Guid.Parse(scheduleId); }

                    schedule.Id = Guid.NewGuid();
                    schedule.StartingTime = period["start"];
                    schedule.EndingTime = period["end"];
                    schedule.Title = period["title"];
                    list.Add(schedule);
                }

            }

            Logic lg = new Logic();
            Guid result = scheduleId != "" ? result = lg.EditSchedule(list) : result = lg.AddSchedule(list);
            return result != new Guid() ? Json(new { success = true }) : Json(new { success = false });

        }

        public ActionResult Delete(Guid id)
        {
            var result = new Logic().DeleteSchedule(id);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        public ActionResult AddSchedulePolicy(Guid id)
        {
            TempData["id"] = id;
            return View();
        }
        [HttpPost]
        public ActionResult AddSchedulePolicy(AttendancePolicy model)
        {
            var id = Guid.Parse(TempData["id"].ToString());
            new Logic().AddSchedulePolicy(model, id);
            return RedirectToAction("Index");
        }

        public ActionResult EditSchedulePolicy(Guid id)
        {
            var result = new Logic().GetAttendancePolicy(id);
            return View(result);
        }

        public ActionResult EditSchdulePolicy(AttendancePolicy model)
        {
            var result = new Logic().EditSchedulePolicy(model);
            return RedirectToAction("Index");
        }

        public ActionResult DeleteSchedulePolicy(Guid id)
        {
            return View(new Logic().GetAttendancePolicy(id));
        }

        [HttpPost]
        public ActionResult DeleteSchedulePolicy(AttendancePolicy model)
        {
            new Logic().DeleteSchedulePolicy(model);
            return RedirectToAction("Index");
        }

        public ActionResult UserScheduleIndex(string id)
        {
            return View(new Logic().GetSchedule(id));
        }

        public ActionResult EditUserSchedule(string id)
        {
            TempData["schedules"] = new Logic().GetAllSchedule();
            return View(new Logic().GetSchedule(id));
        }

        [HttpPost]
        public ActionResult EditUserSchedule(UserScheduleViewModel model)
        {
            var result = new Logic().EditUserSchedule(model);
            return RedirectToAction("UserScheduleIndex", "Schedule", new { id = result });
        }

    }
}