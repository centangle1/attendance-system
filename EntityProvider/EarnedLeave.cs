﻿using CustomModels.Collection;
using CustomModels.Identity;
using EnumLibrary;
using System;
using System.Collections.Generic;
using EntityProvider;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EntityProvider
{
    public partial class DataAccess
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <returns> A list of filtered EarnedLeaveViewModels</returns>
        public List<EarnedLeaveViewModel> GetEarnedLeavesForUpdation(EarnedLeaveViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var rules = GetLeaveRuleDetail<LeaveRulesDetailViewModel>(new LeaveRulesDetailViewModel { RuleType = RuleType.EarnedLeaves });
                    var leaveDetails = GetLeaveDetail(new LeaveDetailSearchViewModel { SearchType = LeaveDetailSearchType.Report});
                    var employees = db.Users.Where(m => m.Active == true).ToList();
                    var earnedLeaveRecords = GetEarnedLeaveDetailForSource(new EarnedLeaveDetailSearchViewModel { EmployeeList = employees.Select(m => m.Id).ToList(), SearchType = LeaveDetailSearchType.Report });

                    var result = (from e in employees
                    join r in rules on e.Type equals r.EmployeeType
                    join ld in leaveDetails on e.Id equals ld.EmployeeId
                    let LastEndingDate = GetLastEarnedLeaveDate.Compile()(e, earnedLeaveRecords, r.Id, !r.IsCarriedOn)// If carried on, then we need to check last added date of earned leave no matter if it was added automatically or manually
                                  let RuleDurationInDays = RuleDurationInDays.Compile()(r.Duration)
                                  let MissedDays = DateTime.Now.Subtract(LastEndingDate).Days
                                  let existingLeaves = leaveDetails.Where(x => x.EmployeeId == e.Id && x.Type == r.LeaveType).FirstOrDefault()
                                  where(
                                      r.IsDeleted == false && e.Active == true &&
                                     (e.Id == ld.EmployeeId && ld.Type == r.LeaveType) &&
                                     (string.IsNullOrEmpty(search.Employee.Id) || e.Id == search.Employee.Id) &&
                                     (string.IsNullOrEmpty(search.Employee.UserName) || e.UserName.Contains(search.Employee.UserName)) &&
                                     (search.Rule.Id == new Guid() || r.Id == search.Rule.Id) &&
                                     (string.IsNullOrEmpty(search.Rule.Name) || r.Name.Contains(search.Rule.Name)) &&
                                     (MissedDays >= RuleDurationInDays)
                                 )
                                  select new EarnedLeaveViewModel
                                  {
                                      Checked = true,
                                      Employee = e,
                                      MissedCycles = MissedDays / RuleDurationInDays,
                                      Rule = r,
                                      StartingDate = LastEndingDate,
                                      EarnedLeaveCount = (MissedDays / RuleDurationInDays) * float.Parse(r.Value),
                                      ExistingLeaves = existingLeaves != null ? existingLeaves.Count : 0
                                  }).ToList();

                    //var result1 = employees.Join(leaveDetails, e=>e.Id, ld=>ld.EmployeeId, (e, ld) => new { e,ld})
                    //    .Join(rules, e => e.e.Type, lr => lr.EmployeeType, (e, lr) => new
                    //{
                    //    e = e.e,
                    //    lr,
                    //    ld  = e.ld,
                    //    lastEndingDate = GetLastEarnedLeaveDate.Compile()(e.e, earnedLeaveRecords, lr.Id, !lr.IsCarriedOn),
                    //    ruleDurationInDays = RuleDurationInDays.Compile()(lr.Duration)
                    //})
                    //                     .Select(m => new
                    //                     {
                    //                         e = m.e,
                    //                         lr = m.lr,
                    //                         ld = m.ld,
                    //                         lastEndingDate = m.lastEndingDate,
                    //                         ruleDurationInDays = m.ruleDurationInDays,
                    //                         missingDays = DateTime.Now.Subtract(m.lastEndingDate).Days,
                    //                         existingLeaves = leaveDetails.Where(x => x.EmployeeId == m.e.Id && x.Type == m.lr.LeaveType).FirstOrDefault()
                    //                     })
                    //                     .Where(m => m.lr.IsDeleted == false && m.lr.IsDeleted == false && m.e.Active == true &&
                    //                           (m.e.Id == m.ld.EmployeeId && m.lr.LeaveType == m.ld.Type) &&
                    //                           (string.IsNullOrEmpty(search.Employee.Id) || m.e.Id == search.Employee.Id) &&
                    //                           (string.IsNullOrEmpty(search.Employee.UserName) || m.e.UserName.Contains(search.Employee.UserName)) &&
                    //                           (search.Rule.Id == new Guid() || m.lr.Id == search.Rule.Id) &&
                    //                           (string.IsNullOrEmpty(search.Rule.Name) || m.lr.Name.Contains(search.Rule.Name)) &&
                    //                           (m.missingDays >= m.ruleDurationInDays))
                    //                     .Select(m => new EarnedLeaveViewModel
                    //                     {
                    //                         Checked = true,
                    //                         Employee = m.e,
                    //                         MissedCycles = m.missingDays / m.ruleDurationInDays,
                    //                         Rule = m.lr,
                    //                         StartingDate = m.lastEndingDate,
                    //                         EarnedLeaveCount = (m.missingDays / m.ruleDurationInDays) * float.Parse(m.lr.Value),
                    //                         ExistingLeaves = m.existingLeaves != null ? m.existingLeaves.Count : 0
                    //                     }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<EarnedLeaveViewModel>();
            }
        }

        Expression<Func<ApplicationUser, List<EarnedLeaveDetailViewModel>, Guid, bool, DateTime>> GetLastEarnedLeaveDate = (employee, logs, RuleId, CheckAutomaticSourceOnly) =>
                    logs.Where(m => (!CheckAutomaticSourceOnly || (m.Source == EarnedLeaveDetailSource.Automatic && m.RuleId == RuleId)) && m.EmployeeId == employee.Id).OrderByDescending(m => m.EndingDate).FirstOrDefault() == null ?
                    employee.AddedOn :
                    logs.Where(m => (!CheckAutomaticSourceOnly || (m.Source == EarnedLeaveDetailSource.Automatic && m.RuleId == RuleId)) && m.EmployeeId == employee.Id).OrderByDescending(m => m.EndingDate).FirstOrDefault().EndingDate;


        public Expression<Func<LeaveDurationType, int>> RuleDurationInDays = duration =>
            duration == LeaveDurationType.Weekly ? 7 :
            duration == LeaveDurationType.BiWeekly ? 14 :
            duration == LeaveDurationType.Monthly ? DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) :
            duration == LeaveDurationType.BiMonthly ? DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month) :
            duration == LeaveDurationType.Quarterly ? 365 / 4 :
            duration == LeaveDurationType.SemiAnnually ? 365 / 2 :
            duration == LeaveDurationType.Annually ? 365 :
            0;


        public List<EarnedLeaveDetail> GetEarnedLeaveDetail(EarnedLeaveDetailSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.SearchType == LeaveDetailSearchType.Report)
                    {
                        return db.EarnedLeaveDetails.Where(m => m.IsDeleted == search.IsDeleted &&
                                                            (search.LeaveType == 0 || m.LeaveType == search.LeaveType) &&
                                                            //(search.RuleId == new Guid() || m.RuleId == search.RuleId) &&
                                                            (string.IsNullOrEmpty(search.EmployeeId) || m.EmployeeId == search.EmployeeId) &&
                                                            (search.Source == 0 || m.Source == search.Source) &&
                                                            (search.EmployeeList.Count() == 0 || search.EmployeeList.Contains(m.EmployeeId))
                                                            ).OrderByDescending(m => m.AddedOn).ToList();
                    }
                    return db.EarnedLeaveDetails.Where(m => m.IsDeleted == search.IsDeleted &&
                                                             (search.LeaveType == 0 || m.LeaveType == search.LeaveType) &&
                                                             (search.Source == 0 || m.Source == search.Source) &&
                                                             (search.EmployeeList.Count() == 0 || search.EmployeeList.Contains(m.EmployeeId))
                                                             ).ToList();
                }
            }
            catch (Exception ex)
            {

                return new List<EarnedLeaveDetail>();
            }
        }
        public List<EarnedLeaveDetailViewModel> GetEarnedLeaveDetailForSource(EarnedLeaveDetailSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.EarnedLeaveDetails.Where(m => m.IsDeleted == search.IsDeleted &&
                                                             (search.LeaveType == 0 || m.LeaveType == search.LeaveType) &&
                                                             (search.Source == 0 || m.Source == search.Source) &&
                                                             (search.EmployeeList.Count() == 0 || search.EmployeeList.Contains(m.EmployeeId))
                                                             ).GroupBy(x => new { x.EmployeeId, x.Source, x.RuleId })
                                                             .Select(m => new EarnedLeaveDetailViewModel
                                                             {
                                                                 EmployeeId = m.Key.EmployeeId,
                                                                 RuleId = m.Key.RuleId,
                                                                 Source = m.Key.Source,
                                                                 EndingDate = m.Max(x => x.EndingDate),
                                                             }).ToList();

                }
            }
            catch (Exception ex)
            {

                return new List<EarnedLeaveDetailViewModel>();
            }
        }

        public Guid Create(EarnedLeaveDetail model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.EarnedLeaveDetails.Add(model);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                return new Guid();
            }
        }

        public Guid Edit(EarnedLeaveDetail model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.EarnedLeaveDetails.Find(model.Id);
                    record.UpdatedOn = model.UpdatedOn != new DateTime() ? model.UpdatedOn : record.UpdatedOn;
                    record.IsDeleted = model.IsDeleted;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }



    }
}
