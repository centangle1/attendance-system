﻿using EnumLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomModels.ReportModels;
using CustomModels.Identity;
using CustomModels.Collection;
using System.Web;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public List<DailyReportPartTimeModel> DailyReportPartTime(ReportDates filters)
        {
            try
            {
                var fromDate = filters.StartTime.Date;
                var toDate = filters.EndTime == null ? fromDate.AddDays(1).AddMinutes(-1) : (filters.EndTime > DateTime.Now) ? DateTime.Now : filters.EndTime.Value.Date;

                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var users = db.Users.Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
                        .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
                        .Join(db.Companies, b => b.b.CompanyId, c => c.Id, (b, c) => new { b, c })
                            .Where(m => m.b.d.u.Active && m.b.d.d.Active && m.b.b.Active && m.c.Active &&
                            (m.c.Id == filters.CompanyId || filters.CompanyId == new Guid()) &&
                            (m.b.b.Id == filters.BranchId || filters.BranchId == new Guid()) &&
                            (m.b.d.d.Id == filters.DepartmentId || filters.DepartmentId == new Guid()) &&
                            (m.b.d.u.Id == filters.User || string.IsNullOrEmpty(filters.User)))
                        .Select(m => new
                        {
                            Id = m.b.d.u.Id,
                            Active = m.b.d.u.Active,
                            DepartmentId = m.b.d.u.DepartmentId,
                            MachineId = m.b.d.u.MachineId,
                            Email = m.b.d.u.Email,
                            UserName = m.b.d.u.UserName
                        }).ToList()
                        .Select(m => new ApplicationUser
                        {
                            Id = m.Id,
                            Active = m.Active,
                            DepartmentId = m.DepartmentId,
                            MachineId = m.MachineId,
                            Email = m.Email,
                            UserName = m.UserName
                        }).ToList();
                    var userIds = users.Select(m => m.Id).ToList();

                    var workingHours = db.PartTimeUserDetails
                        .Where(m => userIds.Contains(m.UserId)).ToList();

                    var attendances = db.Users.Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
                        .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
                        .Join(db.Attendances, b => b.d.u.MachineId, a => a.MachineId, (b, a) => new { b, a })
                        .Join(db.Machines, a => a.b.b.Id, m => m.BranchId, (a, m) => new { a, m })
                        .Where(m => userIds.Contains(m.a.b.d.u.Id) && m.a.a.EntryTime >= fromDate && m.a.a.EntryTime <= toDate && m.a.a.MacAddress == m.m.MacAddress)
                        .Select(m => new AttendanceViewModel
                        {
                            Active = m.a.a.Active,
                            MacAddress = m.a.a.MacAddress,
                            BranchId = m.a.a.BranchId,
                            EntryTime = m.a.a.EntryTime,
                            Error = m.a.a.Error,
                            Id = m.a.a.Id,
                            MachineId = m.a.a.MachineId,
                            ManualEntry = m.a.a.ManualEntry,
                            UserId = m.a.b.d.u.Id
                        }).ToList();
                    var collectiveRecords = new List<DailyReportPartTimeModel>();

                    for (DateTime currentDate = fromDate.Date; currentDate.Date <= toDate.Date; currentDate = currentDate.AddDays(1))
                    {
                        foreach (var user in users)
                        {
                            var workinghours = workingHours.Where(m => m.UserId == user.Id).FirstOrDefault();
                            var userAttendances = attendances.Where(m => m.UserId == user.Id && m.EntryTime.Date == currentDate.Date).OrderBy(x => x.EntryTime).ToList();
                            int totalAttendance = userAttendances.Count();
                            //if (userAttendances.Count() > 0)
                            {
                                TimeSpan workedHours = new TimeSpan(0, 0, 0);
                                var startTime = new DateTime(0001, 01, 01);
                                var endTime = new DateTime(0001, 01, 01);
                                var totalTime = workingHours.Where(m => m.UserId == user.Id).Select(m => m.WorkingHours).FirstOrDefault();
                                var time = totalTime.Split(':');

                                var totalHours = new TimeSpan(int.Parse(time[0]), int.Parse(time[1]), 0);
                                totalHours = new TimeSpan(totalHours.Ticks / 7);
                                AttendanceStatus status = AttendanceStatus.Absent;

                                AmmendingAttendanceRecordPartTime(ref totalAttendance, userAttendances, currentDate, user, ref status);

                                CalulateWorkingHoursPartTime(userAttendances, ref workedHours);
                                startTime = userAttendances.Count() == 0 ? startTime : userAttendances.First().EntryTime;
                                endTime = userAttendances.Count() == 0 ? endTime : userAttendances.Last().EntryTime;
                                var record = new DailyReportPartTimeModel
                                {
                                    Id = user.Id,
                                    StartTime = status != AttendanceStatus.Present ? currentDate : startTime,
                                    EndTime = endTime,
                                    Name = user.UserName,
                                    Status = status,
                                    IsUserSpecific = false,
                                    TotalHours = totalHours,
                                    WorkedHours = workedHours > totalHours ? totalHours : workedHours,
                                    ActualHoursWorked = workedHours,
                                    Overtime = (workedHours.Ticks - totalHours.Ticks) > 0 ? workedHours - totalHours : new TimeSpan()
                                };
                                collectiveRecords.Add(record);
                            }
                        }
                    }

                    return collectiveRecords;
                }
            }
            catch (Exception ex)
            {
                return new List<DailyReportPartTimeModel>();
            }
        }


        //-------------------------------Daily  Report Query------------------------------------
        /// <summary>
        /// Daily Report Query
        /// </summary>
        /// <param name="filters"></param>
        /// <returns>list of DailyReportModel</returns>
        public List<DailyReportModel> DailyReportQuery(ReportDates filters)
        {
            var fromDate = filters.StartTime.Date;
            var toDate = (filters.EndTime == null) ? filters.StartTime.Date.AddDays(1).AddMilliseconds(-1) : ((filters.EndTime > DateTime.Now) ? DateTime.Now : filters.EndTime.Value.Date.AddDays(1).AddMilliseconds(-1));
            var allYears = new List<int>();
            for (int year = fromDate.Year; year <= toDate.Year; year++)
            {
                allYears.Add(year);
            }
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {


                    var schedules = db.UserSchedules.ToList();
                    var scheduleDetails = db.ScheduleDetails
                        .GroupJoin(db.SchedulePolicies, sd => sd.ScheduleId, sp => sp.ScheduleId, (sd, sp) => new { sd, sp = sp.FirstOrDefault() })
                        .GroupJoin(db.AttendancePolicies, sp => sp.sp.PolicyId, ap => ap.Id, (sp, ap) => new { sp = sp.sp, sd = sp.sd, ap = ap.FirstOrDefault() })
                        .Where(m => m.sd.Active)
                        .Select(m => new SchedulePolicyDetailViewModel
                        {
                            Day = m.sd.Day,
                            EndingTime = m.sd.EndingTime,
                            StartingTime = m.sd.StartingTime,
                            Id = m.sd.Id,
                            ScheduleId = m.sd.ScheduleId,
                            Policy = m.ap
                        })
                        .ToList();
                    /*db.Users.Where(m => m.Active).ToList()*/
                    var users = db.Users.Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
                        .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
                        .Join(db.Companies, b => b.b.CompanyId, c => c.Id, (b, c) => new { b, c })
                            .Where(m => m.b.d.u.Active && m.b.d.d.Active && m.b.b.Active && m.c.Active &&
                            (m.c.Id == filters.CompanyId || filters.CompanyId == new Guid()) &&
                            (m.b.b.Id == filters.BranchId || filters.BranchId == new Guid()) &&
                            (m.b.d.d.Id == filters.DepartmentId || filters.DepartmentId == new Guid()) &&
                            (m.b.d.u.Id == filters.User || string.IsNullOrEmpty(filters.User)))
                        .Select(m => new
                        {
                            Id = m.b.d.u.Id,
                            Active = m.b.d.u.Active,
                            DepartmentId = m.b.d.u.DepartmentId,
                            MachineId = m.b.d.u.MachineId,
                            Email = m.b.d.u.Email,
                            UserName = m.b.d.u.UserName
                        }).ToList()
                        .Select(m => new ApplicationUser
                        {
                            Id = m.Id,
                            Active = m.Active,
                            DepartmentId = m.DepartmentId,
                            MachineId = m.MachineId,
                            Email = m.Email,
                            UserName = m.UserName
                        }).ToList();
                    var nationalHolidays = db.NationalHolidays.Where(nh => nh.Active && (allYears.Contains(nh.StartDate.Year) || allYears.Contains(nh.EndDate.Year))).ToList();
                    var attendances = (from a in db.Attendances
                                       join u in db.Users on a.MachineId equals u.MachineId
                                       join b in db.Branches on a.BranchId equals b.Id
                                       join d in db.Departments on u.DepartmentId equals d.Id
                                       join c in db.Companies on b.CompanyId equals c.Id
                                       where (a.EntryTime >= fromDate && a.EntryTime <= toDate
                                       && (d.Id == filters.DepartmentId || filters.DepartmentId == new Guid())
                                       && (b.Id == filters.BranchId || filters.BranchId == new Guid())
                                       && (c.Id == filters.CompanyId || filters.CompanyId == new Guid())
                                       && (u.Id == filters.User || string.IsNullOrEmpty(filters.User))
                                       )
                                       select new AttendanceViewModel
                                       {
                                           Active = a.Active,
                                           BranchId = a.BranchId,
                                           EntryTime = a.EntryTime,
                                           MacAddress = a.MacAddress,
                                           Id = a.Id,
                                           Error = a.Error,
                                           MachineId = a.MachineId,
                                           ManualEntry = a.ManualEntry
                                       }).Distinct().OrderBy(x => x.EntryTime).ThenBy(m => m.Id).ToList();


                    var leaves = db.Leaves.Where(l => l.Active).ToList();
                    List<DailyReportModel> DailyReport = new List<DailyReportModel>();
                    foreach (var user in users)
                    {
                        var branch = db.Users.Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
                        .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
                        .Where(m => m.d.u.Id == user.Id).Select(m => m.b).FirstOrDefault();
                        var branchName = branch.Name;
                        var branchId = branch.Id;
                        var userSchedule = schedules.Where(s => s.UserId == user.Id).FirstOrDefault();
                        if (userSchedule != null)
                        {
                            for (DateTime currentdate = fromDate; currentdate <= toDate; currentdate = currentdate.AddDays(1))
                            {
                                List<AttendanceViewModel> userAttendances = new List<AttendanceViewModel>();
                                var scheduledTime = scheduleDetails.Where(sd => sd.ScheduleId == userSchedule.Id && sd.Day == (int)currentdate.DayOfWeek).ToList();
                                userAttendances = attendances.Where(m => m.MachineId == user.MachineId && m.BranchId == branchId && m.EntryTime.Date == currentdate.Date).OrderBy(x => x.EntryTime).Distinct().ToList();
                                var totalAttendance = userAttendances.Count();

                                if (totalAttendance > 0 && totalAttendance % 2 != 0)
                                {
                                    var duplicateAttendance = userAttendances.GroupBy(x => new { x.EntryTime.Hour, x.EntryTime.Minute }).ToList();
                                    if (duplicateAttendance.Count < totalAttendance)
                                    {
                                        userAttendances = new List<AttendanceViewModel>();
                                        foreach (var att in duplicateAttendance)
                                        {
                                            userAttendances.Add(att.OrderBy(x => x.EntryTime).FirstOrDefault());
                                        }
                                        totalAttendance = userAttendances.Count();
                                    }


                                }
                                AmmendingAttendanceRecord(ref totalAttendance, userAttendances, scheduledTime, currentdate, user);
                                int status = 0;
                                int dayStatus = 0;
                                AssignDayStatus(ref dayStatus, leaves, user, currentdate, nationalHolidays, scheduledTime);
                                AssignStatus(userAttendances, ref status, leaves, user, currentdate, nationalHolidays, scheduledTime);
                                var reportModel = new DailyReportModel();
                                if (dayStatus == (int)AttendanceDayStatus.Regular)
                                {
                                    reportModel.ScheduledHours = new TimeSpan(scheduledTime.Sum(x => x.EndingTime.Ticks)) - new TimeSpan(scheduledTime.Sum(x => x.StartingTime.Ticks));
                                }
                                //------------------------------Calculating Working Hours-----------------------------
                                CalculateWorkingHours(status, totalAttendance, userAttendances, scheduledTime, reportModel, nationalHolidays, currentdate, dayStatus);

                                reportModel.BranchName = branchName;
                                reportModel.Id = user.Id;
                                reportModel.Name = users.Where(m => m.Id == user.Id).Select(u => u.UserName).FirstOrDefault();
                                reportModel.Entry = userAttendances.Where(en => en.MachineId == user.MachineId).Select(a => a.EntryTime).OrderBy(x => x.Hour).FirstOrDefault();
                                reportModel.Exit = userAttendances.Where(ex => ex.MachineId == user.MachineId).Select(a => a.EntryTime).OrderBy(x => x.Hour).LastOrDefault();
                                reportModel.Status = (AttendanceStatus)status;
                                reportModel.Day = (WeekDays)currentdate.DayOfWeek;

                                if (reportModel.ScheduleDetails.Where(x => x.ArrivalStatus == LateEarlyPolicy.Late).Count() > 0)
                                {
                                    reportModel.Arrival = LateEarlyPolicy.Late;
                                }
                                else if (reportModel.ScheduleDetails.Where(x => x.ArrivalStatus == LateEarlyPolicy.Early).Count() > 0)
                                {
                                    reportModel.Arrival = LateEarlyPolicy.Early;
                                }
                                else
                                {
                                    reportModel.Arrival = LateEarlyPolicy.OnTime;
                                }
                                if (reportModel.ScheduleDetails.Where(x => x.DepartureStatus == LateEarlyPolicy.Late).Count() > 0)
                                {
                                    reportModel.Departure = LateEarlyPolicy.Late;
                                }
                                else if (reportModel.ScheduleDetails.Where(x => x.DepartureStatus == LateEarlyPolicy.Early).Count() > 0)
                                {
                                    reportModel.Departure = LateEarlyPolicy.Early;
                                }
                                else
                                {
                                    reportModel.Departure = LateEarlyPolicy.OnTime;
                                }
                                if (reportModel.Status != AttendanceStatus.Present)
                                {
                                    reportModel.Entry = currentdate;
                                    reportModel.Exit = currentdate;
                                }
                                reportModel.ArrivalDelay = new TimeSpan(reportModel.ScheduleDetails.Where(x => x.ArrivalStatus == LateEarlyPolicy.Late).Sum(x => x.ArrivalDifference.Ticks));
                                reportModel.EarlyArrival = new TimeSpan(reportModel.ScheduleDetails.Where(x => x.ArrivalStatus == LateEarlyPolicy.Early).Sum(x => x.ArrivalDifference.Ticks));
                                reportModel.DepartureDelay = new TimeSpan(reportModel.ScheduleDetails.Where(x => x.DepartureStatus == LateEarlyPolicy.Late).Sum(x => x.DepartureDiffernce.Ticks));
                                reportModel.EarlyDeparture = new TimeSpan(reportModel.ScheduleDetails.Where(x => x.DepartureStatus == LateEarlyPolicy.Early).Sum(x => x.DepartureDiffernce.Ticks));
                                DailyReport.Add(reportModel);
                            }
                        }
                    }

                    return DailyReport.OrderBy(x => x.BranchName).ThenBy(m => m.Entry).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<DailyReportModel>();
            }
        }

        public List<DailyReportModel> SingleDayReport(DateTime date)
        {
            //try
            //{
            //    using(ApplicationDbContext db = new ApplicationDbContext())
            //    {
            //        var 
            //    }
            //}
            return new List<DailyReportModel>();
        }

        public PieChartResult GetNodeAttendanceResult(ReportDates parameters)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                { 
                    var usersStatus = DailyReportQuery(parameters);
                    float totalUsers = usersStatus.Count();
                    float presents = usersStatus.Where(m => m.Status == AttendanceStatus.Present).Count();
                    float absents = usersStatus.Where(m => m.Status == AttendanceStatus.Absent).Count();
                    float leaves = usersStatus.Where(m => m.Status == AttendanceStatus.OnLeave).Count();
                    float presentPercentange = (presents / totalUsers) * 100;
                    float absentPercentage = (absents /totalUsers) * 100;
                    float leavePercentage = (leaves/totalUsers) * 100;
                    
                    var results = new PieChartResult
                    {
                        Title = parameters.BranchName,
                        Leaves = double.IsNaN(Convert.ToDouble(leavePercentage))?0: leavePercentage,
                        Presents = double.IsNaN(Convert.ToDouble(presentPercentange))?0: presentPercentange,
                        Absents = double.IsNaN(Convert.ToDouble(absentPercentage)) ? 100 : absentPercentage,
                        BranchId = parameters.BranchId
                    };
                    return results;
                }
            }
            catch (Exception ex)
            {
                return new PieChartResult();
            }
        }
        public class TestModel
        {
            public ApplicationUser User { get; set; }
            public UserSchedule Schedule { get; set; }
        }
        public Guid MakeAllUsersPermanant()
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var users = db.Users.GroupJoin(db.UserSchedules, u => u.Id, us => us.UserId, (u, us) => new { user = u, userSchedule = us.FirstOrDefault()})
                        .Select(m => new
                        {
                            user = m.user,
                            userSchedule = m.userSchedule
                        }).ToList().DefaultIfEmpty()
                        .Select(m => new TestModel
                        {
                            User = m.user,
                            Schedule = m.userSchedule                            
                        }).ToList();

                    foreach(var user in users)
                    {
                        if (user.Schedule == null)
                        {
                            db.UserSchedules.Add(new UserSchedule { Id = Guid.Parse("0A731ADB-CF53-40C2-B2A3-29786A9B32FF"), UserId = user.User.Id });
                            db.SaveChanges();
                        }
                        if (user.User.Permanant == false)
                        {
                            var userData = db.Users.Find(user.User.Id);
                            userData.Permanant = true;
                            db.Entry(userData).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    return Guid.NewGuid();
                }
            }
            catch(Exception ex)
            {
                return new Guid();
            }
        }

        #region[Helper Function]

        private void AmmendingAttendanceRecord(ref int totalAttendance, List<AttendanceViewModel> userAttendances, List<SchedulePolicyDetailViewModel> scheduledTime, DateTime currentdate, ApplicationUser user)
        {
            if (totalAttendance > 0 && totalAttendance % 2 != 0)
            {
                var ExitDateTime = new DateTime();
                var lastEntryTime = userAttendances.Last().EntryTime;
                if (scheduledTime.Count > 0)
                    ExitDateTime = new DateTime(currentdate.Year, currentdate.Month, currentdate.Day, scheduledTime.Last().EndingTime.Hours, scheduledTime.Last().EndingTime.Minutes, scheduledTime.Last().EndingTime.Seconds);//Calculate According to policy
                else// If schedule doesnot exist
                {
                    ExitDateTime = lastEntryTime;
                }
                if (lastEntryTime <= ExitDateTime)
                {
                    userAttendances.Add(new CustomModels.Collection.AttendanceViewModel()
                    {
                        Id = Guid.NewGuid(),
                        EntryTime = ExitDateTime,
                        MachineId = user.MachineId,
                        MacAddress = userAttendances[0].MacAddress,
                        BranchId = userAttendances[0].BranchId,
                        Active = true,
                        ManualEntry = true,
                    });
                    totalAttendance++;
                }
                else//If entry is later than last schedule exit.
                {
                    if (lastEntryTime >= ExitDateTime)
                    {
                        userAttendances.Add(new CustomModels.Collection.AttendanceViewModel()
                        {
                            Id = Guid.NewGuid(),
                            EntryTime = lastEntryTime,
                            MachineId = user.MachineId,
                            MacAddress = userAttendances[0].MacAddress,
                            BranchId = userAttendances[0].BranchId,
                            Active = true,
                            ManualEntry = true,
                        });
                        totalAttendance++;
                    }
                }
            }
        }

        private void AssignStatus(List<AttendanceViewModel> userAttendances, ref int status, List<Leave> leaves, ApplicationUser user, DateTime currentdate, List<NationalHoliday> nationalHolidays, List<SchedulePolicyDetailViewModel> scheduledTime)
        {
            if (userAttendances.Count() > 0)
            {
                status = (int)AttendanceStatus.Present;
                return;
            }
            var userNationalHoliday = nationalHolidays.Where(nh => currentdate.Date >= nh.StartDate && currentdate.Date < nh.EndDate).FirstOrDefault();
            if (userNationalHoliday != null)
            {
                status = (int)AttendanceStatus.NationalHoliday;
                return;
            }
            if (scheduledTime.Count() > 0)
            {
                var userLeave = leaves.Where(l => l.UserId == user.Id && (currentdate.Date >= l.StartingDate && currentdate.Date <= l.EndingDate)).FirstOrDefault();
                if (userLeave == null)
                {
                    status = (int)AttendanceStatus.Absent;
                }
                else
                {
                    status = (int)AttendanceStatus.OnLeave;
                }


            }
            else
            {
                status = (int)AttendanceStatus.Holiday;
            }
        }

        private void AssignDayStatus(ref int dayStatus, List<Leave> leaves, ApplicationUser user, DateTime currentdate, List<NationalHoliday> nationalHolidays, List<SchedulePolicyDetailViewModel> scheduledTime)
        {
            var userNationalHoliday = nationalHolidays.Where(nh => currentdate.Date >= nh.StartDate && currentdate.Date < nh.EndDate).FirstOrDefault();
            if (userNationalHoliday != null)
            {
                dayStatus = (int)AttendanceDayStatus.NationalHoliday;
                return;
            }
            if (scheduledTime.Count() <= 0)
            {
                dayStatus = (int)AttendanceDayStatus.Holiday;
                return;
            }
            var userLeave = leaves.Where(l => l.UserId == user.Id && (currentdate.Date >= l.StartingDate && currentdate.Date < l.EndingDate)).FirstOrDefault();
            if (userLeave != null)
            {
                dayStatus = (int)AttendanceDayStatus.OnLeave;
                return;
            }
            dayStatus = (int)AttendanceDayStatus.Regular;

        }

        private void CalculateWorkingHours(int status, int totalAttendance, List<AttendanceViewModel> userAttendances, List<SchedulePolicyDetailViewModel> scheduledTime, DailyReportModel reportModel, List<NationalHoliday> nationalHolidays, DateTime currentdate, int dayStatus)
        {
            try
            {
                TimeSpan entryDifference = new TimeSpan(0, 0, 0);
                TimeSpan exitDifference = new TimeSpan(0, 0, 0);
                TimeSpan workingHours = new TimeSpan(0, 0, 0);
                TimeSpan totalHours = new TimeSpan(0, 0, 0);
                TimeSpan arrivalDelay = new TimeSpan(0, 0, 0);
                TimeSpan earlyArrival = new TimeSpan(0, 0, 0);
                TimeSpan departureDelay = new TimeSpan(0, 0, 0);
                TimeSpan earlyDeparture = new TimeSpan(0, 0, 0);
                TimeSpan actualOverTime = new TimeSpan(0, 0, 0);
                TimeSpan totalScheduledTime = new TimeSpan(0, 0, 0);
                reportModel.ScheduleDetails = new List<ScheduleDetailReport>();

                if (status == (int)AttendanceStatus.Present)
                {
                    for (int index = 0; index < totalAttendance; index += 2)
                    {
                        var startTime = new TimeSpan(userAttendances[index].EntryTime.Hour, userAttendances[index].EntryTime.Minute, userAttendances[index].EntryTime.Second);
                        var endTime = new TimeSpan(userAttendances[index + 1].EntryTime.Hour, userAttendances[index + 1].EntryTime.Minute, userAttendances[index + 1].EntryTime.Second);
                        totalHours += endTime - startTime;
                        foreach (var sct in scheduledTime)
                        {
                            ScheduleDetailReport detail = CheckArrivalLeaveStatus(sct, startTime, endTime);
                            reportModel.ScheduleDetails.Add(detail);
                            if (startTime >= sct.StartingTime) //Later or equal than start time
                            {
                                if (startTime < sct.EndingTime && endTime > sct.EndingTime) //Came late and went late
                                {
                                    workingHours += sct.EndingTime - startTime;
                                }
                                else if (startTime < sct.EndingTime)//Came late and went early
                                {
                                    workingHours += endTime - startTime;
                                }
                            }
                            else  //Came Early
                            {
                                if (endTime > sct.StartingTime && endTime < sct.EndingTime) //Came Early and went Early
                                {
                                    workingHours += endTime - sct.StartingTime;
                                }
                                else if (endTime > sct.StartingTime)//Came early and went late
                                {
                                    workingHours += sct.EndingTime - sct.StartingTime;
                                }
                            }
                            if (index == 0)
                            {
                                totalScheduledTime += sct.EndingTime - sct.StartingTime;
                            }
                        }

                    }

                }
                if (dayStatus != (int)AttendanceDayStatus.Regular && status == (int)AttendanceStatus.Present)
                {
                    reportModel.WorkingHours = TimeSpan.Zero;
                    reportModel.Overtime = totalHours;
                }
                else
                {
                    reportModel.WorkingHours = workingHours;
                    reportModel.Overtime = totalHours - workingHours;
                    if (status == (int)AttendanceStatus.Present)
                    {
                        reportModel.WorkingHours = workingHours;
                        reportModel.Overtime = totalHours - workingHours;
                    }
                }
                var difference = (totalScheduledTime - totalHours) > TimeSpan.Zero ? totalScheduledTime - totalHours : TimeSpan.Zero;
                reportModel.ActualOverTime = reportModel.Overtime - difference;
                reportModel.TotalHours = totalHours;
                reportModel.ArrivalDelay = arrivalDelay;
                reportModel.EarlyArrival = earlyArrival;
                reportModel.DepartureDelay = departureDelay;
                reportModel.EarlyDeparture = earlyDeparture;
                reportModel.IsUserSpecific = false;
            }
            catch (Exception ex)
            {

            }

        }

        public ScheduleDetailReport CheckArrivalLeaveStatus(SchedulePolicyDetailViewModel sct, TimeSpan startTime, TimeSpan endTime)
        {
            var scheduleDetails = new ScheduleDetailReport();
            var ArrivalTimeDifference = sct.StartingTime - startTime;
            var DepartureTimeDifference = endTime - sct.EndingTime;
            sct.Policy = sct.Policy != null ? sct.Policy : new AttendancePolicy();
            sct.Policy.Early = sct.Policy.Early != 0 ? sct.Policy.Early : 30;
            sct.Policy.Late = sct.Policy.Late != 0 ? sct.Policy.Late : 30;

            scheduleDetails.ArrivalStatus = (ArrivalTimeDifference.TotalMinutes < -sct.Policy.Late) ? LateEarlyPolicy.Late :
                      (ArrivalTimeDifference.TotalMinutes > sct.Policy.Early) ? LateEarlyPolicy.Early : LateEarlyPolicy.OnTime;
            if (scheduleDetails.ArrivalStatus == LateEarlyPolicy.Early)
            {
                scheduleDetails.ArrivalDifference = ArrivalTimeDifference.Duration();
            }
            else if (scheduleDetails.ArrivalStatus == LateEarlyPolicy.Late)
            {
                scheduleDetails.ArrivalDifference = ArrivalTimeDifference.Duration();
            }
            scheduleDetails.DepartureStatus = (DepartureTimeDifference.TotalMinutes < -sct.Policy.Early) ? LateEarlyPolicy.Early :
                       (DepartureTimeDifference.TotalMinutes > sct.Policy.Late) ? LateEarlyPolicy.Late : LateEarlyPolicy.OnTime;
            if (scheduleDetails.DepartureStatus == LateEarlyPolicy.Early)
            {
                scheduleDetails.DepartureDiffernce = DepartureTimeDifference.Duration();
            }
            else if (scheduleDetails.DepartureStatus == LateEarlyPolicy.Late)
            {
                scheduleDetails.DepartureDiffernce = DepartureTimeDifference.Duration();
            }
            return scheduleDetails;
        }

        public void CheckFilters(ReportDates filters)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var users = new List<ApplicationUser>();
                    Guid check = new Guid();
                    if (HttpContext.Current.User.IsInRole("Company Manager"))
                    {
                        check = db.Companies.Join(db.Branches, c => c.Id, b => b.CompanyId, (c, b) => new { c, b })
                                .Join(db.Departments, b => b.b.Id, d => d.BranchCode, (b, d) => new { b, d })
                                .Join(db.Users, d => d.d.Id, u => u.DepartmentId, (d, u) => new { d, u })
                                .Where(u => u.u.Id == filters.LoggedInUser)
                                .Select(m => m.d.b.c.Id).FirstOrDefault();
                        filters.CompanyId = filters.CompanyId != check ? check : filters.CompanyId;


                    }
                    else if (HttpContext.Current.User.IsInRole("Branch Manager"))
                    {
                        check = db.Branches.Join(db.Departments, b => b.Id, d => d.BranchCode, (b, d) => new { b, d })
                            .Join(db.Users, d => d.d.Id, u => u.DepartmentId, (d, u) => new { d, u })
                            .Where(m => m.u.Id == filters.LoggedInUser)
                            .Select(m => m.d.b.Id).FirstOrDefault();
                        filters.BranchId = filters.BranchId == new Guid() ? check : filters.BranchId;


                    }
                    else if (HttpContext.Current.User.IsInRole("Department Manager"))
                    {
                        check = db.Departments.Join(db.Users, d => d.Id, u => u.DepartmentId, (d, u) => new { d, u })
                            .Where(m => m.u.Id == filters.LoggedInUser)
                            .Select(m => m.d.Id).FirstOrDefault();
                        filters.DepartmentId = filters.DepartmentId != check ? check : filters.DepartmentId;
                    }
                    else if (HttpContext.Current.User.IsInRole("Member"))
                    {
                        check = Guid.Parse(filters.LoggedInUser);
                        filters.User = filters.User != check.ToString() ? check.ToString() : filters.User;
                    }


                }
            }
            catch (Exception ex)
            {

            }
        }

        public void AmmendingAttendanceRecordPartTime(ref int totalAttendance, List<AttendanceViewModel> userAttendances, DateTime currentDate, ApplicationUser user, ref AttendanceStatus status)
        {
            if (totalAttendance > 0 && totalAttendance % 2 != 0)
            {
                status = AttendanceStatus.Present;
                userAttendances.Add(new AttendanceViewModel
                {
                    Active = true,
                    Id = Guid.NewGuid(),
                    MacAddress = userAttendances[totalAttendance - 1].MacAddress,
                    BranchId = userAttendances[totalAttendance - 1].BranchId,
                    EntryTime = userAttendances[totalAttendance - 1].EntryTime.AddMinutes(1),
                    MachineId = userAttendances[totalAttendance - 1].MachineId,
                    ManualEntry = true,
                    UserId = userAttendances[totalAttendance - 1].UserId
                });
                totalAttendance++;
            }
            else if (totalAttendance <= 0)
            {
                status = AttendanceStatus.Absent;
            }
            else
            {
                status = AttendanceStatus.Present;
            }
        }

        public void CalulateWorkingHoursPartTime(List<AttendanceViewModel> userAttendance, ref TimeSpan workedHours)
        {
            for (int index = 0; index < userAttendance.Count(); index += 2)
            {
                workedHours += userAttendance[index + 1].EntryTime.TimeOfDay - userAttendance[index].EntryTime.TimeOfDay;
            }
        }
        #endregion
    }
}
