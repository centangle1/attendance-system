﻿using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Helpers;
using AutoMapper;
using System.Data.Entity;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetNotifications<T>(NotificationViewModel search)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = db.Notifications.Join(db.Users, n => n.UserId, u => u.Id, (n, u) => new { n, u })
                                                 .Where(m => m.u.Active &&
                                                            m.n.IsDeleted == search.IsDeleted &&
                                                            (string.IsNullOrEmpty(search.EmployeeId) || m.n.UserId == search.EmployeeId) &&
                                                            (search.DateTime == new DateTime() || m.n.DateTime == search.DateTime) &&
                                                            (search.CommunicateBy == 0 || m.n.CommunicateBy == search.CommunicateBy) &&
                                                            (search.Id == new Guid() || m.n.Id == search.Id) &&
                                                            (string.IsNullOrEmpty(search.EmployeeName) || m.u.UserName == search.EmployeeName) &&
                                                            (search.AllStatus || m.n.Status == search.Status)
                                                        )
                                                 .Select(m => new NotificationViewModel
                                                 {
                                                     CommunicateBy = m.n.CommunicateBy,
                                                     DateTime = m.n.DateTime,
                                                     Employee = m.u,
                                                     Id = m.n.Id,
                                                     Message = m.n.Message,
                                                     Status = m.n.Status
                                                 }).AsQueryable();

                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {

                return new SearchResultViewModel<T>();
            }
        }

        public Guid Create(NotificationViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<NotificationViewModel, Notification>().ForMember(d => d.UserId, opt => opt.MapFrom(s => s.EmployeeId)); });
                    var iMapper = config.CreateMapper();
                    var record = iMapper.Map<NotificationViewModel, Notification>(model);
                    record.Employee = null;
                    var result = db.Notifications.Add(record);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid Edit(NotificationViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Notifications.Find(model.Id);
                    if (!record.IsDeleted)
                    {
                        record.CommunicateBy = model.CommunicateBy == 0 ? record.CommunicateBy : model.CommunicateBy;
                        //record.DateTime = model.DateTime == new DateTime() ? record.DateTime : model.DateTime ;
                        //record.UserId = string.IsNullOrEmpty(model.Employee.Id) ? record.UserId : model.Employee.Id;
                        record.IsDeleted = model.IsDeleted;
                        record.Message = string.IsNullOrEmpty(model.Message) ? record.Message : model.Message;
                        record.Status = model.Status;
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();
                        return model.Id;
                    }
                    else
                        return new Guid();
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }
    }
}
