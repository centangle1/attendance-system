﻿using BusinessLogic;
using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomModels.ReportModels;
using AttendanceSystem.Models;
using System.Net.Http;
using System.IO;

namespace AttendanceSystem.Controllers
{
    public class AttendanceController : Controller
    {
        //GET: Attendance
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetAttendances(DataTableAjaxPostModel searchModel)
        {
            var userParameters = new UserParameters();

            //user parameters
            TempData["userId"] = Singleton.Instance.UserId;
            var userProperties = new Helpers.Helpers().GetUserProperties(Singleton.Instance.UserId);
            userParameters.UserId = userProperties.UserId;
            userParameters.MachineId = userProperties.MachineId;
            userParameters.UserRole = userProperties.UserRole;
            userParameters.AjaxSearchParameters = searchModel;

            var result = new Logic().ViewAllAttendance(userParameters);

            return Json(new { data = result.Attendances, draw = Request.Form["draw"], recordsTotal = result.TotalRecordCount, recordsFiltered = result.FilteredRecordCount });

        }

        public string Create()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            bool result = true;
            var jsonList = JObject.Parse(json);
            var attendance = JsonConvert.DeserializeObject<AttendanceFormat>(jsonList.ToString());
            if (string.IsNullOrEmpty(attendance.Error) && attendance.UserId.Count > 0)
            {
                result = new Logic().AddAttendance(attendance);
            }
            return (result == true ? "1" : "0");
        }

        public ActionResult CreateManually()
        {
            var userProperties = new SelectUserProperties { UserId = Singleton.Instance.UserId, CompanyId = Singleton.Instance.CompanyId, BranchId = Singleton.Instance.BranchId, DepartmentId = Singleton.Instance.DepartmentId };

            var users = new UserLogic().GetAllUsers(EnumLibrary.UserType.Administrator, userProperties);
            TempData["users"] = users;
            return View(new AttendanceViewModel());
        }

        [HttpPost]
        public ActionResult CreateManually(AttendanceViewModel model)
        {
            try
            {
                if (model.Image != null)
                {
                    var pic = Path.GetFileName(model.Image.FileName);
                    var mappedPath = Server.MapPath("/Images/AttendanceAttachments");
                    if (!Directory.Exists(mappedPath))
                        Directory.CreateDirectory(mappedPath);
                    var path = Path.Combine(mappedPath, pic);
                    model.Image.SaveAs(path);
                    model.ImageUrl = "/Images/AttendanceAttachments/" + pic;
                }

                var attendance = new Logic().GetUser(model.UserId);
                model.MacAddress = attendance.MacAddress;
                model.BranchId = attendance.BranchId;
                model.MachineId = attendance.MachineId;
                model.ManualEntry = true;
                var result = new Logic().AddManualAttendance(model);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        public ActionResult Edit(Guid id)
        {
            return View(new Logic().GetAttendance(id));
        }

        [HttpPost]
        public ActionResult Edit(AttendanceViewModel model)
        {
            var result = new Logic().EditAttendance(model);
            return RedirectToAction("Index", new { id = result });
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

    }
}