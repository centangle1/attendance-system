﻿using CustomModels.Collection;
using EntityProvider;
using EnumLibrary;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<LeaveDetailViewModel> GetLeaveDetail(LeaveDetailSearchViewModel search)
        {
            return new DataAccess().GetLeaveDetail(search);
        }


        public Guid Update(LeaveDetailViewModel model, bool createEarnedLeaveDetailEntry = true)
        {
            var resultId = new Guid();
            if (model.LeaveTypeDetailList != null && model.LeaveTypeDetailList.Count <= 0)// When Only One Leave type is added it's sent directly in model
            {
                model.LeaveTypeDetailList = new List<LeaveTypeDetailViewModel>()
                {
                    new LeaveTypeDetailViewModel
                    {
                      Type=model.Type,
                      Count =model.Count,
                      Description=model.Description
                    }
                };
            }


            var anyTypeLeave = model.LeaveTypeDetailList.Where(m => m.Type == LeaveType.Any).FirstOrDefault();// unecessary type, therefore removing it from the list.
            if (anyTypeLeave != null)
                model.LeaveTypeDetailList.Remove(anyTypeLeave);


            foreach (var item in model.LeaveTypeDetailList)
            {
                var result = new DataAccess().Update(new LeaveDetailViewModel { Id = Guid.NewGuid(), EmployeeId = model.EmployeeId, Count = item.Count, Description = model.Description, Type = item.Type });
                if (createEarnedLeaveDetailEntry)// When Employee apply for leave we dont need to add this
                    Create(new EarnedLeaveDetail { EmployeeId = model.EmployeeId, LeaveType = item.Type, NumberOfLeaves = item.Count, Source = EarnedLeaveDetailSource.Manual });
                resultId = result;
            }

            var Action = model.AutomaticUpdate ? "Leave synchronized automatically" : "Leave synchronized manually";
            if (model.LeaveTypeDetailList.Count() > 0)
            {
                var variableType = model.GetType();

                Create(new LogSearchViewModel { Action = Action, Model = new JavaScriptSerializer().Serialize(model), Table = "LeaveDetails", AssemblyName = variableType.Assembly.Location, DataType = variableType.FullName, Namespace = variableType.Namespace });
            }
            return resultId;

        }

        public List<LeaveDetailReportViewModel> GetLeaveDetailsReport(LeaveDetailSearchViewModel search)
        {
            var resultList = new List<LeaveDetailReportViewModel>();
            search.SearchType = EnumLibrary.LeaveDetailSearchType.Report;
            var groupedDetails = new DataAccess().GetLeaveDetail(search).GroupBy(x => x.EmployeeId);
            foreach (var groupedDetail in groupedDetails)
            {
                var result = new LeaveDetailReportViewModel { Branch = groupedDetail.First().Branch, Department = groupedDetail.First().Department, Employee = groupedDetail.First().Employee };
                foreach (var detail in groupedDetail)
                {
                    if (detail.Type != 0)
                        result.LeaveTypeDetailList.Add(new LeaveTypeDetailViewModel { Type = detail.Type, Count = detail.Count, Description = detail.Description });
                }
                resultList.Add(result);

            }
            return resultList;
        }

        public Guid AutomaticUpdationOfEarnedLeave(LeaveDetailViewModel model)
        {
            return new Guid();
        }
    }
}
