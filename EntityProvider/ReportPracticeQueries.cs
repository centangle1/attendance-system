﻿using EnumLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomModels.ReportModels;
using CustomModels.Identity;
using CustomModels.Collection;


namespace EntityProvider
{
    public partial class DataAccess
    {
        public List<DailyReportModel> GetReport(ReportDates filters)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var users = db.Users.Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
                                          .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
                                          .Join(db.Companies, b => b.b.CompanyId, c => c.Id, (b, c) => new { b, c })
                                              .Where(m => m.b.d.u.Active && m.b.d.d.Active && m.b.b.Active && m.c.Active &&
                                              (m.c.Id == filters.CompanyId || filters.CompanyId == new Guid()) &&
                                              (m.b.b.Id == filters.BranchId || filters.BranchId == new Guid()) &&
                                              (m.b.d.d.Id == filters.DepartmentId || filters.DepartmentId == new Guid()) &&
                                              (m.b.d.u.Id == filters.User || filters.User == ""))
                                          .Select(m => new
                                          {
                                              Id = m.b.d.u.Id,
                                              Active = m.b.d.u.Active,
                                              DepartmentId = m.b.d.u.DepartmentId,
                                              MachineId = m.b.d.u.MachineId,
                                              Email = m.b.d.u.Email,
                                              UserName = m.b.d.u.UserName
                                          }).ToList()
                                          .Select(m => new ApplicationUser
                                          {
                                              Id = m.Id,
                                              Active = m.Active,
                                              DepartmentId = m.DepartmentId,
                                              MachineId = m.MachineId,
                                              Email = m.Email,
                                              UserName = m.UserName
                                          }).ToList();
                    var attendances = db.Attendances.Join(db.Branches, a => a.BranchId, b => b.Id, (a, b) => new { a, b })
                        .Join(db.Departments, b => b.b.Id, d => d.BranchCode, (b, d) => new { b, d })
                        .Join(db.Users, d => d.d.Id, u => u.DepartmentId, (d, u) => new { d, u })
                        .Join(db.Companies, b => b.d.b.b.CompanyId, c => c.Id, (b, c) => new { b, c }).
                        Where(m => m.c.Active && m.b.d.b.b.Active &&
                        m.b.d.b.a.EntryTime >= filters.StartTime &&
                        m.b.d.b.a.EntryTime <= filters.EndTime.Value &&
                        m.b.d.b.a.MachineId == m.b.u.MachineId)
                        .Select(m => new
                        {
                            Active = m.b.d.b.a.Active,
                            MacAddress = m.b.d.b.a.MacAddress,
                            BranchId = m.b.d.b.a.BranchId,
                            EntryTime = m.b.d.b.a.EntryTime,
                            Id = m.b.d.b.a.Id,
                            MachineId = m.b.d.b.a.MachineId,
                            Error = m.b.d.b.a.Error,
                            ManualEntry = m.b.d.b.a.ManualEntry
                        }).ToList()
                        .Select(m => new Attendance
                        {
                            Active = m.Active,
                            MacAddress = m.MacAddress,
                            BranchId = m.BranchId,
                            EntryTime = m.EntryTime,
                            Id = m.Id,
                            MachineId = m.MachineId,
                            Error = m.Error,
                            ManualEntry = m.ManualEntry
                        }).ToList();
                    var schedules = db.Schedules.Where(m=>m.Active).ToList();
                    var usersId = users.Select(m => m.Id).ToList();
                    var userSchedules = db.UserSchedules.Where(x => usersId.Contains(x.UserId)).Distinct().ToList();
                        //.AsEnumerable()
                        //.Select(m => new UserSchedule { Id=m.scheduleId, UserId=m.userId}).ToList();
                    //var s = db.UserSchedules.Where(us => us.UserId == (users.Where(u => u.Id == us.UserId).Select(m => m.Id).FirstOrDefault())).Select(m => m.Id).FirstOrDefault();

                    //var userSchedules = db.UserSchedules.Where(m => m.UserId.Any(b=>b.usersId));




                    return new List<DailyReportModel>();
                
                }
            }
            catch(Exception ex)
            {
                return new List<DailyReportModel>();
            }
        }
    }
}
