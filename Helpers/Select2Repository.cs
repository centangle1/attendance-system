﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CustomModels.ReportModels;

namespace Helpers
{
    public class Select2Repository
    {
        //IQueryable<Select2OptionModel> AllOptionsList;
        //public Select2Repository()
        //{
        //    AllOptionsList = GetSelect2Options();
        //}
        //IQueryable<Select2OptionModel> GetSelect2Options()
        //{
        //    string cacheKey = "Select2Options";

        //    //check cache
        //    if (HttpContext.Current.Cache[cacheKey] != null)
        //    {
        //        return (IQueryable<Select2OptionModel>)HttpContext.Current.Cache[cacheKey];
        //    }
        //    var optionList = new List<Select2OptionModel>();
        //    var optionText = "Option Number ";
        //    for (int i = 1; i < 1000; i++)
        //    {
        //        optionList.Add(new Select2OptionModel
        //        {
        //            id = i.ToString(),
        //            text = optionText + i
        //        });
        //    }

        //    var result = optionList.AsQueryable();

        //    //cache results
        //    HttpContext.Current.Cache[cacheKey] = result;

        //    return result;
        //}

        //List<Select2OptionModel> GetAllSearchResults(string searchTerm)
        //{
        //    var resultList = new List<Select2OptionModel>();
        //    if (!string.IsNullOrEmpty(searchTerm))
        //        resultList = AllOptionsList.Where(n => n.text.ToLower().Contains(searchTerm.ToLower())).ToList();
        //    else
        //        resultList = AllOptionsList.ToList();
        //    return resultList;
        //}

        public Select2PagedResult GetSelect2PagedResult(int pageSize, int pageNumber, List<Select2OptionModel> list)
        {
            var select2pagedResult = new Select2PagedResult();
            var totalResults = 0;
            select2pagedResult.Results = GetPagedListOptions(pageSize, pageNumber, list, out totalResults);
            select2pagedResult.Total = totalResults;
            return select2pagedResult;
        }

        List<Select2OptionModel> GetPagedListOptions(int pageSize, int pageNumber, List<Select2OptionModel> list, out int totalSearchRecords)
        {
            //var allSearchedResults = GetAllSearchResults(searchTerm);
            totalSearchRecords = list.Count;
            return list.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}

