﻿using System.Web;
using System.Web.Optimization;

namespace AttendanceSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.3.1.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryfullcalendar").Include(
                        //"~/Scripts/jquery-{version}.js",
                        "~/Scripts/moment.min.js",
                        "~/Scripts/fullcalendar.min.js"
                        ));
        
           bundles.Add(new ScriptBundle("~/bundles/tabler").Include(

                        //"~/Scripts/dashboard.js",
                        "~/Scripts/bootstrap.bundle.min.js",
                        "~/Content/Plugins/charts-c3/js/c3.min.js",
                        "~/Content/Plugins/charts-c3/js/d3.v3.min.js",
                        //"~/Content/Plugins/charts-c3/plugin.js",
                        //"~/Content/Plugins/input-mask/plugin.js",
                        "~/Content/Plugins/input-mask/js/jquery.mask.min.js",
                        "~/Scripts/select2.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/require").Include(
                        "~/Scripts/require.min.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/dataTable").Include(
                        "~/Scripts/DataTables/jquery.dataTables.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jq").Include(
                           "~/Scripts/jquery-ui-1.12.1.min.js",
                          "~/Scripts/jquery.schedule.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery-ui-1.12.1.min.js",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.validate.unobtrusive.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Scripts/mvcfoolproof.unobtrusive.min.js"
                        //"~/Scripts/select2.js"
                        
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/umd/popper.min.js",
                      "~/Scripts/umd/popper-utils.min.js",
                     // "~/Scripts/umd/popper.min.js.map",
                      "~/Scripts/respond.js"
                      ));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css",
                      "~/Content/fullcalendar.min.css",
                      "~/Content/fullcalendar.print.min.css",
                      "~/Content/select2/select2.css"));
            
            bundles.Add(new StyleBundle("~/Content/datatable").Include(
                      "~/Content/DataTables/css/jquery.dataTables.css"));

            bundles.Add(new StyleBundle("~/Content/tabler").Include(
                      "~/Content/font-awesome.css",
                      "~/Content/select2/select2.css",
                      "~/Content/google-fonts.css",
                      "~/Content/dashboard.css",
                      "~/Content/Plugins/charts-c3/plugin.css",
                      //"~/Content/bootstrap.css",
                      "~/Content/fullcalendar.min.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/jqCss").Include(
                   "~/Content/bootstrap.css",
                   "~/Content/Site.css",
                   "~/Content/themes/base/jquery-ui.min.css",
                   "~/Content/jquery.schedule.min.css",
                   "~/Content/jquery.schedule-demo.min.css"
                   ));

            BundleTable.EnableOptimizations = true;
        }

    }
}
