﻿using CustomModels.Collection;
using CustomModels.Identity;
using EntityProvider;
using EnumLibrary;
using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public static class Rules
    {
        public static string ProcessRulesForSync(string userId, LeaveType leaveType, EmployeeType employeeType, float comparedValue)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var rules = string.IsNullOrEmpty(userId) && employeeType != 0 ?
                        new DataAccess().GetLeaveRuleDetail<LeaveRulesDetailViewModel>(new LeaveRulesDetailViewModel { LeaveType = leaveType, EmployeeType = employeeType, RuleType = RuleType.RoleLeaveLimit }) :
                        new DataAccess().GetLeaveRuleDetail<LeaveRulesDetailViewModel>(new LeaveRulesDetailViewModel { LeaveType = leaveType, EmployeeType = GetUser(userId).Type, RuleType = RuleType.RoleLeaveLimit });
                    foreach (var rule in rules)
                    {
                        if (!RuleValueComparison(CastValue(rule.Value, rule.ValueType), comparedValue, rule.Operators))
                            return rule.ErrorMessage;
                    }
                    return "true";
                }
            }
            catch (Exception ex)
            {

                return "Rule cannot be processed.";
            }
        }

        public static string ProcessRulesForLeaveCreation(LeaveViewModel leave)
        {
            try
            {
                var rules = new Logic().GetLeaveRuleDetail<LeaveRulesDetailViewModel>(new LeaveRulesDetailViewModel { LeaveType = leave.Type, EmployeeType = new UserQueries().GetUser(leave.UserId).Type, RuleType = RuleType.LeaveLimit });
                var leaves = GetLeaves(leave.UserId, leave.SkipLeave);
                var user = GetUser(leave.UserId);
                var availableLeaveRecord = GetAvailableLeaves(leave.UserId, leave.Type);
                if (availableLeaveRecord.Id == new Guid())
                {
                    return "Add Leave Details for this Employee before applying for leave";
                }
                else
                {
                    var rule = new LeaveRulesDetailViewModel
                    {
                        Duration = LeaveDurationType.Annually,
                        Value = (availableLeaveRecord.Count + leave.EditedLeaveDayCount).ToString(),
                        ValueType = RulesValueType.Float,
                        Operators = Operators.LessThanOrEqualTo,
                        ExcludePreviousLeavesCount = true,
                        ErrorMessage = "Number of leaves applied is greater than leaves available for this employee.",
                    };
                    if (!CompareLeaveCount(leave, rule, leaves, user))
                        return rule.ErrorMessage;
                }
                leave.AvailableLeaves = availableLeaveRecord.Count;
                foreach (var rule in rules)
                {
                    if (!CompareLeaveCount(leave, rule, leaves, user))
                        return rule.ErrorMessage;
                }
                return "true";
            }
            catch (Exception ex)
            {
                return "Leave cannot be processed.";
            }
        }

        public static bool CompareLeaveCount(LeaveViewModel leave, LeaveRulesDetailViewModel rule, List<LeaveViewModel> leaves, ApplicationUser user)
        {
            var durationSet = SetDuration(new LeaveViewModel { StartingDate = leave.StartingDate, EndingDate = leave.EndingDate }, rule.Duration, user);
            var filteredLeaves = leaves.Where(m => m.StartingDate >= durationSet.StartingDate && m.EndingDate <= durationSet.EndingDate.AddDays(1).AddMilliseconds(-1) && m.Type == leave.Type).OrderByDescending(x => x.StartingDate).ToList();
            filteredLeaves.ForEach(m => m.DaysCount = SortLeaves(m, rule));
            var leavesHistory = new LeaveTypeDetailViewModel { Count = filteredLeaves.Sum(m => m.DaysCount), Type = leave.Type };
            var existingLeaves = leavesHistory.Count;
            leave.DaysCount = SortLeaves(leave, rule);
            //existing leaves are added when we have to count leaves count for the whole duration, if leave lies with in the duration and ExculdePreviousLeaveCount is false, it will further add the duration of the 
            //current leave, in case of ExcludePrevious bool to be true, it will only return the count of current leaves.
            //In case of out of bound it will send -1 or 0
            existingLeaves = leave.StartingDate >= durationSet.StartingDate && leave.StartingDate <= durationSet.EndingDate ? !rule.ExcludePreviousLeavesCount ? existingLeaves + leave.DaysCount : leave.DaysCount : leave.StartingDate < durationSet.StartingDate ? -1 : existingLeaves + 0;
            return CompareValue(rule, existingLeaves, leave, filteredLeaves);
        }

        public static int SortLeaves(LeaveViewModel model, LeaveRulesDetailViewModel rule)
        {
            var leaveDayCount = model.EndingDate.Date.Subtract(model.StartingDate.Date).Days + 1;
            return leaveDayCount >= 7 && !rule.IncludeHolidays ?
                                             leaveDayCount - ((leaveDayCount / 7) * 2) :
                                             (
                                             model.StartingDate.DayOfWeek > DayOfWeek.Sunday && model.EndingDate.DayOfWeek == DayOfWeek.Saturday ||
                                             model.StartingDate.DayOfWeek == DayOfWeek.Sunday && model.EndingDate.DayOfWeek < DayOfWeek.Saturday
                                             ) && !rule.IncludeHolidays ?
                                             leaveDayCount :
                                             model.StartingDate.DayOfWeek <= DayOfWeek.Saturday && model.EndingDate.DayOfWeek < model.StartingDate.DayOfWeek && !rule.IncludeHolidays ?
                                             leaveDayCount - 2 :
                                             leaveDayCount;
        }

        public static dynamic CastValue(string value, RulesValueType valueType)
        {
            switch (valueType)
            {
                case RulesValueType.Int: return int.Parse(value);
                case RulesValueType.Float: return float.Parse(value);
                case RulesValueType.Double: return double.Parse(value);
                case RulesValueType.DateTime: return DateTime.Parse(value);
                default: return value;
            }
        }

        public static bool CompareValue(LeaveRulesDetailViewModel rule, double comparedValue, LeaveViewModel leave, List<LeaveViewModel> filteredLeaves)
        {
            var value = CastValue(rule.Value, rule.ValueType);
            if (rule.IsConsecutive)
            {
                var consecutiveLeaves = GetConsecutiveLeaves(leave.Type, filteredLeaves);//Gets consecutive leaves.
                var nextConsecutiveLeaveGroup = consecutiveLeaves.Where(m => m.StartingDate >= leave.EndingDate).OrderBy(x => x.StartingDate).GroupBy(m => m.ConsecutiveIndex).FirstOrDefault();//Consecutive leave next to applied leave.
                var previousConsecutiveLeaveGroup = consecutiveLeaves.Where(m => m.EndingDate <= leave.StartingDate).OrderByDescending(x => x.StartingDate).GroupBy(m => m.ConsecutiveIndex).FirstOrDefault();//Consecutive leave prior to applied leave.
                var nextImmidiateConsecutiveLeave = nextConsecutiveLeaveGroup != null && nextConsecutiveLeaveGroup.FirstOrDefault() != null ? nextConsecutiveLeaveGroup.First() : new LeaveViewModel();
                var previousImmidiateConsecutiveLeave = previousConsecutiveLeaveGroup != null && previousConsecutiveLeaveGroup.First() != null ? previousConsecutiveLeaveGroup.First() : new LeaveViewModel();
                var nextConsecutiveLeaveDifference = nextImmidiateConsecutiveLeave.Id != new Guid() ? nextImmidiateConsecutiveLeave.StartingDate.Subtract(leave.EndingDate).Days : -1;
                var previousConsecutiveLeaveDifference = previousImmidiateConsecutiveLeave.Id != new Guid() ? leave.StartingDate.Subtract(previousImmidiateConsecutiveLeave.StartingDate).Days : -1;
                var consecutiveLeavesDayCount = nextConsecutiveLeaveDifference == 0 ? leave.DaysCount + nextConsecutiveLeaveGroup.Sum(x => x.DaysCount) : previousConsecutiveLeaveDifference == 0 ? leave.DaysCount + previousConsecutiveLeaveGroup.Sum(x => x.DaysCount) : leave.DaysCount;

                return RuleValueComparison(value, consecutiveLeavesDayCount, rule.Operators);
            }
            if (rule.IsCarriedOn)
            {
                return RuleValueComparison(leave.AvailableLeaves, comparedValue, rule.Operators);
            }
            if (rule.ValueType == RulesValueType.DateTime)
            {
                return RuleValueComparison(value, leave.StartingDate, rule.Operators);
            }
            return RuleValueComparison(value, comparedValue, rule.Operators);
        }

        public static bool RuleValueComparison(dynamic value, dynamic comparativeValue, Operators operators)
        {
            switch (operators)
            {
                case Operators.EqualsTo: return comparativeValue == value;
                case Operators.GreaterThan: return comparativeValue > value;
                case Operators.GreaterThanOrEqualTo: return comparativeValue >= value;
                case Operators.LessThan: return comparativeValue < value;
                case Operators.LessThanOrEqualTo: return comparativeValue <= value;
                case Operators.NotEqualTo: return comparativeValue < value || comparativeValue > value;
                default: return false;
            }
        }

        public static List<LeaveViewModel> GetConsecutiveLeaves(LeaveType type, List<LeaveViewModel> filteredLeaves)
        {
            var leaves = filteredLeaves.Where(m => m.Type == type).OrderByDescending(m => m.StartingDate).ToList(); //Gets all leaves of the type.
            var consecutiveLeavesList = new List<LeaveViewModel>();
            var leaveCount = leaves.Count();
            var check = 0;

            for (var i = 0; i < leaveCount; i++)
            {
                var IsConsecutiveWithInCurrentLeave = leaves[i].DaysCount > 0;// Check consecutive leave within one applied leave.

                if (i + 1 < leaveCount)
                {
                    var dateDifferenceBetweenLeaves = leaves[i].StartingDate.Date.Subtract(leaves[i + 1].EndingDate.Date).Days;
                    var currentLeaveStartingDayOfWeek = (int)leaves[i].StartingDate.DayOfWeek;
                    var nextLeaveEndingDayOfWeek = (int)leaves[i + 1].EndingDate.DayOfWeek;
                    var IsConsecutiveInBetweenTwoLeaves = dateDifferenceBetweenLeaves == 1 // Check for consecutive leaves
                        || (dateDifferenceBetweenLeaves == 3 && currentLeaveStartingDayOfWeek == 1 && nextLeaveEndingDayOfWeek == 5); //Check if leaves are consecutive over the weekend
                    if (IsConsecutiveInBetweenTwoLeaves || IsConsecutiveWithInCurrentLeave)
                    {
                        if (!consecutiveLeavesList.Contains(leaves[i]))
                        {
                            check++;
                            leaves[i].ConsecutiveIndex = check;
                            consecutiveLeavesList.Add(leaves[i]);

                        }
                        if (IsConsecutiveInBetweenTwoLeaves)
                        {
                            leaves[i + 1].ConsecutiveIndex = check;
                            consecutiveLeavesList.Add(leaves[i + 1]);
                        }
                    }
                }
                else
                {
                    if (IsConsecutiveWithInCurrentLeave)
                    {
                        check++;
                        leaves[i].ConsecutiveIndex = check;
                        consecutiveLeavesList.Add(leaves[i]);
                    }
                }
            }
            return consecutiveLeavesList;
        }

        public static LeaveViewModel SetDuration(LeaveViewModel model, LeaveDurationType duration, ApplicationUser user)
        {
            switch (duration)
            {
                case LeaveDurationType.Weekly:
                    model.StartingDate = model.StartingDate.AddDays(-(int)DateTime.Now.DayOfWeek);
                    model.EndingDate = model.StartingDate.AddDays(7 - (int)DateTime.Now.DayOfWeek).AddDays(1).AddMilliseconds(-1); break;

                case LeaveDurationType.BiWeekly:
                    model.StartingDate = model.StartingDate.AddDays(-(int)DateTime.Now.DayOfWeek - 7);
                    model.EndingDate = model.StartingDate.AddDays(7 - (int)DateTime.Now.DayOfWeek).AddDays(1).AddMilliseconds(-1); break;

                case LeaveDurationType.Monthly:
                    model.StartingDate = new DateTime(DateTime.Now.Year, model.StartingDate.Month, 1);
                    model.EndingDate = new DateTime(model.StartingDate.Year, model.StartingDate.Month, DateTime.DaysInMonth(model.StartingDate.Year, model.StartingDate.Month)).AddDays(1).AddMilliseconds(-1); break;

                case LeaveDurationType.BiMonthly:
                    model.StartingDate = new DateTime(model.StartingDate.Year, model.StartingDate.AddMonths(-1).Month, 1);
                    model.EndingDate = new DateTime(model.StartingDate.Year, model.StartingDate.Month, DateTime.DaysInMonth(model.StartingDate.Year, model.StartingDate.Month)).AddDays(1).AddMilliseconds(-1); break;

                case LeaveDurationType.Quarterly:
                    var currentQuarter = (model.StartingDate.Month / 3) + 1;
                    var startingMonth = currentQuarter == 1 ? currentQuarter : 3 * (currentQuarter - 1) + 1;
                    model.StartingDate = new DateTime(model.StartingDate.Year, startingMonth, 1);
                    model.EndingDate = new DateTime(model.StartingDate.Year, 3 * currentQuarter, DateTime.DaysInMonth(model.StartingDate.Year, model.StartingDate.Month)); break;

                case LeaveDurationType.SemiAnnually:
                    var currentPeriod = model.StartingDate.Month % 6 == 0 ? model.StartingDate.Month / 6 : (model.StartingDate.Month / 6) + 1;
                    startingMonth = currentPeriod == 1 ? currentPeriod : 6 * (currentPeriod - 1) + 1;
                    model.StartingDate = new DateTime(model.StartingDate.Year, startingMonth, 1);
                    model.EndingDate = new DateTime(model.StartingDate.Year, 3 * currentPeriod, DateTime.DaysInMonth(model.StartingDate.Year, model.StartingDate.Month)).AddDays(1).AddMilliseconds(-1); break;

                case LeaveDurationType.Annually:
                    model.StartingDate = new DateTime(model.StartingDate.Year, 1, 1);
                    model.EndingDate = new DateTime(model.StartingDate.Year, 12, 31).AddDays(1).AddMilliseconds(-1);
                    break;

                case LeaveDurationType.FirstWeek:
                    model.StartingDate = user.AddedOn;
                    model.EndingDate = user.AddedOn.AddDays(8).AddMilliseconds(-1);
                    break;

                case LeaveDurationType.FirstMonth:
                    model.StartingDate = user.AddedOn;
                    model.EndingDate = user.AddedOn.AddDays(8).AddMilliseconds(-1);
                    break;

                case LeaveDurationType.FirstQuarter:
                    model.StartingDate = user.AddedOn;
                    model.EndingDate = user.AddedOn.AddMonths(3).AddDays(1).AddMilliseconds(-1);
                    break;

                case LeaveDurationType.FirstYear:
                    model.StartingDate = user.AddedOn;
                    model.EndingDate = user.AddedOn.AddYears(1).AddDays(1).AddMilliseconds(-1);
                    break;



            }
            return model;
        }

        public static List<LeaveViewModel> GetLeaves(string userId, Guid skipLeave)
        {
            try
            {
                //using (ApplicationDbContext db = new ApplicationDbContext())
                //{
                //    var startingDate = new DateTime(DateTime.Now.Year, 1, 1);
                //    var endingDate = new DateTime(DateTime.Now.Year, 12, 31);
                //    var leaves = db.Leaves.Where(m => m.StartingDate >= startingDate && m.EndingDate <= endingDate && m.UserId == userId && m.Active)
                //                            .Select(m => new
                //                            {
                //                                Status = m.Status,
                //                                Active = m.Active,
                //                                StartingDate = m.StartingDate,
                //                                EndingDate = m.EndingDate,
                //                                Id = m.Id,
                //                                Type = m.Type,
                //                                UserId = m.UserId,
                //                                User = m.User
                //                            }).ToList()
                //                            .Select(m => new LeaveViewModel
                //                            {
                //                                Status = m.Status,
                //                                Active = m.Active,
                //                                EndingDate = m.EndingDate,
                //                                DEndingDate = m.EndingDate,
                //                                StartingDate = m.StartingDate,
                //                                DStartingDate = m.StartingDate,
                //                                Id = m.Id,
                //                                Type = m.Type,
                //                                UserId = m.UserId,
                //                                User = m.User
                //                            }).ToList();

                //    return leaves;
                //}

                var startingDate = new DateTime(DateTime.Now.Year, 1, 1);
                var endingDate = new DateTime(DateTime.Now.Year, 12, 31);
                return new DataAccess().GetLeaves<LeaveViewModel>(new LeaveViewModel { StartingDate = startingDate, EndingDate = endingDate, Active = true, SkipLeave = skipLeave, UserId = userId }).ToList();
            }
            catch (Exception ex)
            {
                return new List<LeaveViewModel>();
            }
        }

        public static ApplicationUser GetUser(string userId)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return new Helpers.Helpers().GetUser(userId);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static LeaveDetail GetAvailableLeaves(string employeeId, LeaveType type)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.LeaveDetails.Where(m => m.EmployeeId == employeeId && m.Type == type).First();
                }
            }
            catch (Exception ex)
            {

                return new LeaveDetail();
            }
        }


    }
}
