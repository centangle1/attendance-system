﻿using BusinessLogic;
using CustomModels.Collection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttendanceSystem.Controllers
{
    public class NationalHolidaysController : Controller
    {
        // GET: NationalHolidays

        public ActionResult Calendar()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CreateJson()
        {
            NationalHoliday nationalHoliday = new NationalHoliday
            {
                Title = Request["title"],
                Description = Request["description"],
                StartDate = Convert.ToDateTime(Request["start"]),
                EndDate = Convert.ToDateTime(Request["end"])
            };

            var result = new Logic().CreateNationalHoliday(nationalHoliday);
            return result != new Guid() ? Json(new { result }) : Json(new { success = false });
        }

        public JsonResult GetJsonResult()
        {
            var result = new Logic().GetAllHolidays();
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditJson()
        {
            NationalHoliday model = new NationalHoliday();
            model.Title = Request["title"];
            model.Description = Request["description"];
            model.StartDate = Convert.ToDateTime(Request["start"]);
            model.EndDate = Convert.ToDateTime(Request["end"]);
            if (Request["permanent"] == "true") { model.Permanent = true; }
            else { model.Permanent = false; }
            model.Id = new Guid(Request["id"]);
            Logic lg = new Logic();
            var result = lg.EditNationalHoliday(model);
            if (result != new Guid())
            {
                return RedirectToAction("Calendar");
            }
            return View();
        }

        [HttpPost]
        public ActionResult DeleteJson()
        {
            NationalHoliday model = new NationalHoliday
            {
                Id = new Guid(Request["id"])
            };
            var result = new Logic().DeleteNationalHoliday(model);
            return result != new Guid() ? View("Calendar") : View();
        }

        public ActionResult Details(Guid id)
        {
            var result = new Logic().GetNationalHoliday(id);
            return View(result);
        }

        public ActionResult Practice()
        {
            return View();
        }
    }
}