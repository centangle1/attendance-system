﻿using CustomModels.Collection;
using CustomModels.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EntityProvider
{
    public partial class DataAccess
    {
        /// <summary>
        /// Gets the filtered branch on the basis of 
        /// Company Id,
        /// Branch Id,
        /// User Id,
        /// if User is an Administrator or Company Manager it will send all the records
        /// </summary>
        /// <param name="search"></param>
        /// <returns>filtered list of Branches</returns>
        public List<Branch> GetBranches(BranchSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    bool IsAdministrator = HttpContext.Current.User.IsInRole("Administrator");
                    bool IsCompanyManager = HttpContext.Current.User.IsInRole("Company Manager");
                    string currentUserId = HttpContext.Current.User.Identity.GetUserId();

                    var result = db.Branches.GroupJoin(db.Departments, b => b.Id, d => d.BranchCode, (b, d) => new { b, d = d.FirstOrDefault() })
                                            .GroupJoin(db.Users, d => d.d.Id, u => u.DepartmentId, (d, u) => new { d = d.d, b = d.b, u = u.FirstOrDefault() })
                                            .Where(m =>
                                                (search.IncludeRoleCondition && (IsAdministrator || IsCompanyManager || m.u.Id == currentUserId))
                                                ||
                                                (m.b.Active &&
                                                (string.IsNullOrEmpty(search.UserId) || m.u.Id == search.UserId) &&
                                                (search.CompanyId == new Guid() || m.b.CompanyId == search.CompanyId) &&
                                                (search.Id == new Guid() || m.b.Id == search.Id))
                                            )
                                            .Select(m => new
                                            {
                                                Id = m.b.Id,
                                                Name = m.b.Name,
                                                Address = m.b.Address,
                                                Active = m.b.Active
                                            }).ToList()
                                        .Select(m => new Branch
                                        {
                                            Id = m.Id,
                                            Name = m.Name,
                                            Address = m.Address,
                                            Active = m.Active
                                        }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<Branch>();
            }
        }

        public Guid AddBranch(Branch model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    model.Active = true;
                    model.Id = Guid.NewGuid();
                    model.Address.Id = Guid.NewGuid();
                    var result = db.Branches.Add(model);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (DbEntityValidationException ex)
            {
                return new Guid();
            }
            catch (Exception ex)
            {
                return new Guid();
            }

        }

        public Guid EditBranch(Branch model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var data = db.Branches.Find(model.Id);
                    data.Name = string.IsNullOrEmpty(model.Name) ? data.Name : model.Name;
                    db.Entry(data).State = EntityState.Modified;
                    var address = db.Addresses.Find(model.AddressId);
                    address.Area = string.IsNullOrEmpty(model.Address.Area) ? address.Area : model.Address.Area;
                    address.City = string.IsNullOrEmpty(model.Address.City) ? address.City : model.Address.City;
                    address.House = model.Address.House;
                    address.Street = model.Address.Street;
                    db.Entry(address).State = EntityState.Modified;
                    db.SaveChanges();

                    return data.CompanyId;


                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid DeleteBranch(Branch model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var branch = db.Branches.Find(model.Id);
                    var branchAddress = db.Addresses.Find(branch.AddressId);
                    branchAddress.Active = false;

                    branch.Active = false;
                    db.Entry(branchAddress).State = EntityState.Modified;
                    db.Entry(branch).State = EntityState.Modified;
                    db.SaveChanges();
                    return Guid.NewGuid();
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }
    }
}
