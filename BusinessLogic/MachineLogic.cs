﻿using CustomModels.Collection;
using EntityProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public Machine GetMachine(string macAddress)
        {
            return new DataAccess().GetMachineInfo(macAddress).FirstOrDefault();
        }
        public Guid AddMachine(Machine model)
        {
            return new DataAccess().Add(model);
        }
        
        public Guid Edit(Machine model)
        {
            return new DataAccess().Edit(model);
        }
        
        public Guid Delete(Guid id)
        {
            return new DataAccess().Delete(id);
        }

        public List<Machine> ViewAllMachines()
        {
            return new DataAccess().GetMachineInfo("");
        }
    }
}
