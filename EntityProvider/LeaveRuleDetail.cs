﻿using AutoMapper;
using CustomModels.Collection;
using CustomModels.Identity;
using CustomModels.ReportModels;
using EnumLibrary;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public List<T> GetLeaveRuleDetail<T>(LeaveRulesDetailViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.IsSelect2)
                        return db.LeaveRulesDetails.Where(m => m.IsDeleted == false &&
                                                               (m.Id == search.Id || search.Id == new Guid()) &&
                                                               (search.LeaveType == 0 || m.LeaveType == search.LeaveType || m.LeaveType == LeaveType.Any) &&
                                                               (search.Duration == 0 || m.Duration == search.Duration) &&
                                                               (search.Operators == 0 || m.Operators == search.Operators) &&
                                                               (string.IsNullOrEmpty(search.Name) || m.Name.Contains(search.Name)))
                                                               .Select(m => new Select2OptionModel { id = m.Id.ToString(), text = m.Name }).ToList() as List<T>;
                    if (search.EmployeeType != 0)
                        return db.LeaveRulesDetails.GroupJoin(db.LeaveRules, lrd => lrd.Id, lr => lr.RuleId, (lrd, lr) => new { d = lrd, rs = lr })
                                                               .SelectMany(x => x.rs.DefaultIfEmpty(),
                                                               (x, y) => new { d = x.d, r = y }) // rs is multiple records of rules, which are further filtered in select many, where d = above d & r = each record present in rs.
                                                               .Where(m => m.d.IsDeleted == false && m.r.IsDeleted == false && 
                                                               (m.d.Id == search.Id || search.Id == new Guid()) &&
                                                               (search.LeaveType == 0 || m.d.LeaveType == search.LeaveType || m.d.LeaveType == LeaveType.Any) &&
                                                               (search.Duration == 0 || m.d.Duration == search.Duration) &&
                                                               (search.EmployeeType == 0 || m.r.EmployeeType == search.EmployeeType) &&
                                                               (search.Operators == 0 || m.d.Operators == search.Operators) &&
                                                               (search.RuleType == 0 || m.d.RuleType == search.RuleType) &&
                                                               (string.IsNullOrEmpty(search.Name) || m.d.Name.Contains(search.Name)))
                                                               .Select(m => new LeaveRulesDetailViewModel
                                                               {
                                                                   Id = m.d.Id,
                                                                   Description = m.d.Description,
                                                                   Name = m.d.Name,
                                                                   Value = m.d.Value,
                                                                   ValueType = m.d.ValueType,
                                                                   Duration = m.d.Duration,
                                                                   IncludeHolidays = m.d.IncludeHolidays,
                                                                   IsConsecutive = m.d.IsConsecutive,
                                                                   Operators = m.d.Operators,
                                                                   RuleType = m.d.RuleType,
                                                                   ErrorMessage = m.d.ErrorMessage,
                                                                   LeaveType = m.d.LeaveType,
                                                                   IsCarriedOn = m.d.IsCarriedOn
                                                               }).ToList() as List<T>;

                    return db.LeaveRulesDetails.GroupJoin(db.LeaveRules, lrd => lrd.Id, lr => lr.RuleId, (lrd, lr) => new { d = lrd, rs = lr })
                                        .SelectMany(x => x.rs.DefaultIfEmpty(),
                                        (x, y) => new { d = x.d, r = y }) // rs is multiple records of rules, which are further filtered in select many, where d = above d & r = each record present in rs.
                                        .Where(m => m.d.IsDeleted == false && m.r.IsDeleted == false &&
                                        (m.d.Id == search.Id || search.Id == new Guid()) &&
                                        (search.LeaveType == 0 || m.d.LeaveType == search.LeaveType || m.d.LeaveType == LeaveType.Any) &&
                                        (search.Duration == 0 || m.d.Duration == search.Duration) &&
                                        (search.EmployeeType == 0 || m.r.EmployeeType == search.EmployeeType) &&
                                        (search.Operators == 0 || m.d.Operators == search.Operators) &&
                                        (search.RuleType == 0 || m.d.RuleType == search.RuleType) &&
                                        (string.IsNullOrEmpty(search.Name) || m.d.Name.Contains(search.Name)))
                                        .Select(m => new LeaveRulesDetailViewModel
                                        {
                                            Id = m.d.Id,
                                            Description = m.d.Description,
                                            Name = m.d.Name,
                                            Value = m.d.Value,
                                            ValueType = m.d.ValueType,
                                            Duration = m.d.Duration,
                                            IncludeHolidays = m.d.IncludeHolidays,
                                            IsConsecutive = m.d.IsConsecutive,
                                            Operators = m.d.Operators,
                                            RuleType = m.d.RuleType,
                                            ErrorMessage = m.d.ErrorMessage,
                                            EmployeeType = m.r.EmployeeType,
                                            LeaveType = m.d.LeaveType,
                                            IsCarriedOn = m.d.IsCarriedOn
                                        }).ToList() as List<T>;
                }
            }
            catch (Exception ex)
            {

                return new List<T>();
            }
        }

        public Guid Create(LeaveRulesDetailViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<LeaveRulesDetailViewModel, LeaveRulesDetail>(); });
                    var iMapper = config.CreateMapper();
                    var leaveRule = iMapper.Map<LeaveRulesDetailViewModel, LeaveRulesDetail>(model);
                    var result = db.LeaveRulesDetails.Add(leaveRule);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                return new Guid();
            }
        }

        public Guid Edit(LeaveRulesDetailViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.LeaveRulesDetails.Find(model.Id);
                    record.Value = string.IsNullOrEmpty(model.Value) ? record.Value : model.Value;
                    record.Description = string.IsNullOrEmpty(model.Description) ? record.Description : model.Description;
                    record.ErrorMessage = string.IsNullOrEmpty(model.ErrorMessage) ? record.ErrorMessage : model.ErrorMessage;
                    record.Duration = model.Duration == 0 ? record.Duration : model.Duration;
                    record.ValueType = model.ValueType == 0 ? record.ValueType : model.ValueType;
                    record.RuleType = model.RuleType == 0 ? record.RuleType : model.RuleType;
                    record.Name = string.IsNullOrEmpty(model.Name) ? record.Name : model.Name;
                    record.LeaveType = model.LeaveType == 0 ? record.LeaveType : model.LeaveType;
                    record.IncludeHolidays = model.IncludeHolidays;
                    record.IsConsecutive = model.IsConsecutive;
                    record.IsCarriedOn = model.IsCarriedOn;
                    record.IsDeleted = model.IsDeleted;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {

                return new Guid();
            }
        }

    }
}
