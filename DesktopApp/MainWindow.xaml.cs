﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AttendanceSystemWpf.UserControls;


namespace AttendanceSystemWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Adress_Click(object sender, RoutedEventArgs e)
        {
            //branch usr = new branch();
            //usr.Owner = App.Current.MainWindow;
            //usr.Show();
        }
        //private void Branch_Click(object sender, RoutedEventArgs e)
        //{
        //    branch usr = new branch();
        //    usr.Owner = App.Current.MainWindow;
        //    usr.Show();
        //}

        private void Adresss_Click(object sender, RoutedEventArgs e)
        {
            Adress adress = new Adress();
            adress.Owner = App.Current.MainWindow;
            adress.Show();
        }

        private void Department_Click(object sender, RoutedEventArgs e)
        {
            Department department = new Department();
            department.Owner = App.Current.MainWindow;
            department.Show();
        }

        private void Leave_Click(object sender, RoutedEventArgs e)
        {
            Leave leave = new Leave();
            leave.Owner = App.Current.MainWindow;
            leave.Show();
        }

        private void Attendance_Click(object sender, RoutedEventArgs e)
        {
            Attendance attendance = new Attendance();
            attendance.Owner = App.Current.MainWindow;
            attendance.Show();
        }

        private void user_Click(object sender, RoutedEventArgs e)
        {
            User user = new User();
            user.Owner = App.Current.MainWindow;
            user.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Test_Click(object sender, RoutedEventArgs e)
        {
            test tstt = new test();
            tstt.Owner = App.Current.MainWindow;
            tstt.Show();
        }

        private void Createbtn_Click(object sender, RoutedEventArgs e)
        {
            //IndexCompany ic = new IndexCompany();
            //ic.Owner = App.Current.MainWindow;
            //ic.Show();
            var ci = new UserControls.Company.Index();
            dispalyarea.Children.Add(ci);
        }
    }
}
