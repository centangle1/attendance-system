﻿
//
//function for displaying error messages
function DisplayErrorMessage(id, error) {

    var errorSpan = $("span[data-valmsg-for = '" + id + "']");
    errorSpan.removeClass("field-validation-valid");
    errorSpan.addClass("field-validation-error");
    errorSpan.html('<span id="Image-error">' + error + '.</span>');
}

//
//function for removing error messages
function RemoveErrorMessage(id) {

    var errorSpan = $("span[data-valmsg-for = '" + id + "']");
    errorSpan.addClass("field-validation-valid");
    errorSpan.removeClass("field-validation-error");
    errorSpan.html('');
}