﻿using AutoMapper;
using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public List<LeaveDetailViewModel> GetLeaveDetail(LeaveDetailSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.SearchType == EnumLibrary.LeaveDetailSearchType.EmployeeId)
                    {

                        return db.LeaveDetails.Where(m => m.EmployeeId == search.EmployeeId && m.IsDeleted == false)
                            .Select(m => new LeaveDetailViewModel
                            {
                                EmployeeId = m.EmployeeId,
                                Count = m.Count,
                                Description = m.Description,
                                Id = m.Id,
                                Type = m.Type
                            }).ToList();
                    }
                    if (search.SearchType == EnumLibrary.LeaveDetailSearchType.Report)
                    {
                        return db.Users.Join(db.LeaveDetails, u => u.Id, ld => ld.EmployeeId, (u, ld) => new { u, ld })
                            .Join(db.Departments, u => u.u.DepartmentId, d => d.Id, (u, d) => new { u = u.u, d = d, ld = u.ld })
                            .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { u = d.u, d = d.d, ld = d.ld, b = b })
                            .Where(m => m.u.Active)
                            .Select(m => new LeaveDetailViewModel
                            {

                                Employee = m.u,
                                Branch = m.b.Name,
                                Department = m.d.Name,
                                Type = m.ld != null ? m.ld.Type : 0,
                                Count = m.ld != null ? m.ld.Count : 0,
                                BranchId = m.b.Id,
                                DepartmentId = m.d.Id,
                                EmployeeId = m.u.Id,
                                Description = m.ld != null ? "" : m.ld.Description,
                                Id = m.ld != null ? m.ld.Id : new Guid()
                            }).Distinct().ToList();
                    }

                    var result = db.LeaveDetails.Find(search.EmployeeId, search.Type);
                    return new List<LeaveDetailViewModel> { new LeaveDetailViewModel { Id = result.Id, Count = result.Count, EmployeeId = result.EmployeeId, Type = result.Type, Description = result.Description } };

                }
            }
            catch (Exception ex)
            {
                return new List<LeaveDetailViewModel>();
            }
        }

        public Guid Update(LeaveDetailViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    // Remove Previous Record if any 
                    var record = db.LeaveDetails.Find(model.EmployeeId, model.Type);
                    if (record != null)
                    {
                        var currentLeaves = record == null ? 0 : record.Count;
                        if (model.AutomaticUpdate && model.IsCarriedOn)
                            model.Count = model.Count < 0 ? currentLeaves : model.Count + currentLeaves;
                        else
                            model.Count = model.Count < 0 ? currentLeaves : model.Count;
                        db.LeaveDetails.Remove(record);
                    }
                    //Create New Record
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<LeaveDetailViewModel, LeaveDetail>(); });
                    var iMapper = config.CreateMapper();
                    var leaveDetail = iMapper.Map<LeaveDetailViewModel, LeaveDetail>(model);
                    var result = db.LeaveDetails.Add(leaveDetail);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                return new Guid();
            }
        }

        //public Guid Edit(LeaveDetailViewModel model)
        //{
        //    try
        //    {
        //        using (ApplicationDbContext db = new ApplicationDbContext())
        //        {
        //            var record = db.LeaveDetails.Find(model.EmployeeId, model.Type);
        //            if (model.AutomaticUpdate && model.IsCarriedOn)
        //                record.Count = model.Count < 0 ? record.Count : model.Count + record.Count;
        //            else
        //                record.Count = model.Count < 0 ? record.Count : model.Count;
        //            record.Description = string.IsNullOrEmpty(model.Description) ? record.Description : model.Description;
        //            record.IsDeleted = model.IsDeleted;
        //            db.Entry(record).State = EntityState.Modified;
        //            db.SaveChanges();
        //            return record.Id;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        return new Guid();
        //    }
        //}
    }
}
