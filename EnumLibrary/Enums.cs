﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumLibrary
{
    public enum LeaveType
    {
        Any = 1,
        Casual = 2,
        Sick = 3,
        [Display(Name = "Fully Paid")]
        FullyPaid = 4,
        Partial = 5
    }

    public enum LogSearchType
    {
        Exception = 1,
        General = 2,
        LeaveSync = 3
    }

    public enum LeaveDetailSearchType
    {
        Report = 1,
        General = 2, 
        EmployeeId = 3
    }

    public enum EarnedLeaveDetailSource
    {
        Manual = 1,
        Automatic = 2,
        Service = 3

    }

    public enum RulesValueType
    {
        Int = 1,
        Float = 2,
        String = 3,
        Double = 4,
        DateTime = 5,
    }

    public enum RuleType
    {
        LeaveLimit = 1,
        EarnedLeaves = 2,
        RoleLeaveLimit = 3
    }

    public enum LeaveDurationType
    {
        Weekly = 1,
        BiWeekly = 2,
        Monthly = 3, 
        BiMonthly = 4,
        Quarterly = 5,
        SemiAnnually = 6,
        Annually = 7,
        FirstWeek = 8,
        FirstMonth = 9,
        FirstQuarter = 10,
        FirstYear = 11
    }

    public enum Operators
    {
        [Display(Name = "==")]
        EqualsTo = 1,
        [Display(Name = "!=")]
        NotEqualTo = 2,
        [Display(Name = "<")]
        LessThan = 3,
        [Display(Name = ">")]
        GreaterThan = 4,
        [Display(Name = "<=")]
        LessThanOrEqualTo = 5,
        [Display(Name = ">=")]
        GreaterThanOrEqualTo = 6
    }

    public enum WeekDays
    {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday
    }

    public enum LateEarlyPolicy
    {
        OnTime = 1,
        Late = 2,
        Early = 3
    }

    public enum AttendanceStatus
    {
        Absent,
        Present,
        OnLeave,
        NationalHoliday,
        Holiday,

    }

    public enum ReportStatusSelection
    {
        Absent = 1,
        Present = 2,
        OnLeave = 3,
        Late = 4,
        [Display(Name = "Consecutive Late Commers")]
        ConsecutiveLateCommers = 5
    }

    public enum AttendanceDayStatus
    {
        Regular,
        OnLeave,
        NationalHoliday,
        Holiday
    }
    public enum ReportSelection
    {
        Daily,
        Weekly,
        Monthly,
        [Display(Name = "Monthly Consolidated")]
        MonthlyConsolidated
    }

    public enum UserType
    {
        Administrator = 1,
        CompanyManager = 2,
        BranchManager = 3,
        DepartmentManager = 4,
        Member = 5
    }

    public enum EmployeeType
    {
        OnContract = 1,
        Permenant = 2,
        Internee = 3
    }

    public enum ModesOfCommunication
    {
        Email = 1,
        SMS = 2,
        Call = 3, 
        Report = 4
    }
}
