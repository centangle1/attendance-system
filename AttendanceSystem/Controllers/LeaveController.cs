﻿using AutoMapper;
using BusinessLogic;
using CustomModels.Collection;
using CustomModels.ReportModels;
using EnumLibrary;
using Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace AttendanceSystem.Controllers
{
    public class LeaveController : Controller
    {
        // GET: Leave
        public ActionResult Index()
        {
            var result = new Logic().GetLeaves<LeaveViewModel>(new LeaveViewModel());
            return View(result);
        }

        public ActionResult ViewLeave(string id)
        {
            TempData["id"] = id;
            var details = new Helpers.Helpers().GetUserProperties(id);
            ViewBag.departmentId = details.DepartmentId;
            
            var result = new Logic().GetLeaves<Leave>(new LeaveViewModel { UserId = id });
            if (result != new List<Leave>())
            {
                return View(result);
            }
            return View();
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            var check = new LeaveViewModel();
            return View(check);
        }

        [HttpPost]
        public ActionResult Create(LeaveViewModel model)
        {
            // Function to process rules for current leave
            var processedResult = new Logic().ProcessRulesForLeaveCreation(model);
            if(processedResult == "true")
            {
                if (model.Image != null)
                {
                    var pic = Path.GetFileName(model.Image.FileName);
                    var mappedPath = Server.MapPath("/Images/LeaveAttachments");
                    if (!Directory.Exists(mappedPath))
                        Directory.CreateDirectory(mappedPath);
                    var path = Path.Combine(mappedPath, pic);
                    model.Image.SaveAs(path);
                    model.ImageUrl = "/Images/LeaveAttachments/" + pic;
                }
                var check = new Logic().CreateLeave(model);
                if (check != new Guid())
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    SetSelect2List(model);
                    return View(model);
                }
            }
            else
            {
                SetSelect2List(model);
                ModelState.AddModelError("StartingDate", processedResult);
                return View(model);
            }
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(Guid id)
        {
            var result = new Logic().GetLeaves<LeaveViewModel>(new LeaveViewModel { Id = id }).FirstOrDefault();
            result.EditedLeaveDayCount = BusinessLogic.Rules.SortLeaves(result, new LeaveRulesDetailViewModel { IncludeHolidays = result.Type == LeaveType.Casual ? true : false });
            return View(result);
        }

        [HttpPost]
        public ActionResult Edit(LeaveViewModel model)
        {
            model.SkipLeave = model.Id;
            var processedResult = new Logic().ProcessRulesForLeaveCreation(model);
            if (processedResult == "true")
            {
                //model.EndingDate = model.EndingDate.AddSeconds(-1);
                var result = new Logic().EditLeave(model);
                if (result != new Guid())
                {
                    return RedirectToAction("Index");
                }
                return View(model);
            }

            else
            {
                SetSelect2List(model);
                ModelState.AddModelError("StartingDate", processedResult);
                return View(model);
            }

        }

        [Authorize(Roles = "Administrator")]
        public ActionResult SyncLeaves()
        {
            var model = InitializeDetailList(new LeaveDetailViewModel { LeaveTypeDetailList = new List<LeaveTypeDetailViewModel>() });
            return View(model);
        }

        [HttpPost]
        public ActionResult SyncLeaves(LeaveDetailViewModel model)
        {
            var result = new Logic().Update(model);
            if (result == new Guid())
                return View();
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult CreateRules()
        {
            return View(new LeaveRulesDetailViewModel());
        }

        [NonAction]
        public ActionResult CreateRuleDetail()
        {
            return View(new LeaveRulesDetailViewModel());
        }

        public ActionResult CreateRuleDetailPartial(LeaveRulesDetailViewModel model)
        {
            model.Action = model.Id == new Guid() ? "Create" : "Edit";
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CreateRuleDetail(LeaveRulesDetailViewModel model)
        {
            var result = new Logic().Create(model);
            if (result != new Guid())
            {
                if (Request.IsAjaxRequest())
                    return Json(true, JsonRequestBehavior.AllowGet);

                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (Request.IsAjaxRequest())
                    return Json(false, JsonRequestBehavior.AllowGet);

                return View(model);

            }
        }

        [HttpPost]
        public ActionResult EditRuleDetail(LeaveRulesDetailViewModel model)
        {
            var result = new Logic().Edit(model);
            if(result!=new Guid())
            {
                if (Request.IsAjaxRequest())
                    return Json(true, JsonRequestBehavior.AllowGet);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (Request.IsAjaxRequest())
                    return Json(false, JsonRequestBehavior.AllowGet);
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult DeleteRuleDetail(LeaveRulesDetailViewModel model)
        {
            model.IsDeleted = true;
            var result = new Logic().Edit(model);
            if (result != new Guid())
            {
                if (Request.IsAjaxRequest())
                    return Json(true, JsonRequestBehavior.AllowGet);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (Request.IsAjaxRequest())
                    return Json(false, JsonRequestBehavior.AllowGet);
                return View(model);
            }
        }

        public ActionResult Delete(Guid id)
        {
            var result = new Logic().GetLeaves<LeaveViewModel>(new LeaveViewModel { Id = id }).FirstOrDefault();
            return View(result);
        }

        [HttpPost]
        public ActionResult Delete(LeaveViewModel model)
        {
            var result = new Logic().DeleteLeave(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Details(Guid id)
        {
            
            var result = new Logic().GetLeaves<Leave>(new LeaveViewModel { Id = id }).FirstOrDefault();
            return View(result);
        }

        public ActionResult LeaveDetailCreatePartial()
        {
            var model = new RegisterViewModel { LeaveDetail = new LeaveDetailViewModel { LeaveTypeDetailList = new List<LeaveTypeDetailViewModel>() } };
            model.LeaveDetail = InitializeDetailList(model.LeaveDetail);
            return PartialView(model);
        }

        public ActionResult GetLeaveDetailForLeaveCreation(string employeeId)
        {
            var model = new LeaveDetailViewModel { LeaveTypeDetailList = new List<LeaveTypeDetailViewModel>() };
            var details = new Logic().GetLeaveDetail(new LeaveDetailSearchViewModel { EmployeeId = employeeId, SearchType = LeaveDetailSearchType.EmployeeId });
            if (details.Count() > 0)
            {
                model.EmployeeId = details.First().EmployeeId;
                foreach (var item in details)
                {
                    if(item.Type != LeaveType.Any)
                    model.LeaveTypeDetailList.Add(new LeaveTypeDetailViewModel { Type = item.Type, Count = item.Count, Description = item.Description });
                }
            }

            return PartialView(model);
        }

        public ActionResult AssignRules()
        {
            return View(new LeaveRuleViewModel());
        }

        public ActionResult AssignRulesPartial(LeaveRuleViewModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult AssignRule(LeaveRuleViewModel model)
        {
            var result = new Logic().Create(model);
            if(result != new Guid())
            {
                if (Request.IsAjaxRequest())
                    return Json(true, JsonRequestBehavior.AllowGet);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (Request.IsAjaxRequest())
                    return Json(false, JsonRequestBehavior.AllowGet);
                return View(model);
            }
        }

        public JsonResult GetAssignedRuleEditPartial(Guid id)
        {
            return Json(new Logic().GetAllAssignedRules(new LeaveRuleViewModel { Id = id }).First(), JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        public ActionResult EditAssignedRule(LeaveRuleViewModel model)
        {
            var result = new Logic().Edit(model);
            if(result != new Guid())
            {
                if (Request.IsAjaxRequest())
                    return Json(true, JsonRequestBehavior.AllowGet);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (Request.IsAjaxRequest())
                    return Json(false, JsonRequestBehavior.AllowGet);
                return View(model);
            }
        }

        public ActionResult AllAssignedRulesPartial()
        {
            var result = new Logic().GetAllAssignedRules(new LeaveRuleViewModel { Rule = new LeaveRulesDetail { Id = new Guid() } });
            return PartialView(result);
        }

        public ActionResult AllLeaveRulesDetailPartial()
        {
            return PartialView(new Logic().GetLeaveRuleDetail<LeaveRulesDetailViewModel>(new LeaveRulesDetailViewModel()));
        }

        #region Functions


        public JsonResult ProcessRulesForSync(string userId, string leaveType, string employeeType, string value)
        {
            float parsedValue;
            float.TryParse(value, out parsedValue);
            return Json(new Logic().ProcessRulesForSync(userId, (LeaveType)Enum.Parse(typeof(LeaveType), leaveType), (EmployeeType)Enum.Parse(typeof(EmployeeType), employeeType) , parsedValue), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Rules(string prefix, int pageSize, int pageNumber, string userType, string leaveType)
        {
            var rules = new Logic().GetLeaveRuleDetail<Select2OptionModel>(new LeaveRulesDetailViewModel { IsSelect2 = true, Name = prefix, LeaveType = (LeaveType)Enum.Parse(typeof(LeaveType), leaveType) });
            var existingRules = new Logic().GetAllAssignedRules(new LeaveRuleViewModel { EmployeeType = (EmployeeType)Enum.Parse(typeof(EmployeeType), userType) }).Select(m=>m.Rule.Id).ToList();
            rules = rules.Where(m => !existingRules.Contains(Guid.Parse(m.id))).ToList();
            var result = new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, rules);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLeaveRuleDetail(Guid id)
        {
            return Json(new Logic().GetLeaveRuleDetail<LeaveRulesDetailViewModel>(new LeaveRulesDetailViewModel { Id = id}).First(), JsonRequestBehavior.AllowGet);
        }

        public LeaveDetailViewModel InitializeDetailList(LeaveDetailViewModel model)
        {
            var enumCount = Enum.GetValues(typeof(LeaveType)).Cast<LeaveType>();
            foreach (var item in enumCount)
            {
                model.LeaveTypeDetailList.Add(new LeaveTypeDetailViewModel { Count = 0, Type = item });
            }
            return model;
        }

        public void SetSelect2List(LeaveViewModel model)
        {
            ViewBag.Branch = new SelectListItem { Value = model.BranchId.ToString(), Text = new Logic().GetBranches(new BranchSearchViewModel { Id = model.BranchId }).FirstOrDefault().Name };
            ViewBag.Department = new SelectListItem { Value = model.DepartmentId.ToString(), Text = new Logic().GetDepartment(model.DepartmentId).Name };
            ViewBag.Employee = new SelectListItem { Value = model.UserId, Text = new Helpers.Helpers().GetUser(model.UserId).UserName };
        }

        #endregion Functions
    }
}