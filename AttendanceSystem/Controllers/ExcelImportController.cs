﻿using BusinessLogic;
using CustomModels.Collection;
using CustomModels.Identity;
using CustomModels.ReportModels;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace AttendanceSystem.Controllers
{
    public class ExcelImportController : Controller
    {
        // GET: ExcelImport
       public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public bool Index(string difference)
        {
            try
            {
                var filePath = string.Empty;

                foreach (string upload in Request.Files)
                {
                    if (Request.Files[upload].FileName != "")
                    {
                        string path = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/uploads/";
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(path, filename));
                        filePath = Path.Combine(path, filename);
                    }
                }
                var strError = string.Empty;
                     
                byte[] file = System.IO.File.ReadAllBytes(filePath);
                using (MemoryStream ms = new MemoryStream(file))
                using (ExcelPackage package = new ExcelPackage(ms))
                {
                    if (package.Workbook.Worksheets.Count == 0)
                    {
                        strError = "Your Excel file does not contain any work sheets";
                        return false;
                    }        

                    else
                    {
                        var workSheet = package.Workbook.Worksheets[1];
                      
                        {



                            //for (int i = workSheet.Dimension.Start.Row;
                            //         i <= workSheet.Dimension.End.Row;
                            //         i++)
                            //{
                            //    for (int j = workSheet.Dimension.Start.Column;
                            //             j <= workSheet.Dimension.End.Column;
                            //             j++)
                            //    {
                            //        object cellValue = workSheet.Cells[i, j].Value;
                            //    }
                            //}

                            //Excel.Application xlApp = new Excel.Application();
                            //Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(filePath);
                            //Excel.Worksheet xlWorkSheet = xlWorkbook.Sheets[1];
                            //Excel.Range xlRange = xlWorkSheet.UsedRange;

                            //int rowCount = xlRange.Rows.Count;
                            //int colCount = xlRange.Columns.Count;
                            List<ExcelSheet> sheet = new List<ExcelSheet>();


                            for (int row = workSheet.Dimension.Start.Row + 1;
                                     row <= workSheet.Dimension.End.Row;
                                     row++)
                            {
                                var excel = new ExcelSheet();
                                for (int col = workSheet.Dimension.Start.Column;
                                         col <= workSheet.Dimension.End.Column;
                                         col++)
                                {
                                    if (workSheet.Cells[row, col] != null && workSheet.Cells[row, col].Value != null)
                                    {

                                        switch (col)
                                        {

                                            case 1: excel.Company = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 2: excel.Description = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 3: excel.Branch = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 4: excel.BCity = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 5: excel.BArea = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 6: excel.BStreet = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 7: excel.BHouse = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 8: excel.Department = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 9: excel.Employee = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 10: excel.Role = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 11: excel.Email = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 12: excel.Contact = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 13: excel.ECity = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 14: excel.EArea = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 15: excel.EStreet = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 16: excel.EHouse = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 17: excel.MachineId = Int32.Parse(workSheet.Cells[row, col].Value.ToString()); break;
                                            case 18: excel.Schedule = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 19: excel.AddedOn = string.IsNullOrEmpty(workSheet.Cells[row, col].Value.ToString()) ? DateTime.Now.ToString() : workSheet.Cells[row, col].Value.ToString(); break;
                                            default: break;

                                        }
                                    }
                                }

                                sheet.Add(excel);
                            }
                            GC.Collect();
                            GC.WaitForPendingFinalizers();

                            //Marshal.ReleaseComObject(xlRange);
                            //Marshal.ReleaseComObject(xlWorkSheet);

                            //xlWorkbook.Close();
                            //Marshal.ReleaseComObject(xlWorkbook);

                            //xlApp.Quit();
                            //Marshal.ReleaseComObject(xlApp);
                            var logic = new Logic();
                            foreach (var item in sheet)
                            {
                                Company company = item.Company != null ? logic.CheckCompany(item.Company) : null;
                                if (company == null && item.Company != null)
                                {
                                    company = new Company();
                                    company.Id = logic.CreateCompany(new CompanyViewModel { Active = true, Description = item.Description, Name = item.Company, Id = Guid.NewGuid() });
                                }
                                Branch branch = item.Branch != null ? logic.CheckBranch(item.Branch, company.Id) : null;
                                if (branch == null && item.Branch != null)
                                {
                                    branch = new Branch();
                                    var address = item.BCity == null ? new Address() : new Address { Active = true, Area = item.BArea, City = item.BCity, House = int.Parse(item.BHouse ?? "1"), Street = int.Parse(item.BStreet ?? "1") };
                                    branch.Id = logic.AddBranch(new Branch { Active = true, Address = address, CompanyId = company.Id, Id = Guid.NewGuid(), Name = item.Branch });
                                }
                                Department department = item.Department != null ? logic.CheckDepartment(item.Department, branch.Id) : null;
                                if (department == null && item.Department != null)
                                {
                                    department = new Department();
                                    department.Id = logic.CreateDepartment(new Department { Active = true, BranchCode = branch.Id, Id = Guid.NewGuid(), Name = item.Department });
                                }
                                var employee = item.Email != null && department.Id != new Guid() ? logic.CheckUser(item.Email, department.Id) : null;
                                if (employee == null && item.Employee != null)
                                {
                                    var signInManager = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
                                    var userManager = HttpContext.GetOwinContext().Get<ApplicationUserManager>();
                                    employee = new ApplicationUser();
                                    var address = item.ECity == null ? new Address() : new Address { Active = true, Area = item.EArea, City = item.ECity, House = int.Parse(item.EHouse ?? "1"), Street = int.Parse(item.EStreet ?? "1") };
                                    var result = new AccountController(userManager, signInManager).Register(new CustomModels.Collection.RegisterViewModel { Active = true, Address = address, Email = item.Email, DepartmentId = department.Id, Name = item.Employee, Password = "Asdfjkl;1", ConfirmPassword = "1234", PhoneNumber = item.Contact, Roles = item.Role, MachineId = item.MachineId, Permanant = true, Schedule = "Day Schedule", AddedOn = DateTime.Parse(item.AddedOn) });

                                }

                            }
                            
                        }
                        return true;
                    }
                }
            }


            catch (Exception ex)
            {
                return false;
            }
        }
      
    }
}