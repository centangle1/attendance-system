﻿using CustomModels.Collection;
using EntityProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public Guid AddPartTimeDetails(PartTimeUserDetail model)
        {
            return new DataAccess().AddPartTimeDetails(model);
        }
    }
}
