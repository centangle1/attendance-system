﻿using EntityProvider;
using CustomModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomModels.ReportModels;

namespace BusinessLogic
{
    public partial class Logic
    {
        public DataTableAttendanceViewModel ViewAllAttendance(UserParameters search)
        {
            var result = new DataAccess().GetAllAttendances(search);
            return result;
        }

        public bool AddAttendance(AttendanceFormat model)
        {
            var machine = new Logic().GetMachine(model.MacAddress);
            bool result = true;
            for (int userId = 0; userId < model.UserId.Count(); userId++)
            {
                try
                {
                    var attendance = new Attendance
                    {
                        BranchId = machine.BranchId,
                        MacAddress = model.MacAddress,
                        MachineId = int.Parse(model.UserId[userId]),
                        EntryTime = model.DateTime[userId]
                    };
                    bool currentResult = new DataAccess().AddAttendance(attendance);
                    if (!currentResult)
                    {
                        result = false;
                    }
                }
                catch
                {
                    result = false;
                }

            }
            return result;
        }

        public AttendanceViewModel GetUser(string id)
        {
            return new DataAccess().GetUser(id);
        }

        public string GetUserId(Attendance model)
        {
            return new DataAccess().GetUserId(model);
        }

        public string AddManualAttendance(AttendanceViewModel model)
        {
            model.Active = true;
            model.Id = Guid.NewGuid();

            return new DataAccess().AddManualAttendance(model);
        }

        public AttendanceViewModel GetAttendance(Guid id)
        {
            return new DataAccess().GetAttendance(id);
        }

        public string EditAttendance(AttendanceViewModel model)
        {
            return new DataAccess().EditAttendance(model);
        }
    }
}
