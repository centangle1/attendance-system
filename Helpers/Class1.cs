﻿using CustomModels.Identity;
using System;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomModels.Collection;
using CustomModels.ReportModels;
using Microsoft.AspNet.Identity;
using System.Web;
using System.Data;
using System.ComponentModel;


namespace Helpers
{
    public class Helpers
    {


        public List<IdentityRole> GetAllRoles()
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Roles.ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<IdentityRole>();
            }
        }
        public ApplicationUser GetUser(string id)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.Users.Where(m => m.Id == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                return new ApplicationUser();
            }
        }
        public Guid GetScheduleIdByName(string name)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Schedules.Where(m => m.Name == name).Select(m => m.Id).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public List<Select2OptionModel> Companies(string prefix)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var userId = HttpContext.Current.User.Identity.GetUserId();
                    var result = new List<Select2OptionModel>();
                    if (HttpContext.Current.User.IsInRole("Administrator"))
                    {
                        result = db.Companies.Where(m => m.Name.Contains(prefix) && m.Active).Select(m => new Select2OptionModel
                        {
                            id = m.Id.ToString(),
                            text = m.Name
                        }).ToList();

                    }
                    else
                    {
                        result = db.Companies.Join(db.Branches, c => c.Id, b => b.CompanyId, (c, b) => new { c, b })
                                    .Join(db.Departments, b => b.b.Id, d => d.BranchCode, (b, d) => new { b, d })
                                    .Join(db.Users, d => d.d.Id, u => u.DepartmentId, (d, u) => new { d, u })
                                    .Where(m => m.u.Id == userId && m.d.b.c.Active)
                                    .Select(m => new Select2OptionModel
                                    {
                                        id = m.d.b.c.Id.ToString(),
                                        text = m.d.b.c.Name
                                    }).ToList();

                    }


                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<Select2OptionModel>();
            }
        }

        public List<Select2OptionModel> Branches(string prefix, Guid company)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var userId = HttpContext.Current.User.Identity.GetUserId();
                    var result = new List<Select2OptionModel>();
                    if (HttpContext.Current.User.IsInRole("Administrator") || HttpContext.Current.User.IsInRole("Company Manager"))
                    {
                        result = db.Branches.Where(m => m.Name.Contains(prefix) && m.Active && m.CompanyId == company).Select(m => new Select2OptionModel
                        {
                            id = m.Id.ToString(),
                            text = m.Name
                        }).ToList();
                    }
                    else
                    {
                        var branchId = db.Branches.Join(db.Departments, b => b.Id, d => d.BranchCode, (b, d) => new { b, d })
                                       .Join(db.Users, d => d.d.Id, u => u.DepartmentId, (d, u) => new { d, u })
                                       .Where(m => m.u.Id == userId)
                                       .Select(m => m.d.b.Id).FirstOrDefault();
                        result = db.Branches.Where(m => m.Id == branchId && m.Active && m.CompanyId == company).Select(m => new Select2OptionModel
                        {
                            id = m.Id.ToString(),
                            text = m.Name
                        }).ToList();
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<Select2OptionModel>();
            }
        }

        public List<Select2OptionModel> Department(string prefix, Guid branch)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var userId = HttpContext.Current.User.Identity.GetUserId();
                    var result = new List<Select2OptionModel>();
                    if (HttpContext.Current.User.IsInRole("Administrator") || HttpContext.Current.User.IsInRole("Company Manager") || HttpContext.Current.User.IsInRole("Branch Manager"))
                    {
                        result = db.Departments.Where(m => m.Active && m.Name.Contains(prefix) && m.BranchCode == branch)
                            .Select(m => new Select2OptionModel
                            {
                                id = m.Id.ToString(),
                                text = m.Name
                            }).ToList();
                    }
                    else
                    {
                        result = db.Departments.Join(db.Users, d => d.Id, u => u.DepartmentId, (d, u) => new { d, u })
                                             .Where(m => m.u.Id == userId && m.d.Active && m.d.BranchCode == branch)
                                             .Select(m => new Select2OptionModel
                                             {
                                                 id = m.d.Id.ToString(),
                                                 text = m.d.Name
                                             }).ToList();
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<Select2OptionModel>();
            }
        }

        public List<Select2OptionModel> Employee(string prefix, Guid department)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var userId = HttpContext.Current.User.Identity.GetUserId();
                    var result = new List<Select2OptionModel>();
                    if (HttpContext.Current.User.IsInRole("Administrator") || HttpContext.Current.User.IsInRole("Company Manager") || HttpContext.Current.User.IsInRole("Branch Manager") || HttpContext.Current.User.IsInRole("Department Manager"))
                    {
                            result = db.Users.Where(m => m.UserName.Contains(prefix) && m.Active && m.DepartmentId == department && m.Permanant)
                               .Select(m => new Select2OptionModel
                               {
                                   id = m.Id,
                                   text = m.UserName
                               }).ToList();
                        
                    }
                    else
                    {
                        result = db.Users.Where(m => m.Id == userId && m.Active && m.DepartmentId == department)
                            .Select(m => new Select2OptionModel
                            {
                                id = m.Id,
                                text = m.UserName
                            }).ToList();

                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<Select2OptionModel>();
            }
        }

        public IList<string> GetRoles(string id)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var roles = userManager.GetRoles(id);
            return roles;
        }

        public SelectUserProperties GetUserProperties(string id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Users.Join(db.Departments, u => u.DepartmentId, d => d.Id, (u, d) => new { u, d })
                        .Join(db.Branches, d => d.d.BranchCode, b => b.Id, (d, b) => new { d, b })
                        .Join(db.Companies, b => b.b.CompanyId, c => c.Id, (b, c) => new { b, c })
                        .Where(m => m.b.d.u.Email == id || m.b.d.u.Id == id
                        //&&
                        //(m.c.Active) ||
                        //(m.c.Active && m.b.b.Active) ||
                        //(m.c.Active && m.b.b.Active && m.b.d.d.Active || m.b.d.u.Active) ||
                        //(m.c.Active && m.b.b.Active && m.b.d.d.Active && m.b.d.u.Active)
                        )
                        .Select(m => new SelectUserProperties
                        {
                            BranchId = m.b.b.Id,
                            CompanyId = m.c.Id,
                            DepartmentId = m.b.d.u.DepartmentId,
                            UserId = m.b.d.u.Id,
                            MachineId = m.b.d.u.MachineId,
                            LogoUrl = m.c.LogoUrl
                        }).FirstOrDefault();
                    result.UserRole = GetRoles(result.UserId).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new SelectUserProperties();
            }
        }

        public int GetDayNumber(string day)
        {
            var result = day == "Sunday" ? 0 : day == "Monday" ? 1 : day == "Tuesday" ? 2 : day == "Wednesday" ? 3 : day == "Thursday" ? 4 : day == "Friday" ? 5 : day == "Saturday" ? 6 : 0;
            return result;
        }

        public DataTable ToDataTable<T>(List<T> iList)
        {
            DataTable dataTable = new DataTable();
            PropertyDescriptorCollection propertyDescriptorCollection =
                TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type type = propertyDescriptor.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(type);


                dataTable.Columns.Add(propertyDescriptor.Name, type);
            }
            object[] values = new object[propertyDescriptorCollection.Count];
            foreach (T iListItem in iList)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
        public static string WriteDataTableToExcel(System.Data.DataTable dataTable, string worksheetName, string saveAsLocation, string ReporType, List<string> columnsToShow, bool isDaily, bool isUserSpecific, Dictionary<string, string> AdditionalProperties, List<string> HeaderColumns = null)
        {
            Microsoft.Office.Interop.Excel.Application excel;
            Microsoft.Office.Interop.Excel.Workbook excelworkBook;
            Microsoft.Office.Interop.Excel.Worksheet excelSheet;
            Microsoft.Office.Interop.Excel.Range excelCellrange;

          //  try
            //{
                // Start Excel and get Application object.
                excel = new Microsoft.Office.Interop.Excel.Application();

                // for making Excel visible
                excel.Visible = false;
                excel.DisplayAlerts = false;

                // Creation a new Workbook
                excelworkBook = excel.Workbooks.Add(Type.Missing);

                // Workk sheet
                excelSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelworkBook.ActiveSheet;
                excelSheet.Name = worksheetName;


                excelSheet.Cells[1, 1] = ReporType;

                excelSheet.Cells[1, columnsToShow.Count] = "Date : " + DateTime.Now.ToShortDateString();

                // loop through each row and add values to our sheet
                int rowcount = 2;
                var oldDate = new DateTime();
                if (isDaily && isUserSpecific == false)
                {
                    var date = dataTable.Rows[3][3].ToString();
                    oldDate = DateTime.Parse(date).Date;
                }
                // Show Headers
                int colIndex = 0;
                foreach (string col in columnsToShow)
                {
                    string HeaderName = null;
                    if (HeaderColumns != null)
                    {
                        HeaderName = HeaderColumns[colIndex];
                    }
                    colIndex++;


                    int index = dataTable.Columns.IndexOf(col);

                    if (HeaderName != null)
                        excelSheet.Cells[rowcount, colIndex] = HeaderName;
                    else
                        excelSheet.Cells[rowcount, colIndex] = dataTable.Columns[index].ColumnName;
                    excelSheet.Cells.Font.Color = System.Drawing.Color.Black;
                }

                // Show Rows
                foreach (DataRow datarow in dataTable.Rows)
                {
                    rowcount += 1;
                    colIndex = 0;
                    foreach (string col in columnsToShow)
                    {
                        colIndex++;
                        int index = dataTable.Columns.IndexOf(col);
                        excelSheet.Cells[rowcount, colIndex] = datarow[index].ToString();
                        if (isDaily)
                        {
                            var newDate = DateTime.Parse(datarow[3].ToString()).Date;
                            if (oldDate.Date != newDate.Date && isUserSpecific == false)
                            {
                                ModifyRowForNewDate(ref rowcount, columnsToShow.Count(), newDate, excelSheet);

                                oldDate = DateTime.Parse(datarow[3].ToString()).Date;
                                break;
                            }

                            var statusIndex = dataTable.Columns.IndexOf("StatusString");
                            var status = datarow[statusIndex].ToString();
                            if (status != "Present")
                            {
                                var branchIndex = dataTable.Columns.IndexOf("BranchName");
                                var dateIndex = dataTable.Columns.IndexOf("StartTime");
                                if (dateIndex == -1)
                                {
                                    dateIndex = dataTable.Columns.IndexOf("Entry");
                                }

                                ModifyAbsentRecord(datarow, ref rowcount, columnsToShow.Count(), status, branchIndex, dateIndex, excelSheet);

                                break;
                            }
                        }

                    }


                }

                // now we resize the columns
                excelCellrange = excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[rowcount, columnsToShow.Count]];
                excelCellrange.EntireColumn.AutoFit();
                Microsoft.Office.Interop.Excel.Borders border = excelCellrange.Borders;
                border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                border.Weight = 2d;


                excelCellrange = excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[1, columnsToShow.Count]];
                FormattingExcelCells(excelCellrange, "#cccccc", System.Drawing.Color.Black, true);


                excelCellrange = excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[1, 1]];
                excelCellrange.Cells.Font.Size = 14;

                excelCellrange = excelSheet.Range[excelSheet.Cells[2, 1], excelSheet.Cells[2, columnsToShow.Count]];
                FormattingExcelCells(excelCellrange, "#ffffff", System.Drawing.Color.Black, true);



                rowcount += 2;
                int additionalRowsIndex = rowcount;
                int additionalColIndex = 1;
                if (AdditionalProperties != null)
                {
                    excelSheet.Cells[rowcount, additionalColIndex] = "Summary";
                    excelCellrange = excelSheet.Range[excelSheet.Cells[rowcount, 1], excelSheet.Cells[rowcount, AdditionalProperties.Count]];
                    FormattingExcelCells(excelCellrange, "#cccccc", System.Drawing.Color.Black, true);

                    excelCellrange = excelSheet.Range[excelSheet.Cells[rowcount, 1], excelSheet.Cells[rowcount, 1]];
                    excelCellrange.Cells.Font.Size = 14;

                    rowcount++;

                    foreach (var addProp in AdditionalProperties)
                    {
                        excelSheet.Cells[rowcount, additionalColIndex] = addProp.Key;
                        excelSheet.Cells[(rowcount + 1), additionalColIndex] = addProp.Value;
                        additionalColIndex++;
                    }

                    if (AdditionalProperties.Count > 0)
                    {
                        //excelCellrange = excelSheet.Range[1];
                        excelCellrange = excelSheet.Range[excelSheet.Cells[additionalRowsIndex, 1], excelSheet.Cells[(additionalRowsIndex + 1), AdditionalProperties.Count]];
                        excelCellrange.Font.Bold = true;
                        excelCellrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                    }
                }
                //now save the workbook and exit Excel


                excelworkBook.SaveAs(saveAsLocation);
                excelworkBook.Close();
                excel.Quit();
                return saveAsLocation;
            //}
            //catch (Exception ex)
            //{

            //    return "";
            //}
            //finally
            //{
            //    excelSheet = null;
            //    excelCellrange = null;
            //    excelworkBook = null;
            //}

        }

        /// <summary>
        /// FUNCTION FOR FORMATTING EXCEL CELLS
        /// </summary>
        /// <param name="range"></param>
        /// <param name="HTMLcolorCode"></param>
        /// <param name="fontColor"></param>
        /// <param name="IsFontbool"></param>
        public static void FormattingExcelCells(Microsoft.Office.Interop.Excel.Range range, string HTMLcolorCode, System.Drawing.Color fontColor, bool IsFontbool)
        {
            range.Interior.Color = System.Drawing.ColorTranslator.FromHtml(HTMLcolorCode);
            range.Font.Color = System.Drawing.ColorTranslator.ToOle(fontColor);
            if (IsFontbool == true)
            {
                range.Font.Bold = IsFontbool;
            }
        }

        //
        //Function to modify rows for new dates
        public static void ModifyRowForNewDate(ref int rowcount, int columnsToShow, DateTime date, Microsoft.Office.Interop.Excel.Worksheet excelSheet)
        {

            excelSheet.Cells[rowcount + 1, columnsToShow / 2] = date.Date;
            excelSheet.Cells[rowcount + 1, 1] = "Date";
            rowcount += 2;
            Microsoft.Office.Interop.Excel.Range excelCellRange;
            excelCellRange = excelSheet.Range[excelSheet.Cells[rowcount - 1, 1], excelSheet.Cells[rowcount, columnsToShow]];
            FormattingExcelCells(excelCellRange, "#cec8c8", System.Drawing.Color.Black, true);
        }

        //
        //Function to modify rows according to status
        public static void ModifyAbsentRecord(DataRow dataRow, ref int rowcount, int columnsToShow, string status, int branchIndex, int dateIndex, Microsoft.Office.Interop.Excel.Worksheet excelSheet)
        {
            excelSheet.Cells[rowcount, 2] = dataRow[branchIndex];
            excelSheet.Cells[rowcount, 3] = dataRow[dateIndex];
            excelSheet.Cells[rowcount, 5] = status;
            Microsoft.Office.Interop.Excel.Range excelCellRange;
            excelCellRange = excelSheet.Range[excelSheet.Cells[rowcount, 3], excelSheet.Cells[rowcount, columnsToShow]];
            if (status == "Absent")
            {
                FormattingExcelCells(excelCellRange, "#c90808", System.Drawing.Color.White, true);
            }
            else if (status == "Holiday")
            {
                FormattingExcelCells(excelCellRange, "#ff9c3a", System.Drawing.Color.White, true);
            }
            else if (status == "OnLeave")
            {
                FormattingExcelCells(excelCellRange, "#deff84", System.Drawing.Color.White, true);
            }
        }

        public static string GetLoggedInUserId()
        {
            return HttpContext.Current.User.Identity.GetUserId();
        }
    }
}
