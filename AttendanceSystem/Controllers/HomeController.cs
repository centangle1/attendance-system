﻿using BusinessLogic;
using CustomModels.Collection;
using CustomModels.ReportModels;
using EnumLibrary;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using Microsoft.Ajax.Utilities;

namespace AttendanceSystem.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            var branches = new Logic().GetBranches();
            var chartResultList = new List<PieChartResult>();
            foreach (var branch in branches)
            {
                var parameters = new ReportDates
                {
                    BranchId = branch.Id,
                    BranchName = branch.Name,
                    StartTime = DateTime.Now,
                    Selection = ReportSelection.Daily
                };
                var userProperties = new Helpers.Helpers().GetUserProperties(User.Identity.GetUserId());
                var result = new Logic().GetNodeAttendanceResult(parameters, userProperties);
                chartResultList.Add(result);
            }
            TempData["chartResultList"] = chartResultList;
            return View();
        }
        public JsonResult GetUpdatedChartResult()
        {

            StreamReader sr = new StreamReader(Request.InputStream);
            var line = sr.ReadToEnd();
            var parameters = JsonConvert.DeserializeObject<ReportDates>(line);
            var userProperties = new Helpers.Helpers().GetUserProperties(User.Identity.GetUserId());
            var result = new Logic().GetNodeAttendanceResult(parameters, userProperties);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult MakeAllUsersPermanant()
        {
            new Logic().MakeAllUsersPermanant();
            return View("Index");
        }

        public ActionResult ConvertObjectUsingReflection()
        {
            var model = new AttendanceViewModel { Id = Guid.NewGuid(), BranchId = Guid.NewGuid(), DepartmentId = Guid.NewGuid(), EntryTime = DateTime.Now, MacAddress = "00:99:345:345", MachineId = 21, UserId = Guid.NewGuid().ToString(), UserName = "test"};
            var serializedModel = JsonConvert.SerializeObject(model);


            var variableType = model.GetType();
            string variableDataType = variableType.FullName;
            string variableAssembly = variableType.Assembly.Location;
            string variableNameSpace = variableType.Namespace;

            object calcInstance = Activator.CreateInstance(variableType);

            calcInstance = model;

            var result = ConvertObject(serializedModel, variableAssembly, variableNameSpace, variableDataType);
            return View();
        }

        public float ConvertObject(object value, string variableAssembly, string variableNamespace, string variableDataType)
        {
            Assembly testAssembly = Assembly.LoadFile(variableAssembly);
            Type testDataType = testAssembly.GetType(variableDataType);
            object calcInstance = Activator.CreateInstance(testDataType);
            calcInstance = JsonConvert.DeserializeObject(value.ToString());

            //Getting type of the property og object. 
            Type type = value.GetType().GetProperty(variableDataType).PropertyType;

            if (type.IsGenericType)
                type = type.GetGenericArguments()[0];

            value.GetType().GetProperty(variableDataType).SetValue(value, Activator.CreateInstance(type, value), null);

            return 0;
        }

    }
}