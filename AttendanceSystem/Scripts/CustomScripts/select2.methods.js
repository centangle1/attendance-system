﻿function Select2AutoCompleteAjax(id, url, dataArray, pageSize) {
    $(id).select2(
        {
            ajax: {
                delay: 150,
                url: url,
                dataType: 'json',

                data: dataArray,
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {

                        results: data.Results,
                        pagination: {
                            more: (params.page * pageSize) < data.Total
                        }
                    };
                }
            },
            placeholder: "--- Select ---",
            minimumInputLength: 0,
            allowClear: true,
        });
}