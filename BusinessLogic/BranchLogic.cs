﻿using CustomModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityProvider;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<Branch> GetBranches(BranchSearchViewModel search)
        {
            return new DataAccess().GetBranches(search);
        }

        public Guid AddBranch(Branch model)
        {
            if (model != null)
            {
                var verification = new DataAccess().AddBranch(model);
                if (verification != Guid.Empty)
                    return verification;
                return new Guid();
            }
            return new Guid();
        }

        public Guid EditBranch(Branch model)
        {
            return new DataAccess().EditBranch(model);
        }

        public Guid DeleteBranch(Branch model)
        {
            return new DataAccess().DeleteBranch(model);
        }
    }
}
