﻿using EntityProvider;
using CustomModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public List<Company> GetCompanies(CompanySearchViewModel search)
        {
            return new DataAccess().GetCompanies(search);
        }

        public Guid CreateCompany(CompanyViewModel model)
        {
            var result = new DataAccess().CreateCompany(model);
            if(result!=new Guid())
            {
                return result;
            }
            return new Guid();
        }

        public Guid EditCompany(CompanyViewModel model)
        {
            var result = new DataAccess().EditCompany(model);
            if(result!=new Guid())
            {
                return result;
            }
            return new Guid();
        }

        public Guid DeleteCompany(Company model)
        {
            var result =  new DataAccess().DeleteCompany(model);
            if (result!=new Guid())
            {
                return result;
            }
            return new Guid();
        }

        public List<CompanyPolicy> GetCompanyPolicies(CompanyPolicySearchViewModel search)
        {
            return new DataAccess().GetCompanyPolicies(search);
        }

        public Guid AddPolicy(CompanyPolicy model)
        {
            model.Id = Guid.NewGuid();
            var result = new DataAccess().AddPolicy(model);
            return result;
        }

        public Guid EditPolicy(CompanyPolicy model)
        {
            return new DataAccess().EditPolicy(model);
        }

        public Guid DeletePolicy(CompanyPolicy model)
        {
            return new DataAccess().DeletePolicy(model);
        }
    }
}
