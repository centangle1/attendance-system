﻿using AutoMapper;
using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public List<Machine> GetMachineInfo(string macAddress)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Machines.Where(m => (string.IsNullOrEmpty(macAddress) || m.MacAddress == macAddress)  && m.Active)
                        .Select(m => new
                        {
                            Id = m.Id,
                            Name = m.Name,
                            Active = m.Active,
                            BranchId = m.BranchId,
                            MacAddress = m.MacAddress,
                            CompanyId = m.CompanyId,
                            CreatedOn = m.CreatedOn,
                            CreatedBy = m.CreatedBy,
                            AppLink = m.AppLink,
                            LastUpdatedOn = m.LastUpdatedOn,
                            IpAddress = m.IpAddress,
                            Port = m.Port,
                            Status = m.Status,
                            StatusUpdateLink = m.StatusUpdateLink,
                            Up = m.Up,
                            UserName = m.UserName,
                            Password = m.Password
                        }).ToList()
                        .Select(m => new Machine
                        {
                            Id = m.Id,
                            Name = m.Name,
                            Active = m.Active,
                            BranchId = m.BranchId,
                            MacAddress = m.MacAddress,
                            Password = m.Password,
                            CompanyId = m.CompanyId,
                            CreatedOn = m.CreatedOn,
                            CreatedBy = m.CreatedBy,
                            AppLink = m.AppLink,
                            LastUpdatedOn = m.LastUpdatedOn,
                            IpAddress = m.IpAddress,
                            Port = m.Port,
                            Status = m.Status,
                            StatusUpdateLink = m.StatusUpdateLink,
                            Up = m.Up,
                            UserName = m.UserName,
                        }).ToList();
                    return result;

                }
            }

            catch (Exception ex)
            {
                return new List<Machine>();
            }
        }
        public Guid Add(Machine model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    model.Active = true;
                    model.Id = Guid.NewGuid();
                    model.CreatedOn = DateTime.Now;
                    model.LastUpdatedOn = DateTime.Now;
                    var result = db.Machines.Add(model);
                    if (db.SaveChanges() > 0)
                    {
                        return result.Id;
                    }
                    return new Guid();
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

          public Guid Edit(Machine model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var machine = db.Machines.Find(model.Id);

                    model.LastUpdatedOn = DateTime.Now;
                    machine.AppLink = model.AppLink;
                    machine.BranchId = model.BranchId;
                    machine.IpAddress = model.IpAddress;
                    machine.LastUpdatedOn = model.LastUpdatedOn;
                    machine.MacAddress = model.MacAddress;
                    machine.Name = model.Name;
                    machine.Password = model.Password;
                    machine.Port = model.Port;
                    machine.Status = model.Status;
                    machine.StatusUpdateLink = model.StatusUpdateLink;
                    machine.Up = model.Up;
                    machine.UserName = model.UserName;
                    db.Entry(machine).State = System.Data.Entity.EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        return machine.Id;
                    }
                    return new Guid();
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

          public Guid Delete(Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var machine = db.Machines.Find(id);
                    if (machine != null) {
                        db.Machines.Remove(machine);
                    }
                    if (db.SaveChanges() > 0)
                    {
                        return id;
                    }
                    return new Guid();
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

    }
}
