﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AttendanceSystemWpf.UserControls.Company;
using BusinessLogic.CompanyLogic;
using Models.Collection;
namespace AttendanceSystemWpf.UserControls.Company
{
    /// <summary>
    /// Interaction logic for Createu.xaml
    /// </summary>
    public partial class Create : UserControl
    {
        public Create()
        {
            InitializeComponent();
            

        }

        private void createcompany_Click(object sender, RoutedEventArgs e)
        {
            Models.Collection.Company model = new Models.Collection.Company();
            Logic cl = new Logic();
            
            cl.CreateCompanyLogic(model);   
            }

        }
                    
    }

