﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AttendanceSystemWpf
{
    /// <summary>
    /// Interaction logic for Adress.xaml
    /// </summary>
    public partial class Adress : Window
    {
        public Adress()
        {
            InitializeComponent();
        }

        private void bt1_Click(object sender, RoutedEventArgs e)
        {
            string cs = @"Data Source=.\SQLEXPRESS;Initial Catalog=AttendanceDb;Integrated Security=True";
            SqlConnection conn = null;


            try
            {

                string cntry = tb1.Text;
                string cty = tb2.Text;
                string areaa = tb3.Text;
                string streetnmbr = tb4.Text;
                string housenmbr = tb5.Text;
                int addresid = Convert.ToInt32(tb6.Text);


                conn = new SqlConnection(cs);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;

                cmd.CommandText = "INSERT INTO Adress(country,city,area,street_number,house_number,adress_id) VALUES('" + cntry + "',' " + cty + "', '" + areaa + "', '" + streetnmbr + "','" + housenmbr + "','" + addresid + "')";

                cmd.Prepare();


                cmd.ExecuteNonQuery();
                //}


            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

        }
    }
    } 
