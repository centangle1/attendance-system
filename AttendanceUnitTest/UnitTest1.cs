﻿using System;
using BusinessLogic;
using CustomModels.Collection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AttendanceUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetEarnedLeaves()
        {
            var result = new Logic().GetEarnedLeavesForUpdation(new EarnedLeaveViewModel());
        }

        [TestMethod]
        public void TestStatusAssignment()
        {
            var early = new Logic().CheckArrivalLeaveStatus(new SchedulePolicyDetailViewModel
            {
                StartingTime = new TimeSpan(9, 0, 0),
                EndingTime = new TimeSpan(5, 0, 0),
                Policy = new AttendancePolicy { Early = 30, Late = 30 }
            },
                new TimeSpan(8, 29, 00), new TimeSpan(4, 29, 0));
            var onTime = new Logic().CheckArrivalLeaveStatus(new SchedulePolicyDetailViewModel
            {
                StartingTime = new TimeSpan(9, 0, 0),
                EndingTime = new TimeSpan(5, 0, 0),
                Policy = new AttendancePolicy { Early = 30, Late = 30 }
            },
                new TimeSpan(9, 0, 0), new TimeSpan(5, 0, 0));
            var late = new Logic().CheckArrivalLeaveStatus(new SchedulePolicyDetailViewModel
            {
                StartingTime = new TimeSpan(9, 0, 0),
                EndingTime = new TimeSpan(5, 0, 0),
                Policy = new AttendancePolicy { Early = 30, Late = 30 }
            },
                new TimeSpan(9, 31, 00), new TimeSpan(5, 31, 0));
        }
    }
}
