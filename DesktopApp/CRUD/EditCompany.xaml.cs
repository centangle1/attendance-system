﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Models.Collection;

namespace AttendanceSystemWpf
{
    /// <summary>
    /// Interaction logic for EditCompany.xaml
    /// </summary>
    public partial class EditCompany : Window
    {
        #region[Constructor]
        public EditCompany()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Page_Loaded);
        }
        #endregion
        #region [Events]
        void Page_Loaded(object sender, RoutedEventArgs e)

        {
            Company model = new Company();
            {
                model.Name = "abccc";
                model.Description = "testing..";
            };
            this.DataContext = model;

        }
        #endregion
        #region [Event Helpers]
        #endregion
    }
}
