﻿using CustomModels.Collection;
using CustomModels.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public Guid AddPartTimeDetails(PartTimeUserDetail model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.PartTimeUserDetails.Add(model);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch(Exception ex)
            {
                return new Guid();
            }
        }
    }
}
